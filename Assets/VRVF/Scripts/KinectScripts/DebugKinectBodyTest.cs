﻿using UnityEngine;
using System.Collections;
using Kinect = Windows.Kinect;

public class DebugKinectBodyTest : MonoBehaviour
{
    BodySourceManager m_bodySourceManager;
    Kinect.Body[] m_bodiesArray;

    void Start()
    {

        m_bodySourceManager = FindObjectOfType<BodySourceManager>();
    }

    void Update()
    {
        m_bodiesArray = m_bodySourceManager.GetData();

        if (m_bodiesArray != null)
        {
            //Debug.Log(m_bodiesArray[0].Joints[Kinect.JointType.HandLeft].Position);
            Debug.Log(GetVector3FromJoint(m_bodiesArray[0].Joints[Windows.Kinect.JointType.HandLeft]));
        }
    }



    private static Vector3 GetVector3FromJoint(Kinect.Joint joint)
    {
        return new Vector3(joint.Position.X * 10, joint.Position.Y * 10, joint.Position.Z * 10);
    }

}
