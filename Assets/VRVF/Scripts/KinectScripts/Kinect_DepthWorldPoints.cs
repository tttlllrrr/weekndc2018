﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;

public class Kinect_DepthWorldPoints : MonoBehaviour
{
    KinectSensor m_kinectSensor;
    CoordinateMapper m_coordinateMapper;
    DepthSourceManager m_depthSourceManager;

    CameraSpacePoint[] m_cameraSpacePoints;
    UnityEngine.Vector4[] m_depthWorldPoints;

    float m_depthMin;
    float m_depthMax;

    FrequencyDataManager m_frequencyDataManager;
    public bool m_isDepthParticlesColorFFT = false;
    public Color m_depthParticlesStaticColor = Color.white;
    public float m_depthParticlesLifetime = 1.0f;
    public float m_depthParticlesScale = 1.0f;

    //public Vector3 m_positionsOffset;
    //public Vector3 m_positionsScale = Vector3.one;
    public Vector3 m_spawnVelocity;


    public bool m_isFreshDepthFlag = false;

    public float m_worldTransitionDuration = 5.0f;
    public AnimationCurve m_worldTransitionCurve;

    Vector3 m_sideMirror_PO = new Vector3(0, 1.1f, -1.63f);
    Vector3 m_sideMirror_PS = new Vector3(1,1,1);

    Vector3 m_frontMirror_PO = new Vector3(-0.52f, 1.1f, 0);
    Vector3 m_frontMirror_PS = new Vector3(-1, 1, -1);

    Vector3 m_upsideDown_PO = new Vector3(-0.52f, 2.1f, 0);
    Vector3 m_upsideDown_PS = new Vector3(-1, -1, -1);



    void Start()
    {
        m_kinectSensor = KinectSensor.GetDefault();
        m_coordinateMapper = m_kinectSensor.CoordinateMapper;

        m_depthSourceManager = FindObjectOfType<DepthSourceManager>();
        m_cameraSpacePoints = new CameraSpacePoint[m_kinectSensor.DepthFrameSource.FrameDescription.LengthInPixels];
        m_depthWorldPoints = new UnityEngine.Vector4[m_cameraSpacePoints.Length];

        m_depthMin = m_kinectSensor.DepthFrameSource.DepthMinReliableDistance;
        m_depthMax = m_kinectSensor.DepthFrameSource.DepthMaxReliableDistance;

        m_frequencyDataManager = FindObjectOfType<FrequencyDataManager>();

        Debug.Log(m_depthSourceManager._Reader);
        

        m_depthSourceManager._Reader.FrameArrived += _Reader_FrameArrived;

    }
    
    void OnDestroy()
    {
        if(m_depthSourceManager != null && m_depthSourceManager._Reader != null)
            m_depthSourceManager._Reader.FrameArrived -= _Reader_FrameArrived;
    }

    private void _Reader_FrameArrived(object sender, DepthFrameArrivedEventArgs e)
    {
        //Debug.Log("Depth arrived at frame: " + Time.frameCount);
        m_coordinateMapper.MapDepthFrameToCameraSpace(m_depthSourceManager.GetData(), m_cameraSpacePoints);
        m_isFreshDepthFlag = true;
    }


    public int GetDepthPointsCount()
    {
        return (int)m_kinectSensor.DepthFrameSource.FrameDescription.LengthInPixels;
    }

    public Color GetDepthParticlesColor()
    {
        if (m_isDepthParticlesColorFFT == true)
            return m_frequencyDataManager.m_currentFFTColor;
        else
            return m_depthParticlesStaticColor;
    }
    public float GetDepthParticlesLife()
    {
        return m_depthParticlesLifetime;
    }
    public float GetDepthParticlesScale()
    {
        return m_depthParticlesScale;
    }

    public CameraSpacePoint[] GetDepthCameraSpacePointsArray()
    {
        return m_cameraSpacePoints;
    }
    
    /*
    public IEnumerator AnimateTo_SideMirror()
    {
        Vector3 initialPositionOffset = m_positionsOffset;
        Vector3 intialScale = m_positionsScale;
        float timeCounter = 0;

        while(timeCounter < m_worldTransitionDuration)
        {
            float step = m_worldTransitionCurve.Evaluate(timeCounter / m_worldTransitionDuration);
            m_positionsOffset = Vector3.Lerp(initialPositionOffset, m_sideMirror_PO, step);
            m_positionsScale = Vector3.Lerp(intialScale, m_sideMirror_PS, step);

            timeCounter += Time.deltaTime;
            yield return null;
        }

        m_positionsOffset = m_sideMirror_PO;
        m_positionsScale = m_sideMirror_PS;
    }

    public IEnumerator AnimateTo_FrontMirror()
    {
        Vector3 initialPositionOffset = m_positionsOffset;
        Vector3 intialScale = m_positionsScale;
        float timeCounter = 0;

        while (timeCounter < m_worldTransitionDuration)
        {
            float step = m_worldTransitionCurve.Evaluate(timeCounter / m_worldTransitionDuration);
            m_positionsOffset = Vector3.Lerp(initialPositionOffset, m_frontMirror_PO, step);
            m_positionsScale = Vector3.Lerp(intialScale, m_frontMirror_PS, step);

            timeCounter += Time.deltaTime;
            yield return null;
        }

        m_positionsOffset = m_frontMirror_PO;
        m_positionsScale = m_frontMirror_PS;
    }

    public IEnumerator AnimateTo_UpsideDown()
    {
        Vector3 initialPositionOffset = m_positionsOffset;
        Vector3 intialScale = m_positionsScale;
        float timeCounter = 0;

        while (timeCounter < m_worldTransitionDuration)
        {
            float step = m_worldTransitionCurve.Evaluate(timeCounter / m_worldTransitionDuration);
            m_positionsOffset = Vector3.Lerp(initialPositionOffset, m_upsideDown_PO, step);
            m_positionsScale = Vector3.Lerp(intialScale, m_upsideDown_PS, step);

            timeCounter += Time.deltaTime;
            yield return null;
        }

        m_positionsOffset = m_upsideDown_PO;
        m_positionsScale = m_upsideDown_PS;
    }
    */

}
