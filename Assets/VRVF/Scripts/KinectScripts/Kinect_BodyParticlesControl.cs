﻿using UnityEngine;
using System.Collections;
using Kinect = Windows.Kinect;

public class Kinect_BodyParticlesControl : MonoBehaviour 
{
	BodySourceManager m_bodySourceManager;
	Kinect.Body[] m_bodiesArray;

	Kinect.HandState m_previousHandState_Left;
	Kinect.HandState m_previousHandState_Right;

	ComputeParticlesGenerationManager m_computeParticlesGenerationManager;
	ComputeVectorFieldManager m_computeVectorFieldManager;

	Vector3 m_handTipVelocity_Right = Vector3.zero;
	Vector3 m_handTipPreviousPosition_Right;
	

	void Start()
	{
		m_bodySourceManager = FindObjectOfType<BodySourceManager>();
		m_computeParticlesGenerationManager = FindObjectOfType<ComputeParticlesGenerationManager>();
		m_computeVectorFieldManager = FindObjectOfType<ComputeVectorFieldManager>();

	}

	void Update()
	{
		m_bodiesArray = m_bodySourceManager.GetData();

		if(m_bodiesArray != null)
		{

			//Debug.Log(m_bodiesArray.Length);

			for(int i = 0; i < m_bodiesArray.Length; i++)
			{
				if(m_bodiesArray[i].IsTracked == false)
					continue;

				Kinect.HandState currentHandState_Left = m_bodiesArray[i].HandLeftState;
				Kinect.HandState currentHandState_Right = m_bodiesArray[i].HandRightState;


				// left hand
				if(m_previousHandState_Left == Kinect.HandState.Open && currentHandState_Left == Kinect.HandState.Closed)
				{
					Debug.Log("Left Hand just closed!: " + Time.frameCount);
					//m_computeParticlesGenerationManager.m_isInjectionOverwrite = false;

				}
				if(m_previousHandState_Left == Kinect.HandState.Closed && currentHandState_Left == Kinect.HandState.Open)
				{
					Debug.Log("Left Hand just opened!: " + Time.frameCount);
					//m_computeParticlesGenerationManager.m_isInjectionOverwrite = true;
				}

				// right hand
				if(m_previousHandState_Right == Kinect.HandState.Open && currentHandState_Right == Kinect.HandState.Closed)
				{
					Debug.Log("Right Hand just closed!: " + Time.frameCount);
					//m_computeParticlesGenerationManager.m_isInjectionOverwrite = false;
				}
				if(m_previousHandState_Right == Kinect.HandState.Closed && currentHandState_Right == Kinect.HandState.Open)
				{
					Debug.Log("Right Hand just opened!: " + Time.frameCount);
					//m_computeParticlesGenerationManager.m_isInjectionOverwrite = true;
				}

				if(currentHandState_Right == Kinect.HandState.Lasso )
				{
					Vector3 freshHandTipPosition = JointToPosVector3( m_bodiesArray[i].Joints[Kinect.JointType.HandTipRight] );
					if(m_previousHandState_Right == Kinect.HandState.Lasso)
					{						
						Vector3 instantVel = (freshHandTipPosition - m_handTipPreviousPosition_Right) / Time.deltaTime;
						instantVel.z = -instantVel.z;
						m_handTipVelocity_Right = 0.5f * Vector3.Lerp(m_handTipVelocity_Right, instantVel, 0.5f);
						m_computeVectorFieldManager.SetVectorFieldVec_Uniform(m_handTipVelocity_Right);
						Debug.Log(m_handTipVelocity_Right);
					}
					
					m_handTipPreviousPosition_Right = freshHandTipPosition;
				}


				m_previousHandState_Left = currentHandState_Left;
				m_previousHandState_Right = currentHandState_Right;

				break;
			}

		}




	}

	private Vector3 JointToPosVector3(Kinect.Joint joint)
    {
        return new Vector3(joint.Position.X * 10, joint.Position.Y * 10, joint.Position.Z * 10);
    }

}
