﻿using UnityEngine;
using System.Collections;

public class FFTSetMaterialColor : MonoBehaviour
{
    FrequencyDataManager m_frequencyDataManager;
    public Material m_targetMaterial;
    Color m_colorToSet;    

    void Start()
    {
        m_frequencyDataManager = FindObjectOfType<FrequencyDataManager>();        
    }

    void Update()
    {
        float a = m_targetMaterial.color.a;
        m_colorToSet = m_frequencyDataManager.m_currentFFTColor;
        m_colorToSet.a = a;
        m_targetMaterial.color = m_colorToSet;
        
    }

}
