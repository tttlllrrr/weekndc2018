﻿using UnityEngine;
using System.Collections;

public class TransformLerper : MonoBehaviour
{
    public Transform m_targetTransform;
    [Range(0,1)]
    public float m_lerpRatioPerFrame_Position = 0.25f;
    [Range(0, 1)]
    public float m_lerpRatioPerFrame_Rotation = 0.25f;
    [Range(0, 1)]
    public float m_lerpRatioPerFrame_Scale = 0.25f;

    public bool m_isLerpingPosition = true;
    public bool m_isLerpingRotation = true;
    public bool m_isLerpingScale = false;

    Vector3 dummyVel = Vector3.zero;

    public bool m_isLookAt = false;
    public Transform m_lookAtTarget;

    void LateUpdate()
    {
        if (m_targetTransform == null)
            return;

        if(m_isLerpingPosition == true)
            transform.position = Vector3.SmoothDamp(transform.position, m_targetTransform.position, ref dummyVel, m_lerpRatioPerFrame_Position);  //Vector3.Lerp(transform.position, m_targetTransform.position, m_lerpRatioPerFrame_Position);

        if (m_isLerpingRotation == true)
            transform.rotation = Quaternion.Slerp(transform.rotation, m_targetTransform.rotation, m_lerpRatioPerFrame_Rotation);

        if (m_isLerpingScale == true)
            transform.localScale = Vector3.Lerp(transform.localScale, m_targetTransform.localScale, m_lerpRatioPerFrame_Scale);

        if(m_isLookAt == true)
        {
            transform.LookAt(m_lookAtTarget);
        }

    }

}
