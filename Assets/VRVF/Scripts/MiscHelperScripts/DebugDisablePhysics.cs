﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugDisablePhysics : MonoBehaviour
{
    void Start()
    {
        Physics.autoSimulation = false;
    }

}
