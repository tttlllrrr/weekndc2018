﻿using UnityEngine;
using System.Collections;

public class TransformRotator : MonoBehaviour
{
    public Vector3 m_rotationSpeed;

    void Update()
    {
        transform.Rotate(m_rotationSpeed * Time.deltaTime);
    }

}
