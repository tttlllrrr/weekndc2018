﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(RollingVelocityTracker))]
public class OrientationByRollingVelocity : MonoBehaviour
{
    RollingVelocityTracker m_rollingVelocityTracker;

    void Start()
    {
        m_rollingVelocityTracker = GetComponent<RollingVelocityTracker>();
    }

    void Update()
    {
        Vector3 rollingVel = m_rollingVelocityTracker.GetCurrentRollingVelocity(); 
        if(rollingVel.sqrMagnitude > 0.00001f)
            transform.forward = rollingVel;

    }
  

}
