﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugToggleIsParticleSimSpeedReactive : MonoBehaviour
{
    ComputeParticleSimulationManager m_computeParticleSim;

    void Start()
    {
        m_computeParticleSim = FindObjectOfType<ComputeParticleSimulationManager>();
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            m_computeParticleSim.m_isSimSpeedExtern = !m_computeParticleSim.m_isSimSpeedExtern;
            if (m_computeParticleSim.m_isSimSpeedExtern == false)
                m_computeParticleSim.m_particleSimSpeed = 1.0f;
        }
    }


}
