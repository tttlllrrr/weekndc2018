﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearRandomWriteTargets : MonoBehaviour
{
    private void OnPostRender()
    {
        Graphics.ClearRandomWriteTargets();
    }

}
