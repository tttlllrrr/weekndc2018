﻿using UnityEngine;
using System.Collections;

public class TimeScaler : MonoBehaviour
{
    [Range(0,1)]
    public float m_timeScaler = 1.0f;
    
    void Update()
    {
        Time.timeScale = m_timeScaler;
    }

}
