﻿using UnityEngine;
using System.Collections;

public class DebugSimSpeedControllByVolRM : MonoBehaviour
{

    ComputeParticleSimulationManager m_computeParticleSimulationManager;
    FrequencyDataManager m_frequencyDataManager;

    public bool m_isActive = false;

    [Range(-1, 4)]
    public float m_calculatedSpeed;

    [Range(-1, 1)]
    public float m_volDiff;
    float m_previousVol;

    public int m_expononent =1;
    public float m_preExponentScaler = 1.0f;
    public float m_postExponentScaler = 1.0f;
    public float m_preExponentStaticOffset = 0.0f;
    public float m_postExponentStaticOffset = 0.0f;

    public float m_volDiffOffsetScaler = 1.0f;

    public float m_preOutputScaler = 1.0f;
    public float m_outputClamp_Min = -1.0f;
    public float m_outputClamp_Max = 4.0f;


    void Start()
    {
        m_computeParticleSimulationManager = FindObjectOfType<ComputeParticleSimulationManager>();
        m_frequencyDataManager = FindObjectOfType<FrequencyDataManager>();
    }

    void Update()
    {
        if (m_isActive == false)
            return;

        float freshVol = m_frequencyDataManager.m_smoothDampedVolTemporalMax;
        m_volDiff = freshVol - m_previousVol;

        if (m_volDiff < 0)
            m_volDiff *= 10.0f;

        m_calculatedSpeed = CalculateExp(freshVol) + Mathf.Sign(m_volDiff)*CalculateExp(Mathf.Abs(m_volDiff) * m_volDiffOffsetScaler);
        m_calculatedSpeed = Mathf.Clamp(m_preOutputScaler*m_calculatedSpeed, m_outputClamp_Min, m_outputClamp_Max);
        


        m_calculatedSpeed = freshVol * m_postExponentScaler;

        m_computeParticleSimulationManager.m_externalSimSpeedScaler = m_calculatedSpeed;
        m_previousVol = freshVol;
    }

    float CalculateExp(float val)
    {
        return m_postExponentScaler* Mathf.Pow(m_preExponentScaler * (m_preExponentStaticOffset + val), m_expononent) + m_postExponentStaticOffset;     
    }

}
