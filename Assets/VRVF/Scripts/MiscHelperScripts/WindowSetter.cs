﻿using UnityEngine;
using System.Collections;

public class WindowSetter : MonoBehaviour 
{

	int m_windowResolution_x = 1920;
	int m_windowResolution_y = 1200;//1080;

	int m_windowPosition_x = 1920;
	int m_windowPosition_y = 0;

	void Start()
	{
		if(Application.isEditor == false)
		{
			// commenting out resolution set because it breaks vector field for some reason
			WindowController.SetWindowResolution(m_windowResolution_x, m_windowResolution_y);
			WindowController.SetOurWindowPosition(m_windowPosition_x, m_windowPosition_y);
		}
	}

}
