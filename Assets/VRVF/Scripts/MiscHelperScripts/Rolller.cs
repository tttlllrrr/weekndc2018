﻿using UnityEngine;
using System.Collections;

public class Rolller : MonoBehaviour
{
    public float m_rollSpeed = 0.0f;

    void Update()
    {
        //transform.Rotate(Vector3.forward, m_rollSpeed * Time.deltaTime, Space.World);
        transform.RotateAroundLocal(transform.forward, m_rollSpeed * Time.deltaTime);

    }
}
