﻿using UnityEngine;
using System.Collections;

public class DebugOrbit : MonoBehaviour
{

    public Vector3 m_targetPos = new Vector3(0, 0, 0);
    public Vector3 m_orbitOffset = new Vector3();

    [Range(0.1f,30)]
    public float m_orbitRadius = 2.0f;
    [Range(0,1)]
    public float m_orbitSpeed = 0.50f;

    float m_timeCounter = 0;
    
	void Update ()
    {
        m_timeCounter += Time.deltaTime;

        float x = m_orbitOffset.x + m_orbitRadius * Mathf.Sin(m_orbitSpeed * m_timeCounter);
        float z = m_orbitOffset.z + m_orbitRadius * Mathf.Cos(m_orbitSpeed * m_timeCounter);

        transform.position = new Vector3(x, m_orbitOffset.y, z);
        transform.LookAt(m_targetPos);
	}
}
