﻿using UnityEngine;
using System.Collections;

public class Orbiter : MonoBehaviour
{
    public float m_orbitDirection = 1.0f; // only valid should be +1 or -1
    public float m_orbitSpeed = 1.0f;
    public float m_orbitRadius = 1.0f;
    public float m_orbitProgress = 0;

    Vector3 m_currentOrbitCoord_Flat; // flat, centered values
    Vector3 m_currentOrbitCoord_Rotated;
    public Vector3 m_orbitRotation;
    public Vector3 m_orbitCenterOffset;
    Vector3 m_currentOrbit_Final;
    public Vector3 m_lookAtTargetPosition;
    public Vector3 m_sineOffsetAmplitude;
    public Vector3 m_sineOffsetFrequency;
    public Vector3 m_sineOffsetPhase;

    float m_timeCounter = 0;

    void Update()
    {
        m_timeCounter += Time.deltaTime;
        m_orbitProgress += Mathf.Sign(m_orbitDirection) * m_orbitSpeed * Time.deltaTime;        

        float flatX = m_orbitRadius * Mathf.Cos(m_orbitProgress);
        float flatY = m_orbitRadius * Mathf.Sin(m_orbitProgress);
        m_currentOrbitCoord_Flat = new Vector3(flatX, 0, flatY) + new Vector3( m_sineOffsetAmplitude.x * Mathf.Sin( m_sineOffsetFrequency.x * m_timeCounter * m_orbitSpeed + m_sineOffsetPhase.x),
                                                                               m_sineOffsetAmplitude.y * Mathf.Sin( m_sineOffsetFrequency.y * m_timeCounter * m_orbitSpeed + m_sineOffsetPhase.y),
                                                                               m_sineOffsetAmplitude.z * Mathf.Sin( m_sineOffsetFrequency.z * m_timeCounter * m_orbitSpeed + m_sineOffsetPhase.z));

        m_currentOrbitCoord_Rotated = Quaternion.Euler(m_orbitRotation) * m_currentOrbitCoord_Flat;
        m_currentOrbit_Final = m_currentOrbitCoord_Rotated + m_orbitCenterOffset;


        transform.localPosition = m_currentOrbit_Final;
        transform.LookAt(m_lookAtTargetPosition);
    }


}
