﻿using UnityEngine;
using System.Collections;

public class AudioAmplitude_TransformScaleBoost : MonoBehaviour
{
    FrequencyDataManager m_frequencyDataManager;
    float m_previousVolume = 0;
    float m_maxExtraScale = 1.0f;
    Vector3 m_originalScale;

    float m_extraScaleCounter = 0.0f;
    float m_extraScalerDecay = 1.0f;

    float m_volumeDiffThreashold = 0.17f;

    void Start()
    {
        m_frequencyDataManager = FindObjectOfType<FrequencyDataManager>();
        m_originalScale = transform.localScale;
    }

    void Update()
    {
        float freshVolume = m_frequencyDataManager.m_currentVolume;

        if ((freshVolume - m_previousVolume) > m_volumeDiffThreashold)
        {           
            m_extraScaleCounter = freshVolume;
        }

        m_extraScaleCounter -= m_extraScalerDecay * Time.deltaTime;

        m_extraScaleCounter = Mathf.Clamp(m_extraScaleCounter, 0.0f, m_maxExtraScale);
        transform.localScale = m_originalScale + m_extraScaleCounter * Vector3.one;

        m_previousVolume = freshVolume;
    }


}
