﻿using UnityEngine;
using System.Collections;

public class AudioTrackListPlayer : MonoBehaviour
{
    public AudioClip[] m_tracksArray;
    int m_currentPlayIndex = 0;

    AudioSource m_audioSource;

    void Start()
    {
        m_audioSource = GetComponent<AudioSource>();
    }

    public void MoveToNextTrack()
    {
        m_currentPlayIndex = (m_currentPlayIndex + 1) % m_tracksArray.Length;
        m_audioSource.Stop();
        m_audioSource.clip = m_tracksArray[m_currentPlayIndex];
        m_audioSource.Play();
    }

}
