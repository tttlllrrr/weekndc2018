﻿using UnityEngine;
using System.Collections;
using MidiJack;

public class MIDIControllerInputsManager : MonoBehaviour
{


    void OnEnable()
    {
        MidiMaster.noteOnDelegate += NoteOn;
        MidiMaster.noteOffDelegate += NoteOff;
        MidiMaster.knobDelegate += Knob;
    }

    void OnDisable()
    {
        MidiMaster.noteOnDelegate -= NoteOn;
        MidiMaster.noteOffDelegate -= NoteOff;
        MidiMaster.knobDelegate -= Knob;
    }

    ParametersManager m_parametersManager;

    float[] m_lastRecordedStateArray;

    void Start()
    {
        m_parametersManager = FindObjectOfType<ParametersManager>();
        m_lastRecordedStateArray = new float[64];
        for (int i = 0; i < m_lastRecordedStateArray.Length; i++)
            m_lastRecordedStateArray[i] = 0;
    }



    void NoteOn(MidiChannel channel, int note, float velocity)
    {
        Debug.Log("NoteOn: " + channel + "," + note + "," + velocity);
    }

    void NoteOff(MidiChannel channel, int note)
    {
        Debug.Log("NoteOff: " + channel + "," + note);
    }

    void Knob(MidiChannel channel, int knobNumber, float knobValue)
    {
        m_lastRecordedStateArray[knobNumber] = knobValue;
        //Debug.Log("Knob: " + knobNumber + "," + knobValue);

        // knob number 0-7 are the fader
        
        // group 0 is particle sim related
        if(knobNumber == 0)
        {
            m_parametersManager.SetParticlesMaterialScale(knobValue);
        }
        if(knobNumber == 8)
        {
            m_parametersManager.SetParticleSimulationSpeed(knobValue);
        }
        if(knobNumber == 16)
        {
            m_parametersManager.ToggleFreezeParticleSimulationSpeed(knobValue);
        }
        

        // group 1 is camera orbit
        if (knobNumber == 1)
        {
            // using hold mechanic
            if (m_lastRecordedStateArray[19] > 0)
            {
                m_parametersManager.SetCameraRotationAngle_x(knobValue);
            }
            else if (m_lastRecordedStateArray[20] > 0)
            {
                m_parametersManager.SetCameraRotationAngle_y(knobValue);
            }
            else if (m_lastRecordedStateArray[21] > 0)
            {
                m_parametersManager.SetCameraRotationAngle_z(knobValue);
            }
            else
                m_parametersManager.SetCameraOrbitRadius(1.0f -knobValue);
        }
        if (knobNumber == 9)
        {
            m_parametersManager.SetCameraOrbitSpeed(knobValue);
        }
        if(m_lastRecordedStateArray[19] + m_lastRecordedStateArray[20] + m_lastRecordedStateArray[21] > 1)
        {
            if (knobValue > 0)
                m_parametersManager.ToggleCameraOrbitDirection();
        }


        // group 2 is camera chase controls //force source control
        if (knobNumber == 2)
        {
            //Debug.Log("knob 2");
            //m_parametersManager.SetForceSourceHeight(knobValue);
            
        }
        if (knobNumber == 10)
        {
            //Debug.Log("knob 10");
            //m_parametersManager.SetForceSourceMagnitude(knobValue);
        }
        if( knobNumber == 22)
        {
            //Debug.Log("knob 22");
            //m_parametersManager.FlipForceSourceDirection(knobValue);
            if (knobValue > 0)
                m_parametersManager.ToggleCameraTarget();
        }

        // group 3 is vector sin set controls
        if(knobNumber == 3) //slider
        {
            //Debug.Log("knob 3");
            m_parametersManager.VectorField_SetSin_Scaler(knobValue);
            m_parametersManager.VectorField_SetSphere_SetScalerValue(knobValue);
        }
        if(knobNumber == 11) //knob
        {
            //Debug.Log("knob 11");
            m_parametersManager.VectorField_SetSin_Frequency(knobValue);
        }
        if(knobNumber == 25)
        {
            //Debug.Log("knob 25");
            if(knobValue > 0)
                m_parametersManager.VectorField_SetSin_Dispatch();
        }        
        if(knobNumber == 26)
        {
            if(knobValue > 0)
                m_parametersManager.VectorField_SetSphere_Dispatch(); //m_parametersManager.VectorField_SetSin_SetScalerSign();                        
        }
        


        // vector field controls
        if(knobNumber == 41)
        {
            if(knobValue > 0)
                m_parametersManager.ClearVectorField();
        }
        if (knobNumber == 42)
        {
            if (knobValue > 0)
                m_parametersManager.HalfVectorField();
        }
        if (knobNumber == 43)
        {
            if (knobValue > 0)
                m_parametersManager.DoubleVectorField();
        }
        if (knobNumber == 44)
        {
            if (knobValue > 0)
                m_parametersManager.FlipVectorField();
        }
        if (knobNumber == 45)
        {
            if (knobValue > 0)
                m_parametersManager.ToggleVectorFieldUpdate();
        }
        if (knobNumber == 46)
        {
            if (knobValue > 0)
                m_parametersManager.ToggleVectorFieldViewer();
        }
        


        // central spawner
        if (knobNumber == 34)
        {
            m_parametersManager.SetGeneratorLookAtType_UpAligned();
        }
        if (knobNumber == 35)
        {
            m_parametersManager.SetGeneratorLookAtType_TangentAligned();
        }
        if (knobNumber == 36)
        {
            m_parametersManager.SetGeneratorLookAtType_Center();
        }
        if (knobNumber == 37)
        {
            m_parametersManager.SetGeneratorTypeTo_Circle();
        }
        if (knobNumber == 38)
        {
            m_parametersManager.SetGeneratorTypeTo_Flat();
        }
        if (knobNumber == 39)
        {
            m_parametersManager.SetGeneratorTypeTo_Flower();
        }
        if(knobNumber == 14)
        {
            m_parametersManager.SetGeneratorPetalFrequency(knobValue);
        }
        if (knobNumber == 15)
        {
            m_parametersManager.SetGeneratorRadius(knobValue);
        }

        // genrator orbit controls
        if(knobNumber == 12)
        {            
            m_parametersManager.SetParticlesGeneratorOrbitRadius(knobValue);
        }
        if (knobNumber == 4)
        {
            if(m_lastRecordedStateArray[28] > 0)
                m_parametersManager.SetParticlesGeneratorOrbitSpeed(knobValue);
            if(m_lastRecordedStateArray[29] > 0)
                m_parametersManager.SetParticlesGeneratorOrbitOffsetAmplitude(knobValue);
            if(m_lastRecordedStateArray[30] > 0)
                m_parametersManager.SetParticlesGeneratorRotationSpeed(knobValue);
        }
        


        // the last 3 are RGB control
        if(knobNumber == 5)
        {
            m_parametersManager.RefreshBackgroundColor_R(knobValue);
        }
        if (knobNumber == 6)
        {
            m_parametersManager.RefreshBackgroundColor_G(knobValue);
        }
        if (knobNumber == 7)
        {
            m_parametersManager.RefreshBackgroundColor_B(knobValue);
        }

              
        if(knobNumber == 13)
        {
            m_parametersManager.SaturationBoost(knobValue);
        }
        
        if(knobNumber == 62)
        {
            if (knobValue > 0)
                m_parametersManager.CycleParticlesMaterial();
        }
        if(knobNumber == 61)
        {
            if (knobValue > 0)
                m_parametersManager.SpawnAtOnce();
        }
        if(knobNumber == 60)
        {
            if (knobValue > 0)
                m_parametersManager.ToggleAudioVolParticleSpeed();
        }

    }

}
