﻿using UnityEngine;
using System.Collections;

public class TransformLocalRotator : MonoBehaviour
{
    public Vector3 m_rotationAxies;
    public float m_rotationSpeed;

    void Update()
    {
        transform.Rotate(m_rotationSpeed * Time.deltaTime * m_rotationAxies, Space.Self);
    }

}
