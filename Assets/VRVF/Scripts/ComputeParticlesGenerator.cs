﻿using UnityEngine;
using System.Collections;

public class ComputeParticlesGenerator : MonoBehaviour
{
    [Range(0,100000)]
    public int m_particlesPerSecond = 10; // doesn't support spawn rate between 0 and 1
    [Range(0, 5)]
    public float m_particlesSpawnPositionRandomRange = 0.0f;

    public Vector3 m_spawnPositionScale = Vector3.one;
    public bool m_isSpawnRangeAbsoluteValue_x = false;
    public bool m_isSpawnRangeAbsoluteValue_y = false;
    public bool m_isSpawnRangeAbsoluteValue_z = false;

    public float m_particlesVelocityScale = 1.0f;
    public float m_particlesVelocity_Min = 0.1f;
    public float m_particlesVelocity_Max = 10.0f;

    public float m_particlesLifetime_Min = 1.0f;
    public float m_particlesLifetime_Max = 10.0f;

    public Color m_defaultSpawnColor = Color.red; // defailt

    public float m_spawnScale = 1.0f;

    public bool m_isParticleFlowConstant = false;
    public bool m_isParticleSpawnVelConstant = true;

    float m_timeCounter = 0;
    float m_timeIntervalBetweenSpawn = 0;
    ComputeParticlesGenerationManager m_computeParticlesGenerationManager;

    public bool m_isLeapSource = false;

    public enum SpawnTypes
    {
        Default,
        FFTBased
    };
    public SpawnTypes m_spawnType;

    public bool m_isUpVectorCircleSpawnUp = false;
    
    FrequencyDataManager m_frequencyDataManager;

    public int m_spawnConstant = 10;
    public bool m_isScaleSpreadFFT = false;
    public float m_scaleSpread_Min = 0.25f;
    public float m_scaleSpread_Max = 25.0f;

    // optional
    public int m_fftIndex = 0;
    
    // will be used to spawn partciles till fill the gaps between current and previous position, right now multiple particles are probably spawned on the exact same position while gaps remain
    Vector3 m_lastSpawnedPosition;
    Quaternion m_lastSpawnedRotation;

    bool m_isInitComplete = false;

    void Start()
    {
        m_lastSpawnedPosition = transform.position;
        m_lastSpawnedRotation = transform.rotation;

        m_computeParticlesGenerationManager = FindObjectOfType<ComputeParticlesGenerationManager>();
        m_frequencyDataManager = FindObjectOfType<FrequencyDataManager>();

        m_isInitComplete = true;
    }

    public void ManualUpdate(float deltaTime)
    {
        if (m_isInitComplete == false)
            return;

        UnityEngine.Profiling.Profiler.BeginSample("generator ManualUpdate");

        float volTemporalMaxFactor = Mathf.Pow(( m_frequencyDataManager.m_volumeTemporalMaxima), 4);

        if (m_spawnType == SpawnTypes.Default || m_isParticleFlowConstant == true)
        {
            m_timeIntervalBetweenSpawn = 1.0f / m_particlesPerSecond;
        }
        else if(m_spawnType == SpawnTypes.FFTBased)
        {
            m_timeIntervalBetweenSpawn = 1.0f / (volTemporalMaxFactor * (float)m_particlesPerSecond + m_spawnConstant);
        }

        m_timeCounter += deltaTime;

        // if not enough time has passed, simply abort here, there's nothing else to do
        if (m_timeCounter <= m_timeIntervalBetweenSpawn)
            return;

        Vector3 spawnPosition =  Vector3.zero;
        Vector3 spawnVelocity = Vector3.zero;
        Color spawnColor = m_defaultSpawnColor;
        Vector3 spawnForwardVector = transform.forward;
        Vector3 spawnUpVector = transform.up;
        float spawnLifetime = 1.0f;
        float spawnScale = 1.0f;

        Vector3 lastSpawnFromPreviousFrame_Pos = m_lastSpawnedPosition;
        Quaternion lastSpawnFromPreviousFrame_Rot = m_lastSpawnedRotation;
        Vector3 lerpedSpawnPos = Vector3.zero;
        Quaternion lerpedSpawnRot = Quaternion.identity;
        float timeRatio = m_timeIntervalBetweenSpawn/ m_timeCounter;
        float lerpStep = 0;

        while (m_timeCounter > m_timeIntervalBetweenSpawn)
        {
            lerpStep += timeRatio;
            //Debug.Log(Time.frameCount + " | " + lerpStep);
            lerpedSpawnPos = Vector3.Lerp(lastSpawnFromPreviousFrame_Pos, transform.position, lerpStep);
            lerpedSpawnRot = Quaternion.Slerp(lastSpawnFromPreviousFrame_Rot, transform.rotation, lerpStep);
            Vector3 lerpedForward = (lerpedSpawnRot * Vector3.forward);

            if (m_spawnType == SpawnTypes.Default)
            {
                spawnPosition = (GenerateRandomSpawnOffset()) + lerpedSpawnPos; //transform.position;
                spawnVelocity = m_particlesVelocityScale * Random.Range(m_particlesVelocity_Min, m_particlesVelocity_Max) * lerpedForward;//transform.forward;
                spawnColor = m_defaultSpawnColor;
                spawnLifetime = Random.Range(m_particlesLifetime_Min, m_particlesLifetime_Max);
                spawnScale = m_spawnScale;
            }
            else if (m_spawnType == SpawnTypes.FFTBased)
            {
                spawnPosition = volTemporalMaxFactor * (GenerateRandomSpawnOffset()) + lerpedSpawnPos;//transform.position;

                if (m_isParticleSpawnVelConstant == true)
                    spawnVelocity = m_particlesVelocityScale * Random.Range(m_particlesVelocity_Min, m_particlesVelocity_Max) * lerpedForward;//transform.forward;
                else
                    spawnVelocity = m_particlesVelocityScale * volTemporalMaxFactor * lerpedForward + 0.42f * lerpedForward;


                spawnColor = m_frequencyDataManager.m_currentFFTColor;
                spawnLifetime = Random.Range(m_particlesLifetime_Min, m_particlesLifetime_Max); //* m_frequencyDataManager.m_volumeTemporalMaxima;
                if (m_isScaleSpreadFFT == false)
                {
                    spawnScale = m_spawnScale * m_frequencyDataManager.m_volumeTemporalMaxima + 1.0f; //* m_frequencyDataManager.m_volumeTemporalMaxima;
                }
                else
                {
                    spawnScale = Mathf.Lerp(m_scaleSpread_Min, m_scaleSpread_Max, m_frequencyDataManager.m_processedFFTDataArray[m_fftIndex]);
                }
            }
            spawnColor.a = 1.0f;

            if (m_particlesSpawnPositionRandomRange > 0 && m_isUpVectorCircleSpawnUp == true)
                spawnUpVector = spawnPosition - lerpedSpawnPos;//transform.position;            

            spawnForwardVector = lerpedForward;

            m_computeParticlesGenerationManager.AddNewPartcileToSpawnList(spawnPosition, spawnVelocity, spawnColor, spawnForwardVector,spawnUpVector, spawnLifetime, spawnScale);

            m_timeCounter -= m_timeIntervalBetweenSpawn;
        }

        m_lastSpawnedPosition = lerpedSpawnPos;
        m_lastSpawnedRotation = lerpedSpawnRot;

        UnityEngine.Profiling.Profiler.EndSample();
    }

    Vector3 GenerateRandomSpawnOffset()
    {
        Vector3 randomPos = Vector3.zero;
        randomPos = m_particlesSpawnPositionRandomRange * Random.onUnitSphere;

        if (m_isSpawnRangeAbsoluteValue_x == true)
            randomPos.x = Mathf.Abs(randomPos.x);
        if (m_isSpawnRangeAbsoluteValue_y == true)
            randomPos.y = Mathf.Abs(randomPos.y);
        if (m_isSpawnRangeAbsoluteValue_z == true)
            randomPos.z = Mathf.Abs(randomPos.z);

        randomPos.x *= m_spawnPositionScale.x;
        randomPos.y *= m_spawnPositionScale.y;
        randomPos.z *= m_spawnPositionScale.z;       

        return transform.rotation * randomPos;
    }



}
