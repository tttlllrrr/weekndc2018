﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioReactiveMatScaler : MonoBehaviour 
{

	FrequencyDataManager m_frequencyDataManager;
	public float m_matScale_Min = 2.0f;
	public float m_matScale_Max = 10.0f;

	ComputeParticleSimulationManager m_computeParticlesSimulationManager;

	void Start()
	{
		m_frequencyDataManager = FindObjectOfType<FrequencyDataManager>();
		m_computeParticlesSimulationManager = FindObjectOfType<ComputeParticleSimulationManager>();
	}

	void Update()
	{
		float vol = m_frequencyDataManager.m_volumeTemporalMaxima; //m_smoothDampedVolTemporalMax;
		float step = Mathf.SmoothStep(0.250f, 1.0f, vol);
		float lerpedMatScale = Mathf.Lerp(m_matScale_Min, m_matScale_Max, step * step * step );

		m_computeParticlesSimulationManager.m_materialSizeScaler = lerpedMatScale;
	}

}
