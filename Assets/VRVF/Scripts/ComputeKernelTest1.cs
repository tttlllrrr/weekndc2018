﻿using UnityEngine;
using System.Collections;

public class ComputeKernelTest1 : MonoBehaviour
{
    public ComputeShader m_computeShader;

    void Start()
    {
        ComputeBuffer buffer = new ComputeBuffer(4 * 4 * 2 * 2, sizeof(int));
        int kernel = m_computeShader.FindKernel("CSMain2");

        m_computeShader.SetBuffer(kernel, "buffer2", buffer);
        m_computeShader.Dispatch(kernel, 2, 2, 1);

        int[] data = new int[4 * 4 * 2 * 2];
        buffer.GetData(data);

        for (int i = 0; i < 8; i++)
        {
            string line = "";
            for (int j = 0; j < 8; j++)
            {
                line += " " + data[j + i * 8];
            }
            Debug.Log(line);
        }
        buffer.Release();
    }

}
