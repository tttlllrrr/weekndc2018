﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;

public class PicturesData : MonoBehaviour
{
    string m_imagesFoldersFolderName = "ImagesFoldersFolder";
    string m_pathToImagesFolders;
    public List<string>[] m_picturePathsListsArray; // array is for folder, list for picture files

    // should be unsig EnumrateFiles/ etc...

    public bool m_isInit = false;
    public bool m_areImagesLoaded = false;



    void Start()
    {
        if(m_isInit == false)
            Init();

    }

    public void Init()
    {
        m_pathToImagesFolders = Application.streamingAssetsPath + "/" + m_imagesFoldersFolderName + "/";
        DirectoryInfo foldersFolderDirInfo = new DirectoryInfo(m_pathToImagesFolders);
        DirectoryInfo[] picturesFolderInfo = foldersFolderDirInfo.GetDirectories();
        m_picturePathsListsArray = new List<string>[picturesFolderInfo.Length];
        for (int i = 0; i < m_picturePathsListsArray.Length; i++)
        {
            m_picturePathsListsArray[i] = new List<string>();
            // bleurhh baaarf
            FileInfo[] filesInfo = picturesFolderInfo[i].GetFiles("*", SearchOption.AllDirectories).Where(s => s.ToString().EndsWith(".jpg", System.StringComparison.CurrentCultureIgnoreCase) ||
                                                                                                               s.ToString().EndsWith(".png", System.StringComparison.CurrentCultureIgnoreCase)).ToArray();
            for (int j = 0; j < filesInfo.Length; j++)
                m_picturePathsListsArray[i].Add(filesInfo[j].FullName);
        }

        /*
        for (int i = 0; i < m_picturePathsListsArray.Length; i++)
        {
            for (int j = 0; j < m_picturePathsListsArray[i].Count; j++)
                Debug.Log(m_picturePathsListsArray[i][j]);                
        }
        */

        m_isInit = true;
    }


    public IEnumerator LoadImagesIntoSprites(SpriteRenderer[][] m_spriteRenderersGroupsArray)
    {
        for(int i = 0; i < m_spriteRenderersGroupsArray.Length; i++)
        {
            for(int j = 0; j < m_spriteRenderersGroupsArray[i].Length; j++)
            {
                WWW www = new WWW(m_picturePathsListsArray[i][j]);
                
                int texW = www.texture.width ;
                int texH = www.texture.height;
                Texture2D tex = new Texture2D(texW, texH, TextureFormat.RGB24, false);

                yield return null;
                //yield return www;
                www.LoadImageIntoTexture(tex);
                m_spriteRenderersGroupsArray[i][j].sprite = Sprite.Create(tex, new Rect(0, 0, texW, texH), new Vector2(0.5f, 0.5f));
                //m_spriteRenderersGroupsArray[i][j].sprite = Sprite.Create(www.texture, new Rect(0, 0, texW, texH), new Vector2(0.5f,0.5f));
            }

            Debug.Log("Group complete " + i.ToString() + " | " + m_spriteRenderersGroupsArray[i].Length + " | " + Time.time);
        }

        m_areImagesLoaded = true;
        Debug.Log("Images loading complete");

    }


}
