﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PictureSpritesGenerator : MonoBehaviour
{
    public GameObject m_spritePrefab;
    SpriteRenderer[][] m_pictureSpriteRenderersGroupsArray;
    Transform[][] m_picturesTransformsGroupsArray;

    Transform[] m_groupHoldersTransforms;

    PicturesData m_picturesData;

    bool m_isTransformsSet = false;

    void Start()
    {
        m_picturesData = FindObjectOfType<PicturesData>();
        m_picturesData.Init();

        int groupsCount = m_picturesData.m_picturePathsListsArray.Length;
        m_pictureSpriteRenderersGroupsArray = new SpriteRenderer[groupsCount][];
        m_picturesTransformsGroupsArray = new Transform[groupsCount][];
        m_groupHoldersTransforms = new Transform[groupsCount];

        for(int i = 0; i < groupsCount; i++)
        {
            int picturesInGroupCount = m_picturesData.m_picturePathsListsArray[i].Count;
            m_pictureSpriteRenderersGroupsArray[i] = new SpriteRenderer[picturesInGroupCount];
            m_picturesTransformsGroupsArray[i] = new Transform[picturesInGroupCount];
            GameObject groupHolderGO = new GameObject("PicturesGroupHolder_" + i.ToString());
            m_groupHoldersTransforms[i] = groupHolderGO.transform;
            m_groupHoldersTransforms[i].transform.parent = transform;
            m_groupHoldersTransforms[i].transform.localPosition = Vector3.zero;
            for(int j = 0; j < picturesInGroupCount; j++)
            {
                GameObject spriteGO = (GameObject)Instantiate(m_spritePrefab);
                spriteGO.name += "_" + i.ToString() + "_" + j.ToString();
                m_pictureSpriteRenderersGroupsArray[i][j] = spriteGO.GetComponent<SpriteRenderer>();
                m_picturesTransformsGroupsArray[i][j] = spriteGO.transform;
                m_picturesTransformsGroupsArray[i][j].transform.parent = m_groupHoldersTransforms[i];
                m_picturesTransformsGroupsArray[i][j].transform.localPosition = Vector3.zero;
            }
        }

        StartCoroutine(m_picturesData.LoadImagesIntoSprites(m_pictureSpriteRenderersGroupsArray));

    }



    void Update()
    {
        if(m_picturesData.m_areImagesLoaded == true)
        {

            if(m_isTransformsSet == false)
            {
                SetTransforms();
            }

        }
    }

    void SetTransforms()
    {
        for(int i= 0; i < m_picturesTransformsGroupsArray.Length; i++)
        {
            m_groupHoldersTransforms[i].gameObject.AddComponent<TransformLocalRotator>();
            m_groupHoldersTransforms[i].GetComponent<TransformLocalRotator>().m_rotationAxies = Random.rotationUniform.eulerAngles;
            m_groupHoldersTransforms[i].GetComponent<TransformLocalRotator>().m_rotationSpeed = 0.1f *Random.Range(0.1f, 0.15f*(1.0f/(i+1.0f)));
            for (int j = 0; j < m_picturesTransformsGroupsArray[i].Length; j++)
            {
                float radius = 20.0f + i * 8.0f;
                m_picturesTransformsGroupsArray[i][j].localPosition = Random.onUnitSphere * radius;
                m_picturesTransformsGroupsArray[i][j].forward = -m_picturesTransformsGroupsArray[i][j].localPosition.normalized;
            }
        }


        m_isTransformsSet = true;
    }





}
