﻿using UnityEngine;
using System.Collections;

public class ComputeParticlesGenerationManager : MonoBehaviour
{
    public ComputeShader m_particleGenerationCS;
    ComputeBuffer m_particlesToSpawn_PositionsCB;
    ComputeBuffer m_particlesToSpawn_VelocitiesCB;
    ComputeBuffer m_particlesToSpawn_ColorsCB;
    ComputeBuffer m_particlesToSpawn_ForwardVectorCB;
    ComputeBuffer m_partcilesToSpawn_UpVectorCB;
    
    ComputeBuffer m_kinectDepthCameraSpacePointsCB;

    ComputeParticleSimulationManager m_computeParticlesSimulationManager;
    ComputeParticlesGenerator[] m_particlesGeneratorsArray;
    ComputeParticlesLerpToTargetManager m_computeParticlesLerpToTargetManager;    

    Vector4[] m_particlesToSpawnPositionsArray;
    Vector4[] m_particlesToSpawnVelocitiesArray;
    Vector4[] m_particlesToSpawnColorsArray;
    Vector4[] m_particlesToSpawnForwardVectorsArray;
    Vector4[] m_particlesToSpawnUpVectorsArray;

    int m_currentSpawnIndex = 0; // should be moduloed using the max particle count    
    int m_particlesSpawnedThisFrameListCount = 0;

    ComputeParticlesTransform m_computeParticlesTransform;    
    ComputeParticlesTransform.TransformData[] m_transformDataStructArray;
    ComputeBuffer m_transformInverseDataCB;
    int m_sizeTT;

    public struct ParticleSpawnData
    {
        Vector4 spawnPos;
        Vector4 spawnVel;
        Vector4 spawnCol;
        Vector4 spawnUpVec;
        Vector4 spawnForwardVec;
    }
    ComputeBuffer m_particleSpawnDataAB;
    ComputeBuffer m_particleSpawnDataCounter;

    void Start()
    {
        m_computeParticlesSimulationManager = FindObjectOfType<ComputeParticleSimulationManager>();
        m_particlesGeneratorsArray = FindObjectsOfType<ComputeParticlesGenerator>();
        m_computeParticlesLerpToTargetManager = FindObjectOfType<ComputeParticlesLerpToTargetManager>();
        m_computeParticlesTransform = FindObjectOfType<ComputeParticlesTransform>();

        m_particlesToSpawnPositionsArray = new Vector4[m_computeParticlesSimulationManager.GetParticleCount()];
        m_particlesToSpawnVelocitiesArray = new Vector4[m_computeParticlesSimulationManager.GetParticleCount()];
        m_particlesToSpawnColorsArray = new Vector4[m_computeParticlesSimulationManager.GetParticleCount()];
        m_particlesToSpawnForwardVectorsArray = new Vector4[m_computeParticlesSimulationManager.GetParticleCount()];
        m_particlesToSpawnUpVectorsArray = new Vector4[m_computeParticlesSimulationManager.GetParticleCount()];

        m_particlesToSpawn_PositionsCB = new ComputeBuffer(m_computeParticlesSimulationManager.GetParticleCount(), sizeof(float) * 4);
        m_particlesToSpawn_VelocitiesCB = new ComputeBuffer(m_computeParticlesSimulationManager.GetParticleCount(), sizeof(float) * 4);
        m_particlesToSpawn_ColorsCB = new ComputeBuffer(m_computeParticlesSimulationManager.GetParticleCount(), sizeof(float) * 4);
        m_particlesToSpawn_ForwardVectorCB = new ComputeBuffer(m_computeParticlesSimulationManager.GetParticleCount(), sizeof(float) * 4);
        m_partcilesToSpawn_UpVectorCB = new ComputeBuffer(m_computeParticlesSimulationManager.GetParticleCount(), sizeof(float) * 4);

        m_transformDataStructArray = new ComputeParticlesTransform.TransformData[1];
        m_transformDataStructArray[0] = new ComputeParticlesTransform.TransformData();
        m_transformDataStructArray[0].transformMatrix = m_computeParticlesTransform.GetTransform().localToWorldMatrix.inverse;
        m_transformDataStructArray[0].transformPosition = -m_computeParticlesTransform.GetTransform().position;
        m_sizeTT = System.Runtime.InteropServices.Marshal.SizeOf(typeof(ComputeParticlesTransform.TransformData));
        m_transformInverseDataCB = new ComputeBuffer(1, m_sizeTT);
        m_transformInverseDataCB.SetData(m_transformDataStructArray);
        
        for (int i = 0; i < m_particlesToSpawnPositionsArray.Length; i++)
        {
            m_particlesToSpawnPositionsArray[i] = Vector4.zero;
            m_particlesToSpawnVelocitiesArray[i] = Vector4.zero;
            m_particlesToSpawnColorsArray[i] = Vector4.zero;
            m_particlesToSpawnForwardVectorsArray[i] = Vector4.zero;
            m_particlesToSpawnUpVectorsArray[i] = Vector4.zero;
        }

        m_particleSpawnDataAB = new ComputeBuffer(m_computeParticlesSimulationManager.GetParticleCount(), System.Runtime.InteropServices.Marshal.SizeOf(typeof(ParticleSpawnData)), ComputeBufferType.Append);
        m_particleSpawnDataAB.SetCounterValue(0);
        m_particleSpawnDataCounter = new ComputeBuffer(1, sizeof(uint), ComputeBufferType.Raw);



    }

    void LateUpdate()
    {
        UnityEngine.Profiling.Profiler.BeginSample("Update");
        // clear all spawn data
        m_particlesSpawnedThisFrameListCount = 0;
        // clearing arrays actually shouldn't be necessary
        
        UnityEngine.Profiling.Profiler.BeginSample("For loop");
        for (int i = 0; i < m_particlesGeneratorsArray.Length; i++)
        {
            if (m_particlesGeneratorsArray[i].gameObject.activeInHierarchy == true)
            {
                m_particlesGeneratorsArray[i].ManualUpdate(Time.deltaTime);                
            }
        }
        UnityEngine.Profiling.Profiler.EndSample();

        
        UnityEngine.Profiling.Profiler.BeginSample("spawn from list");
        if (m_particlesSpawnedThisFrameListCount > 0) 
        {
            //Debug.Log("updating spawner: " + Time.frameCount);

            // old brute force approach
            //SpawnParticlesBasedFromListAndInjection();

            // CPU side spawning 
            // new append consume buffer based appraoch
            SpawnParticles_AppendConsume(m_particlesSpawnedThisFrameListCount, m_particlesToSpawnPositionsArray, m_particlesToSpawnVelocitiesArray, m_particlesToSpawnColorsArray, m_particlesToSpawnForwardVectorsArray, m_particlesToSpawnUpVectorsArray);

            // GPU side spwaning (pure buffers), using ParticleData struct (first test is spawning from surf shader)


        }
        UnityEngine.Profiling.Profiler.EndSample();

        UnityEngine.Profiling.Profiler.EndSample();        
    }

    public ComputeBuffer GetTransformInverseDataCB()
    {
        return m_transformInverseDataCB;
    }


    public ComputeBuffer GetFresh_ParticleSpawnDataCounter()
    {
        ComputeBuffer.CopyCount(m_particleSpawnDataAB, m_particleSpawnDataCounter, 0);
        return m_particleSpawnDataCounter;
    }

    public ComputeBuffer GetParticleSpawnDataAB()
    {
        return m_particleSpawnDataAB;
    }

    // pure GPU side particle gen
    // this function just sets buffers (really shouldn't need to be done every frame) and calls dispatch
    // should get called by script that's managing/handling the materials/shaders that are spawning
    public void SpawnParticles_AppendFrom_ParticleSpawnAB(ComputeBuffer particleSpawnData_Buffer_Consume, ComputeBuffer particleSpawnData_Counter)
    {
        UnityEngine.Profiling.Profiler.BeginSample("SpawnParticles_AppendFrom_ParticleSpawnAB");


        // need to call these to allow use from pixel shader
        Graphics.SetRandomWriteTarget(5, m_computeParticlesSimulationManager.GetParticlesAliveListBuffer_Consume(), true);
        Graphics.SetRandomWriteTarget(6, m_computeParticlesSimulationManager.GetParticlesDeadListBuffer(), true);
        //Graphics.SetRandomWriteTarget(7, GetParticleSpawnDataAB(), true);
        Graphics.SetRandomWriteTarget(7, particleSpawnData_Buffer_Consume, true);
        


        int kernelIndex = 2;
        //kernelIndex = m_particleGenerationCS.FindKernel("CSParticleGenerator_AppendFromParticleSpawnData");
        //Debug.Log(kernelIndex);

        m_particleGenerationCS.SetInt("_particlesTotalCount", m_computeParticlesSimulationManager.GetParticleCount());
        m_particleGenerationCS.SetInt("_partcilesPerThread", m_computeParticlesSimulationManager.GetParticlesPerThread());
        m_particleGenerationCS.SetInt("k_threadGroupCount_x", m_computeParticlesSimulationManager.GetThreadGroupCount_X());
        m_particleGenerationCS.SetInt("k_threadGroupCount_y", m_computeParticlesSimulationManager.GetThreadGroupCount_Y());


        m_particleGenerationCS.SetBuffer(kernelIndex, "_positions", m_computeParticlesSimulationManager.GetPositionsCB());
        m_particleGenerationCS.SetBuffer(kernelIndex, "_velocities", m_computeParticlesSimulationManager.GetVelocitiesCB());
        m_particleGenerationCS.SetBuffer(kernelIndex, "_colors", m_computeParticlesSimulationManager.GetColorsCB());
        m_particleGenerationCS.SetBuffer(kernelIndex, "_forwardVectors", m_computeParticlesSimulationManager.GetForwardVectorsCB());
        m_particleGenerationCS.SetBuffer(kernelIndex, "_upVectors", m_computeParticlesSimulationManager.GetUpVectorsCB());

        m_particleGenerationCS.SetBuffer(kernelIndex, "_particlesDeadListBuffer_Consume", m_computeParticlesSimulationManager.GetParticlesDeadListBuffer());
        m_particleGenerationCS.SetBuffer(kernelIndex, "_particlesAliveListBuffer_Append", m_computeParticlesSimulationManager.GetParticlesAliveListBuffer_Consume());

        m_particleGenerationCS.SetBuffer(kernelIndex, "_particlesAliveListCounter", m_computeParticlesSimulationManager.GetFreshParticlesAliveCounterBuffer());
        m_particleGenerationCS.SetBuffer(kernelIndex, "_particlesDeadListCounter", m_computeParticlesSimulationManager.GetFreshParticlesDeadCounterBuffer());

        //m_particleGenerationCS.SetBuffer(kernelIndex, "_particleSpawnDataBuffer_Consume", GetParticleSpawnDataAB());
        //m_particleGenerationCS.SetBuffer(kernelIndex, "_particleSpawnData_Counter", GetFresh_ParticleSpawnDataCounter());
        m_particleGenerationCS.SetBuffer(kernelIndex, "_particleSpawnDataBuffer_Consume", particleSpawnData_Buffer_Consume);
        m_particleGenerationCS.SetBuffer(kernelIndex, "_particleSpawnData_Counter", particleSpawnData_Counter);


        UnityEngine.Profiling.Profiler.BeginSample("particle gen from ParticleSpawnAB dispatch");
        // this is spawning the previous frame's Append buffer contents, it's counter should be set to 0 for fresh data spawn  only once this has finished running/spawning
        m_particleGenerationCS.Dispatch(kernelIndex, m_computeParticlesSimulationManager.GetThreadGroupCount_X(), m_computeParticlesSimulationManager.GetThreadGroupCount_Y(), 1);
        UnityEngine.Profiling.Profiler.EndSample();

        
        UnityEngine.Profiling.Profiler.EndSample();

        //ComputeBuffer.CopyCount(m_particlesToDrawIndicesAB, m_particlesToDrawIndicesCountBuffer, 0);
        //ComputeBuffer.CopyCount(m_computeParticlesSimulationManager.GetParticlesDeadListBuffer(), m_particlesToDrawIndicesCountBuffer, 0);
        //ComputeBuffer.CopyCount(m_computeParticlesSimulationManager.GetParticlesAliveListBuffer_Consume(), m_particlesToDrawIndicesCountBuffer, 0);
        //ComputeBuffer.CopyCount(m_computeParticlesSimulationManager.GetParticlesAliveListBuffer_Append(), m_particlesToDrawIndicesCountBuffer, 0);
        //ComputeBuffer.CopyCount(m_particlesToDrawIndicesAB, m_particlesToDrawIndicesCountBuffer, 0);

        //UnityEngine.Profiling.Profiler.EndSample();
        //UnityEngine.Profiling.Profiler.BeginSample("Get Data");
        //m_particlesToDrawIndicesCountBuffer.GetData(m_particlesToDrawIndicesCountBufferDump);
        //UnityEngine.Profiling.Profiler.EndSample();
        //m_particlesToDrawCount = m_particlesToDrawIndicesCountBufferDump[0];
        //Debug.Log(m_particlesToDrawCount);
    }

    public void SetParticleSpawnABCounter(uint value)
    {        
        m_particleSpawnDataAB.SetCounterValue(value);
    }

    //new append/consume based approach
    // can be called from anywhere by anyone for a spawn operation
    public void SpawnParticles_AppendConsume(int particlesSpawnCount, Vector4[] spawnPositionsArray, Vector4[] spawnVelocitiesArray, Vector4[] spawnColorsArray, Vector4[] spawnForwardVectorsArray, Vector4[] spawnUpVectorsArray)
    {
        UnityEngine.Profiling.Profiler.BeginSample("SpawnParticles_AppendConsume");

        int kernelIndex = 1; //m_particleGenerationCS.FindKernel("CSParticleGenerator_AppendConsume");
        //Debug.Log(kernelIndex);
        //kernelIndex = 0;

        UnityEngine.Profiling.Profiler.BeginSample("Setting CBs");
        m_particlesToSpawn_PositionsCB.SetData(spawnPositionsArray, 0, 0, particlesSpawnCount);
        m_particlesToSpawn_VelocitiesCB.SetData(spawnVelocitiesArray, 0, 0, particlesSpawnCount);
        m_particlesToSpawn_ColorsCB.SetData(spawnColorsArray, 0, 0, particlesSpawnCount);
        m_particlesToSpawn_ForwardVectorCB.SetData(spawnForwardVectorsArray, 0, 0, particlesSpawnCount);
        m_partcilesToSpawn_UpVectorCB.SetData(spawnUpVectorsArray, 0, 0, particlesSpawnCount);
        UnityEngine.Profiling.Profiler.EndSample();

        // TODO MOVE THIS TO INIT, SHOULD ONLY NEED TO BE SET ONCE

        m_particleGenerationCS.SetInt("_particlesTotalCount", m_computeParticlesSimulationManager.GetParticleCount());
        m_particleGenerationCS.SetInt("_partcilesPerThread", m_computeParticlesSimulationManager.GetParticlesPerThread());
        m_particleGenerationCS.SetInt("k_threadGroupCount_x", m_computeParticlesSimulationManager.GetThreadGroupCount_X());
        m_particleGenerationCS.SetInt("k_threadGroupCount_y", m_computeParticlesSimulationManager.GetThreadGroupCount_Y());


        m_particleGenerationCS.SetBuffer(kernelIndex, "_positions", m_computeParticlesSimulationManager.GetPositionsCB());
        m_particleGenerationCS.SetBuffer(kernelIndex, "_velocities", m_computeParticlesSimulationManager.GetVelocitiesCB());
        m_particleGenerationCS.SetBuffer(kernelIndex, "_colors", m_computeParticlesSimulationManager.GetColorsCB());
        m_particleGenerationCS.SetBuffer(kernelIndex, "_forwardVectors", m_computeParticlesSimulationManager.GetForwardVectorsCB());
        m_particleGenerationCS.SetBuffer(kernelIndex, "_upVectors", m_computeParticlesSimulationManager.GetUpVectorsCB());

        m_particleGenerationCS.SetBuffer(kernelIndex, "_spawnPositions", m_particlesToSpawn_PositionsCB);
        m_particleGenerationCS.SetBuffer(kernelIndex, "_spawnVelocities", m_particlesToSpawn_VelocitiesCB);
        m_particleGenerationCS.SetBuffer(kernelIndex, "_spawnColors", m_particlesToSpawn_ColorsCB);
        m_particleGenerationCS.SetBuffer(kernelIndex, "_spawnForwardVectors", m_particlesToSpawn_ForwardVectorCB);
        m_particleGenerationCS.SetBuffer(kernelIndex, "_spawnUpVectors", m_partcilesToSpawn_UpVectorCB);

        m_particleGenerationCS.SetBuffer(kernelIndex, "_particlesDeadListBuffer_Consume", m_computeParticlesSimulationManager.GetParticlesDeadListBuffer());
        m_particleGenerationCS.SetBuffer(kernelIndex, "_particlesAliveListBuffer_Append", m_computeParticlesSimulationManager.GetParticlesAliveListBuffer_Consume()); //a?
        //m_particleGenerationCS.SetBuffer(kernelIndex, "_particlesAliveListCounter", m_computeParticlesSimulationManager.GetParticlesDeadListBuffer());
        m_particleGenerationCS.SetInt("_particlesSpawnCount", particlesSpawnCount);

        m_particleGenerationCS.SetBuffer(kernelIndex, "_particlesAliveListCounter", m_computeParticlesSimulationManager.GetFreshParticlesAliveCounterBuffer());
        m_particleGenerationCS.SetBuffer(kernelIndex, "_particlesDeadListCounter", m_computeParticlesSimulationManager.GetFreshParticlesDeadCounterBuffer());

        UnityEngine.Profiling.Profiler.BeginSample("particle gen dispatch");
        m_particleGenerationCS.Dispatch(kernelIndex, m_computeParticlesSimulationManager.GetThreadGroupCount_X(), m_computeParticlesSimulationManager.GetThreadGroupCount_Y(), 1);
        UnityEngine.Profiling.Profiler.EndSample();

        UnityEngine.Profiling.Profiler.EndSample();
        //Debug.Log(particlesSpawnCount);
    }

    // old brute force approach
    public void SpawnParticlesBasedFromListAndInjection()
    {
        UnityEngine.Profiling.Profiler.BeginSample("SpawnParticlesBasedFromListAndInjection");
        
        
        UnityEngine.Profiling.Profiler.BeginSample("Setting CBs");
        m_particlesToSpawn_PositionsCB.SetData( m_particlesToSpawnPositionsArray, 0, 0, m_particlesSpawnedThisFrameListCount);
        m_particlesToSpawn_VelocitiesCB.SetData( m_particlesToSpawnVelocitiesArray, 0, 0, m_particlesSpawnedThisFrameListCount);
        m_particlesToSpawn_ColorsCB.SetData( m_particlesToSpawnColorsArray, 0, 0, m_particlesSpawnedThisFrameListCount);
        m_particlesToSpawn_ForwardVectorCB.SetData(m_particlesToSpawnForwardVectorsArray, 0, 0, m_particlesSpawnedThisFrameListCount);
        m_partcilesToSpawn_UpVectorCB.SetData( m_particlesToSpawnUpVectorsArray, 0, 0, m_particlesSpawnedThisFrameListCount);

        UnityEngine.Profiling.Profiler.EndSample();

        m_particleGenerationCS.SetInt("_particlesTotalCount", m_computeParticlesSimulationManager.GetParticleCount());
        m_particleGenerationCS.SetInt("_partcilesPerThread", m_computeParticlesSimulationManager.GetParticlesPerThread());
        m_particleGenerationCS.SetInt("k_threadGroupCount_x", m_computeParticlesSimulationManager.GetThreadGroupCount_X());
        m_particleGenerationCS.SetInt("k_threadGroupCount_y", m_computeParticlesSimulationManager.GetThreadGroupCount_Y());

        
        //UnityEngine.Profiling.Profiler.BeginSample("kinect false set buffers");
        m_particleGenerationCS.SetBuffer(0, "_spawnPositions", m_particlesToSpawn_PositionsCB);
        m_particleGenerationCS.SetBuffer(0, "_spawnVelocities", m_particlesToSpawn_VelocitiesCB);
        m_particleGenerationCS.SetBuffer(0, "_spawnColors", m_particlesToSpawn_ColorsCB);
        m_particleGenerationCS.SetBuffer(0, "_spawnForwardVectors", m_particlesToSpawn_ForwardVectorCB);
        m_particleGenerationCS.SetBuffer(0, "_spawnUpVectors", m_partcilesToSpawn_UpVectorCB);
        m_particleGenerationCS.SetInt("_spawnStartIndex", m_currentSpawnIndex);
        m_particleGenerationCS.SetInt("_spawnCount", m_particlesSpawnedThisFrameListCount);
        //UnityEngine.Profiling.Profiler.EndSample();
        

        UnityEngine.Profiling.Profiler.BeginSample("gen set buffers");

        m_particleGenerationCS.SetBuffer(0, "_positions", m_computeParticlesSimulationManager.GetPositionsCB());
        m_particleGenerationCS.SetBuffer(0, "_velocities", m_computeParticlesSimulationManager.GetVelocitiesCB());
        m_particleGenerationCS.SetBuffer(0, "_colors", m_computeParticlesSimulationManager.GetColorsCB());
        m_particleGenerationCS.SetBuffer(0, "_forwardVectors", m_computeParticlesSimulationManager.GetForwardVectorsCB());
        m_particleGenerationCS.SetBuffer(0, "_upVectors", m_computeParticlesSimulationManager.GetUpVectorsCB());
        UnityEngine.Profiling.Profiler.EndSample();

        UnityEngine.Profiling.Profiler.BeginSample("particle gen dispatch");
        m_particleGenerationCS.Dispatch(0, m_computeParticlesSimulationManager.GetThreadGroupCount_X(), m_computeParticlesSimulationManager.GetThreadGroupCount_Y() , 1);
        UnityEngine.Profiling.Profiler.EndSample();
        //Debug.Log("Frame: " + Time.frameCount + " | Count: " + m_particlesSpawnedThisFrameCount);

        // TODO: temporarily disabled LerpTargetManaget set while I fugure out a new way to do it  
        UnityEngine.Profiling.Profiler.BeginSample("set lerp target dispatch");
       // m_computeParticlesLerpToTargetManager.SetLerpTargetBuffers(m_particlesToSpawnPositionsArray, m_particlesToSpawnVelocitiesArray, m_particlesToSpawnColorsArray, m_particlesToSpawnUpVectorsArray, m_currentSpawnIndex, m_particlesSpawnedThisFrameListCount);
        UnityEngine.Profiling.Profiler.EndSample();

        UnityEngine.Profiling.Profiler.EndSample();
        
    }

    public void AddNewPartcileToSpawnList(Vector3 spawnPos, Vector3 spawnVel, Color spawnCol, Vector3 spawnForwardVec, Vector3 spawnUpVec, float spawnLife, float spawnScale)
    {
        int spawnArrayIndex = m_particlesSpawnedThisFrameListCount;
        spawnArrayIndex = Mathf.Clamp(spawnArrayIndex, 0, m_computeParticlesSimulationManager.GetParticleCount() - 1);

        // remenant from list refactor test, ,might be slower than original array element direct set
        Vector4 dummy = new Vector4();
        
        dummy.x = spawnPos.x;
        dummy.y = spawnPos.y;
        dummy.z = spawnPos.z;
        dummy.w = spawnLife;
        m_particlesToSpawnPositionsArray[spawnArrayIndex] = dummy;

        dummy.x = spawnVel.x;
        dummy.y = spawnVel.y;
        dummy.z = spawnVel.z;
        dummy.w = spawnScale;
        m_particlesToSpawnVelocitiesArray[spawnArrayIndex] = dummy;

        dummy.x = spawnCol.r;
        dummy.y = spawnCol.b;
        dummy.z = spawnCol.g;
        dummy.w = spawnCol.a;
        m_particlesToSpawnColorsArray[spawnArrayIndex] = dummy;

        dummy.x = spawnForwardVec.x;
        dummy.y = spawnForwardVec.y;
        dummy.z = spawnForwardVec.z;
        dummy.w = 0;
        m_particlesToSpawnForwardVectorsArray[spawnArrayIndex] = dummy;

        // store original particle life in w, does not get decrement, used to get 0-1 ratio
        dummy.x = spawnUpVec.x;
        dummy.y = spawnUpVec.y;
        dummy.z = spawnUpVec.z;
        dummy.w = spawnLife;
        m_particlesToSpawnUpVectorsArray[spawnArrayIndex] = dummy;

        m_currentSpawnIndex++;
        m_currentSpawnIndex = m_currentSpawnIndex % m_computeParticlesSimulationManager.GetParticleCount();
        m_particlesSpawnedThisFrameListCount++;
    }


    void OnDestroy()
    {
        if (m_particlesToSpawn_PositionsCB != null)
        {
            m_particlesToSpawn_PositionsCB.Release();
            m_particlesToSpawn_VelocitiesCB.Release();
            m_particlesToSpawn_ColorsCB.Release();
            m_particlesToSpawn_ForwardVectorCB.Release();
            m_partcilesToSpawn_UpVectorCB.Release();

            m_particlesToSpawn_PositionsCB.Dispose();
            m_particlesToSpawn_VelocitiesCB.Dispose();
            m_particlesToSpawn_ColorsCB.Dispose();
            m_particlesToSpawn_ForwardVectorCB.Dispose();
            m_partcilesToSpawn_UpVectorCB.Dispose();

            m_transformInverseDataCB.Release();

            m_transformInverseDataCB.Dispose();

            m_particleSpawnDataAB.Release();
            m_particleSpawnDataCounter.Release();

            m_particleSpawnDataAB.Dispose();
            m_particleSpawnDataCounter.Dispose();
        }                
    }


}
