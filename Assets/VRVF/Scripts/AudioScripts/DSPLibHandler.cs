﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using DSPLib;

using UnityEngine;

public class DSPLibHandler
{
    FFT m_dspFFT = new FFT();
    double[] m_windowCoeffs_Double;
    float[] m_windowCoeffs_Float;
    double m_windowScaleFactor_Double;
    float m_windowScaleFactor_Float;

    //double[] m_waveformData_Raw;
    float[] m_waveformData_Windowed;
    
    Complex[] m_spectrumData_Complex;
    double[] m_spectrumData_Double;
    float[] m_spectrumData_Float;

    // caches
    Complex[] m_unswizzle;

    // assuing data length is power of 2
    public void InitDSP(uint fftDataLength)
    {
        uint dataLength = 2 * fftDataLength;
        m_unswizzle = new Complex[dataLength];
        m_dspFFT.Initialize(dataLength);
        m_windowCoeffs_Double = new double[dataLength];
        m_windowCoeffs_Float = new float[dataLength];
        //m_waveformData_Raw = new double[dataLength];
        m_waveformData_Windowed = new float[dataLength];

        m_spectrumData_Complex = new Complex[dataLength];
        m_spectrumData_Double = new double[dataLength];

        m_spectrumData_Float = new float[fftDataLength];

        m_windowCoeffs_Double = DSP.Window.Coefficients(DSP.Window.Type.Hann, dataLength);
        m_windowScaleFactor_Double = DSP.Window.ScaleFactor.Signal(m_windowCoeffs_Double);

        for (int i = 0; i < m_windowCoeffs_Double.Length; i++)
            m_windowCoeffs_Float[i] = (float)m_windowCoeffs_Double[i];
        m_windowScaleFactor_Float = (float)m_windowScaleFactor_Double;
    }

    public float[] Get_WindowCoeffs()
    {
        return m_windowCoeffs_Float;
    }

    public float Get_WindowScaleFactor()
    {
        return m_windowScaleFactor_Float;
    }

    public void ExecuteFFT(float[] waveformData)
    {        

        UnityEngine.Profiling.Profiler.BeginSample("Apply Windowing");
        // apply windowing
        //m_waveformData_Windowed = DSP.Math.Multiply(m_waveformData_Raw, m_windowCoeffs);
        for (int i = 0; i < waveformData.Length; i++)
            m_waveformData_Windowed[i] = waveformData[i] * m_windowCoeffs_Float[i];                    
        UnityEngine.Profiling.Profiler.EndSample();

        UnityEngine.Profiling.Profiler.BeginSample("FFT Execute");
        // do FFT
        m_dspFFT.Execute_SingleFloatMagnitude(m_waveformData_Windowed, m_unswizzle, m_spectrumData_Float);
        UnityEngine.Profiling.Profiler.EndSample();
        
        UnityEngine.Profiling.Profiler.BeginSample("Multiply Scale Factor");
        // scale to compensate for windowing
        //m_spectrumData_Double = DSP.Math.Multiply(m_spectrumData_Double, m_windowScaleFactor_Double);
        for (int i = 0; i < m_spectrumData_Float.Length; i++)
            m_spectrumData_Float[i] *= m_windowScaleFactor_Float;
        UnityEngine.Profiling.Profiler.EndSample();

        //Debug.Log("Exectute FFT END");
    }

    public float[] GetFFTData_Float()
    {
        return m_spectrumData_Float;
    }


}
