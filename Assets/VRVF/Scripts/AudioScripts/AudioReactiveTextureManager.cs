﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AudioReactiveTextureManager : MonoBehaviour
{    
    public Texture2D m_heightDataTexture;
    // assuming fd also is using 128 fft height data
    // data slices set in width
    const int k_textureHeight = 128;
    const int k_textureWidth = 128;

    // float version
    float[] m_heightDataArray_Float;
    float[] m_heightDataArray_Float_Scrolling;
    float[] m_heightDataArray_Float_InPlace;
    byte[] m_heightDataTextureBuffer_Float_Scrolling;
    byte[] m_heightDataTextureBuffer_Float_InPlace;
    int m_currentHeightDataInsertIndex = 0;

    public Texture2D m_colorDataTexture;
    Color[] m_colorDataArray_Scrolling;
    Color[] m_colorDataArray_InPlace;
    int m_currentColorDataInsertionIndex = 0;

    public Material m_testHeightMapMat;
    public Material m_testColorDataMat;
    public Material[] m_colorHeightNormalsMaterials;

    FrequencyDataManager m_frequencyDataManager;

    public ComputeShader m_heightToNormalsCS;
    public RenderTexture m_normalsDataRenderTexture;
    
    public float m_heightScale = 1;

    public enum TextureDataInsertType
    {
        InPlace,
        Scrolling
    }
    public TextureDataInsertType m_textureDataInsertType = TextureDataInsertType.InPlace;

    void Start()
    {
        m_heightDataTexture = new Texture2D(k_textureWidth, k_textureHeight, TextureFormat.RFloat, false);
        m_heightDataTexture.name = "ARTM_HeightDataTexture";
        m_heightDataTexture.filterMode = FilterMode.Trilinear;
        
        m_heightDataArray_Float = new float[k_textureWidth];
        m_heightDataArray_Float_Scrolling = new float[k_textureWidth * k_textureHeight];
        m_heightDataArray_Float_InPlace = new float[k_textureWidth * k_textureHeight];
        m_heightDataTextureBuffer_Float_Scrolling = new byte[(sizeof(float) / sizeof(byte)) * k_textureWidth * k_textureHeight];
        m_heightDataTextureBuffer_Float_InPlace = new byte[(sizeof(float) / sizeof(byte)) * k_textureWidth * k_textureHeight];

        m_colorDataTexture = new Texture2D(1, k_textureHeight, TextureFormat.RGBA32, false);
        m_colorDataTexture.name = "ARTM_ColorDataTexture";
        m_colorDataArray_Scrolling = new Color[k_textureWidth];
        m_colorDataArray_InPlace = new Color[k_textureWidth];
        
        m_normalsDataRenderTexture = new RenderTexture(128, 128, 0, RenderTextureFormat.ARGBFloat);
        m_normalsDataRenderTexture.name = "NormalsDataTexture";
        m_normalsDataRenderTexture.enableRandomWrite = true;
        m_normalsDataRenderTexture.antiAliasing = 4; // trying for no AA
        m_normalsDataRenderTexture.Create();
        
        m_heightToNormalsCS.SetTexture(0, "_HeightDataTexture", m_heightDataTexture);
        m_heightToNormalsCS.SetTexture(0, "_NormalsTexture", m_normalsDataRenderTexture);
        m_heightToNormalsCS.SetFloat("_HeightScale", m_heightScale);
        m_heightToNormalsCS.Dispatch(0, 16, 16, 1);

        m_testHeightMapMat.mainTexture = m_heightDataTexture;
        m_testColorDataMat.mainTexture = m_colorDataTexture;

        for (int i = 0; i < m_colorHeightNormalsMaterials.Length; i++)
        {
            m_colorHeightNormalsMaterials[i].mainTexture = m_colorDataTexture;
            m_colorHeightNormalsMaterials[i].SetTexture("_HeightTex", m_heightDataTexture);
            m_colorHeightNormalsMaterials[i].SetTexture("_NormalsTex", m_normalsDataRenderTexture);
            m_colorHeightNormalsMaterials[i].SetFloat("_HeightScale", m_heightScale);
        }

        m_frequencyDataManager = FindObjectOfType<FrequencyDataManager>();
    }

    void ConvertHeightDataTo_Alpha8(float[] orignalFloatData, byte[] destination8BitArray)
    {
        for (int i = 0; i < orignalFloatData.Length; i++)
        {
            destination8BitArray[i] = (byte)(255.0f * orignalFloatData[i]);
        }
    }


    void CopyHeightDataTo_Float(float[] orignalFloatData, float[] destinationFloatArray)
    {
        for (int i = 0; i < orignalFloatData.Length; i++)
        {
            destinationFloatArray[i] = orignalFloatData[i];
        }
    }

    // not doing error checks for out of bounds index
    void InsertFreshHeightDataIntoTextureBuffer_Alpha8(byte[] heightDataArray, byte[] textureBuffer, int insertionIndex)
    {
        for (int i = 0; i < heightDataArray.Length; i++)
        {
            textureBuffer[i + insertionIndex] = heightDataArray[i];
        }
    }

    void InsertFreshHeightDataIntoHistoryArray_Float(float[] heightDataArray, float[] historyArray, int insertionIndex)
    {
        for (int i = 0; i < heightDataArray.Length; i++)
        {
            historyArray[i + insertionIndex] = heightDataArray[i];
        }
    }



    void HeightDataArraySetInPlace_Alpha8(byte[] scrollingArray, byte[] inplaceArray, int insertIndex)
    {
        for (int i = 0; i < k_textureHeight; i++)
        {
            int insertRow = insertIndex / k_textureWidth;
            // crazy amount formula to avoid if branch, maybe not best option
            //int rowIndex = ((1 - Mathf.Clamp((insertRow - i), 0, 1)) * k_textureHeight + (insertRow - i)) % (k_textureHeight);            
            int rowIndex = insertRow - i;
            if (rowIndex < 0)
                rowIndex += k_textureHeight;

            for (int j = 0; j < k_textureWidth; j++)
            {
                inplaceArray[i * k_textureWidth + j] = scrollingArray[rowIndex * k_textureHeight + j];
            }
        }
    }

    void HeightDataArraySetInPlace_Float(float[] scrollingHistoryArray, float[] inplaceHistoryArray, int insertIndex)
    {
        for (int i = 0; i < k_textureHeight; i++)
        {
            int insertRow = insertIndex / k_textureWidth;
            int rowIndex = insertRow - i;
            if (rowIndex < 0)
                rowIndex += k_textureHeight;

            for (int j = 0; j < k_textureWidth; j++)
            {
                inplaceHistoryArray[i * k_textureWidth + j] = scrollingHistoryArray[rowIndex * k_textureHeight + j];
            }
        }
    }

    void InsertFreshColorDataIntoCollorArray(Color[] colorDataArray, Color freshColor, int insertionIndex)
    {
        colorDataArray[insertionIndex] = freshColor;
    }

    void ColorDataArraySetInPlace(Color[] scrollingArray, Color[] inplaceArray, int insertionIndex)
    {
        for (int i = 0; i < inplaceArray.Length; i++)
        {
            int scrollingIndex = ((1 - Mathf.Clamp((insertionIndex - i), 0, 1)) * inplaceArray.Length + (insertionIndex - i)) % (inplaceArray.Length);
            inplaceArray[i] = scrollingArray[scrollingIndex];
        }
    }

    void Update()
    {
        /// do height data
        // update with fresh values from FDM        
        CopyHeightDataTo_Float(m_frequencyDataManager.m_processedFFTDataArray, m_heightDataArray_Float);        
        InsertFreshHeightDataIntoHistoryArray_Float(m_heightDataArray_Float, m_heightDataArray_Float_Scrolling, m_currentHeightDataInsertIndex);        
        HeightDataArraySetInPlace_Float(m_heightDataArray_Float_Scrolling, m_heightDataArray_Float_InPlace, m_currentHeightDataInsertIndex);
        m_currentHeightDataInsertIndex = (m_currentHeightDataInsertIndex + k_textureWidth) % (k_textureWidth * k_textureHeight);

        /// RFLOAT
        // copy float data into byte buffers, because no SetFloats functions exists
        Buffer.BlockCopy(m_heightDataArray_Float_Scrolling, 0, m_heightDataTextureBuffer_Float_Scrolling, 0, m_heightDataTextureBuffer_Float_Scrolling.Length);
        Buffer.BlockCopy(m_heightDataArray_Float_InPlace, 0, m_heightDataTextureBuffer_Float_InPlace, 0, m_heightDataTextureBuffer_Float_InPlace.Length);

        if (m_textureDataInsertType == TextureDataInsertType.InPlace)
        {
            // in place
            m_heightDataTexture.LoadRawTextureData(m_heightDataTextureBuffer_Float_InPlace);

        }
        else if (m_textureDataInsertType == TextureDataInsertType.Scrolling)
        {
            // scrolling
            m_heightDataTexture.LoadRawTextureData(m_heightDataTextureBuffer_Float_Scrolling);
        }

        m_heightDataTexture.Apply();


        //// ~~~~~

        /// do color data
        Color freshFFTColor = m_frequencyDataManager.m_currentFFTColor;
        InsertFreshColorDataIntoCollorArray(m_colorDataArray_Scrolling, freshFFTColor, m_currentColorDataInsertionIndex);
        ColorDataArraySetInPlace(m_colorDataArray_Scrolling, m_colorDataArray_InPlace, m_currentColorDataInsertionIndex);


        if (m_textureDataInsertType == TextureDataInsertType.InPlace)
        {
            // inplace style
            m_colorDataTexture.SetPixels(m_colorDataArray_InPlace);

        }
        else if (m_textureDataInsertType == TextureDataInsertType.Scrolling)
        {
            // scrolling style
            //m_colorDataTexture.SetPixel( 0, m_currentColorDataInsertionIndex, freshFFTColor);
            m_colorDataTexture.SetPixels(m_colorDataArray_Scrolling);
        }

        m_currentColorDataInsertionIndex = (m_currentColorDataInsertionIndex + 1) % k_textureHeight;
        m_colorDataTexture.Apply();

        for (int i = 0; i < m_colorHeightNormalsMaterials.Length; i++)
        {
            m_colorHeightNormalsMaterials[i].SetFloat("_HeightScale", m_heightScale);
        }

        m_heightToNormalsCS.SetFloat("_HeightScale", m_heightScale);
        m_heightToNormalsCS.Dispatch(0, 16, 16, 1);


    }





}
