﻿using System.Collections;
using System.Collections.Generic;
using FFTWSharp;

public class FFTWSharpHandler
{
    // managed arrays
    float[] m_fin;
    float[] m_fout;

    fftwf_plan m_fftwfPlan;
    fftwf_plan m_fftwfPlan_r2c;
    fftwf_plan m_fftwPlan_c2r;

    fftwf_complexarray m_fin_CA;
    fftwf_complexarray m_fout_CA;

    float[] m_fftOutputReal;

    float[] m_cached_GetDataReal_dataf;
    float[] m_cached_GetDataReal_data;
    float[] m_cached_SetZeroData_datain;

    public void InitFFTWSharpHandler(uint fftRealDataOutputSize)
    {
        m_fin = new float[fftRealDataOutputSize * 2];
        m_fout = new float[fftRealDataOutputSize * 2];
        m_fin_CA = new fftwf_complexarray(m_fin);
        m_fout_CA = new fftwf_complexarray(m_fout);

        m_cached_GetDataReal_dataf = new float[fftRealDataOutputSize * 2];
        m_cached_GetDataReal_data = new float[fftRealDataOutputSize * 1];
        m_cached_SetZeroData_datain = new float[fftRealDataOutputSize * 2];

        m_fftOutputReal = new float[fftRealDataOutputSize];

        m_fftwfPlan = fftwf_plan.dft_1d((int)fftRealDataOutputSize, m_fin_CA, m_fout_CA, fftw_direction.Forward, fftw_flags.Measure);
        m_fftwfPlan_r2c = fftwf_plan.dft_r2c_1d((int)fftRealDataOutputSize, m_fin_CA, m_fout_CA, fftw_flags.Measure);
        m_fftwPlan_c2r = fftwf_plan.dft_c2r_1d((int)fftRealDataOutputSize, m_fin_CA, m_fout_CA, fftw_direction.Forward, fftw_flags.Measure);
    }

    public void ExecuteFFT(float[] waveformData, float windowScaleFactor)
    {
        // assuming waveformData is same length as fftRealDataOutputSize from Init
        // redundant?
        //System.Array.Copy(waveformData, m_fin, waveformData.Length);
        UnityEngine.Profiling.Profiler.BeginSample("waveform copy");
        for (int i = 0; i < waveformData.Length; i++)
            m_fin[i * 2] = waveformData[i];
        UnityEngine.Profiling.Profiler.EndSample();

        // setup input/output data arrays
        UnityEngine.Profiling.Profiler.BeginSample("Set Data");
        m_fin_CA.SetData(m_fin);
        m_fout_CA.SetZeroData_Cached(m_cached_SetZeroData_datain);
        UnityEngine.Profiling.Profiler.EndSample();
        

        // execute FFT
        UnityEngine.Profiling.Profiler.BeginSample("Execute");
        m_fftwfPlan.Execute();
        //m_fftwfPlan_r2c.Execute();
        //m_fftwPlan_c2r.Execute();
        UnityEngine.Profiling.Profiler.EndSample();

        // discard imaginary data, only keep real part
        UnityEngine.Profiling.Profiler.BeginSample("Get Data");
        m_fftOutputReal = m_fout_CA.GetData_Power_Cached_Scaled(m_cached_GetDataReal_dataf, m_cached_GetDataReal_data, windowScaleFactor);
        UnityEngine.Profiling.Profiler.EndSample();
    }

    public float[] Get_FFTDataReal()
    {
        return m_fftOutputReal;
    }

}
