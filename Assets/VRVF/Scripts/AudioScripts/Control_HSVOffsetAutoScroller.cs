﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control_HSVOffsetAutoScroller : MonoBehaviour
{
    FrequencyDataManager m_fdm;
    public float m_scrollSpeedScale = 1.0f;


    void Start()
    {
        m_fdm = FindObjectOfType<FrequencyDataManager>();
    }

    void Update()
    {
        m_fdm.m_colorHueOffset += m_scrollSpeedScale * m_fdm.m_volumeTemporalMaxima * Time.deltaTime;
        m_fdm.m_colorHueOffset = Mathf.Repeat(m_fdm.m_colorHueOffset, 360.0f);
    }
    

}
