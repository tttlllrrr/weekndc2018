﻿using System.Collections;
using System.Collections.Generic;

public class ManualFFTHandler 
{
    DSPLibHandler m_dspLibHandler;
    FFTWSharpHandler m_fftwSharpHandler;

    float[] m_manualFFTDataReal;
    float[] m_waveformDataWindowed;

    public void InitManualFFTHandler(uint fftRealDataOutputCount)
    {
        m_manualFFTDataReal = new float[fftRealDataOutputCount];
        m_waveformDataWindowed = new float[fftRealDataOutputCount];
        m_dspLibHandler = new DSPLibHandler();
        m_fftwSharpHandler = new FFTWSharpHandler();

        m_dspLibHandler.InitDSP(fftRealDataOutputCount);
        m_fftwSharpHandler.InitFFTWSharpHandler(fftRealDataOutputCount);
    }

    public void ExecuteManualFFT(float[] waveformDataRaw)
    {
        UnityEngine.Profiling.Profiler.BeginSample("Windowing");
        // do windowing on waveform data first
        float[] windowCoeffs = m_dspLibHandler.Get_WindowCoeffs();
        for (int i = 0; i < m_waveformDataWindowed.Length; i++)
            m_waveformDataWindowed[i] = waveformDataRaw[i] * windowCoeffs[i];
        UnityEngine.Profiling.Profiler.EndSample();

        UnityEngine.Profiling.Profiler.BeginSample("FFT execcute");
        // do FFT on windowed data
        m_fftwSharpHandler.ExecuteFFT(m_waveformDataWindowed, m_dspLibHandler.Get_WindowScaleFactor());
        UnityEngine.Profiling.Profiler.EndSample();
        
        // meh?
        m_manualFFTDataReal = m_fftwSharpHandler.Get_FFTDataReal();
    }

    public float[] Get_ManualFFTDataReal()
    {
       return  m_manualFFTDataReal;
    }


}
