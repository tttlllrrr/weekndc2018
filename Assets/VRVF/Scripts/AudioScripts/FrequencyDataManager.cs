﻿using UnityEngine;
using System.Collections;
using Lasp;

// from FD
public class FrequencyDataManager : MonoBehaviour
{

    const int m_rawFFTDataSize = 8192;
    float[] m_currentRawFFTDataArray;

    const int m_processedFFTDataSize = 128; // separated into 8 sections
    public float[] m_processedFFTDataArray;
    float[] m_previousProcessedFFTDataArray;

    public int[] m_samplesAccumulationPerSectionArray = { 1, 2, 4, 6, 8, 10, 14, 18, 20, 28, 36, 52, 64, 78, 84, 128 }; // 16 parts version // { 2, 2, 5, 9, 12, 25, 55, 175 }; // 8 parts version
    public int m_samplesAccumulationStartIndexOffset = 0;
    public AnimationCurve m_sectionScalerCurve = AnimationCurve.EaseInOut(0, 1, 1, 20);
    public float[] m_sectionsScalerArray = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };// 16 parts version //{ 1, 1, 1, 1, 1, 1, 1, 1 };// 8 parts version
    public float m_globalFFTDataScaler = 1.0f;
    public float m_globalVolumeDataScaler = 1.0f;

    public enum AudioDataSources
    {
        AudioListener,
        Microphone,
        StereoMix,
        AudioSource,
        LASP
    };

    public AudioDataSources m_audioDataSource;
    public AudioSource m_targetAudioSource;
    LiveAudioDataManager m_liveAudioDataManager;

    public AnimationCurve m_rValueCurve;
    public AnimationCurve m_gValueCurve;
    public AnimationCurve m_bValueCurve;

    const float k_maxColorMagnitude = 1.27f;
    [Range(0, k_maxColorMagnitude)]
    public float d_r;
    [Range(0, k_maxColorMagnitude)]
    public float d_g;
    [Range(0, k_maxColorMagnitude)]
    public float d_b;

    public Color m_currentFFTColor;
    [Range(0, 1)]
    public float m_currentVolume = 0;
    public Vector3 m_currentFFTColAsVec3;
    public Vector3 m_smoothDampedFFTColAsVec3;
    public Vector3 m_sdFFTColVec3Vel;

    float m_rScaler = 1.50f;
    float m_gScaler = 1.0f;
    float m_bScaler = 1.0f;
    public float m_colorScaler = 25.0f;

    float m_localPeakValueThreashold = 0.15f;
    float m_localPeakRedistributionRatio = 0.25f;
    public int m_peakValuesSpreadItterations = 3;

    const int m_outputSamplesCount = 1024;
    float[] m_outputSamplesArray;
    int k_vollumeRollingAverageCount = 5;
    int m_vollumeRollingAverageIndex = 0;
    float[] m_volumeRollingAverageArray;
    float m_vollumeRollingAverage = 0;

    public float m_colorHueOffset = 0;
    public float m_colorSaturationBoost = 0;
    [Range(0, 1)]
    public float m_volumeTemporalMaxima = 0;
    public float m_volumeTemporalMaximaDecay = 0.421f;
    [Range(0,1)]
    public float m_smoothDampedVolTemporalMax = 0;
    public float m_sdvtmVel = 0;

    public float k_volSmoothDampTime = 0.1f;

    

    [Range(0, 1)]
    public float m_historicalSmoothing = 0.2f;

    float[] m_laspWaveformArray;
    ManualFFTHandler m_manualFFTHandler;


    void Start()
    {
        m_currentRawFFTDataArray = new float[m_rawFFTDataSize];
        
        m_liveAudioDataManager = FindObjectOfType<LiveAudioDataManager>();

        m_processedFFTDataArray = new float[m_processedFFTDataSize];
        m_previousProcessedFFTDataArray = new float[m_processedFFTDataSize];

        m_outputSamplesArray = new float[m_outputSamplesCount];

        m_volumeRollingAverageArray = new float[k_vollumeRollingAverageCount];

        m_manualFFTHandler = new ManualFFTHandler();
        m_manualFFTHandler.InitManualFFTHandler(m_rawFFTDataSize);
        m_laspWaveformArray = new float[m_rawFFTDataSize * 2];
    }

    void Update()
    {
        GetFreshFFTData();
        GetFreshRGB();
        GetFreshVolume();
        CalculateVolumeTemporalMaxima();
    }

    void CalculateVolumeTemporalMaxima()
    {
        
        if (m_vollumeRollingAverage > m_volumeTemporalMaxima)
            m_volumeTemporalMaxima = m_vollumeRollingAverage;
        else
            m_volumeTemporalMaxima = Mathf.Clamp(m_volumeTemporalMaxima - m_volumeTemporalMaximaDecay * Time.deltaTime, 0, 1);
            
        
        m_smoothDampedVolTemporalMax = Mathf.SmoothDamp(m_smoothDampedVolTemporalMax, m_volumeTemporalMaxima, ref m_sdvtmVel, k_volSmoothDampTime);

    }

    void ProcessRawFFTData()
    {
        int accumulationIndex = 0;
        int accumulationInterval = m_processedFFTDataArray.Length / m_samplesAccumulationPerSectionArray.Length;
        float tempSum = 0;
        int rawFFTIndex = 0;
        int currentRawSamplesPerProcessedPoint = 0;

        for (int i = 0; i < m_sectionsScalerArray.Length; i++)
        {
            float step = (float)i / (float)m_sectionsScalerArray.Length;
            m_sectionsScalerArray[i] =  m_sectionScalerCurve.Evaluate(step);
        }


        for (int i = 0; i < m_processedFFTDataArray.Length; i++)
        {
            if (i % accumulationInterval == 0 && i != 0)
                accumulationIndex += 1;

            tempSum = 0;
            currentRawSamplesPerProcessedPoint = m_samplesAccumulationPerSectionArray[accumulationIndex];
            for (int j = 0; j < currentRawSamplesPerProcessedPoint; j++)
            {
                tempSum += m_sectionsScalerArray[accumulationIndex] * m_currentRawFFTDataArray[rawFFTIndex + m_samplesAccumulationStartIndexOffset];
                rawFFTIndex += 1;
            }
            m_processedFFTDataArray[i] = Mathf.Clamp(m_globalFFTDataScaler * tempSum, 0, 1); ///(float)currentRawSamplesPerProcessedPoint;
            //Debug.Log("Sum for acuumuator: " + currentRawSamplesPerProcessedPoint + " , " + tempSum);

            // averageing with previous to smooth out depth axis and hide repeat data
            m_processedFFTDataArray[i] = ( (1.0f - m_historicalSmoothing) * m_processedFFTDataArray[i] + m_historicalSmoothing * m_previousProcessedFFTDataArray[i]);
            m_previousProcessedFFTDataArray[i] = m_processedFFTDataArray[i];
        }

        SpreadOutPeakValues();
    }

    void SpreadOutPeakValues()
    {
        int spreadCounter = 0;
        float prevDelta = 0;
        float postDelta = 0;

        // Do multiple passes
        for (int j = 0; j < m_peakValuesSpreadItterations; j++)
        {
            // TODO: handle cases for high values at begining and end of array
            for (int i = 1; i < m_processedFFTDataArray.Length - 1; i++)
            {
                float amountToRedistributePerSide = 0.5f * m_localPeakRedistributionRatio * m_processedFFTDataArray[i];
                prevDelta = m_processedFFTDataArray[i] - m_processedFFTDataArray[i - 1];
                postDelta = m_processedFFTDataArray[i] - m_processedFFTDataArray[i + 1];

                if (prevDelta > m_localPeakValueThreashold)
                {
                    spreadCounter++;
                    m_processedFFTDataArray[i] -= amountToRedistributePerSide;
                    m_processedFFTDataArray[i - 1] += 0.5f * amountToRedistributePerSide;
                }
                if (postDelta > m_localPeakValueThreashold)
                {
                    spreadCounter++;
                    m_processedFFTDataArray[i] -= amountToRedistributePerSide;
                    m_processedFFTDataArray[i + 1] += 0.5f * amountToRedistributePerSide;
                }
            }
        }

        //if(spreadCounter > 0)
        //	Debug.LogError("spread: " + spreadCounter );
    }

    Color GetFreshRGB()
    {
        int subdivisionIndex = 0;
        int subdivisionInterval = m_processedFFTDataArray.Length / m_samplesAccumulationPerSectionArray.Length;

        // goes through subdivisions 0 - 3
        float r = 0;
        int r_SubdivisionStart = 0;
        int r_SubdivisionEnd = m_samplesAccumulationPerSectionArray.Length/ 2; //4;
        float r_weight = 1.0f / ((r_SubdivisionEnd - r_SubdivisionStart + 1) * (float)subdivisionInterval);

        // goes through subdivisions 3 - 6
        float g = 0;
        int g_SubdivisionStart = m_samplesAccumulationPerSectionArray.Length / 4; //2;
        int g_SubdivisionEnd = 3*m_samplesAccumulationPerSectionArray.Length / 4;
        float g_weight = 1.0f / ((g_SubdivisionEnd - g_SubdivisionStart + 1) * (float)subdivisionInterval);

        // goes through subdivisions 4 - 7
        float b = 0;
        int b_SubdivisionStart = m_samplesAccumulationPerSectionArray.Length / 2; //4;
        int b_SubdivisionEnd = m_samplesAccumulationPerSectionArray.Length -1 ;
        float b_weight = 1.0f / ((b_SubdivisionEnd - b_SubdivisionStart + 1) * (float)subdivisionInterval);

        float progressOnCurve = 0;
        for (int i = 0; i < m_processedFFTDataArray.Length; i++)
        {
            if (i % subdivisionInterval == 0 && i != 0)
                subdivisionIndex += 1;

            if (subdivisionIndex <= r_SubdivisionEnd)
            {
                progressOnCurve = Mathf.InverseLerp(subdivisionInterval * r_SubdivisionStart, subdivisionInterval * r_SubdivisionEnd, i);
                r += m_rValueCurve.Evaluate(progressOnCurve) * m_processedFFTDataArray[i] * r_weight;
            }

            if (subdivisionIndex >= g_SubdivisionStart && subdivisionIndex <= g_SubdivisionEnd)
            {
                progressOnCurve = Mathf.InverseLerp(subdivisionInterval * g_SubdivisionStart, subdivisionInterval * g_SubdivisionEnd, i);
                g += m_gValueCurve.Evaluate(progressOnCurve) * m_processedFFTDataArray[i] * g_weight;
            }

            if (subdivisionIndex >= b_SubdivisionStart)
            {
                progressOnCurve = Mathf.InverseLerp(subdivisionInterval * b_SubdivisionStart, subdivisionInterval * b_SubdivisionEnd, i);
                b += m_bValueCurve.Evaluate(progressOnCurve) * m_processedFFTDataArray[i] * b_weight;
            }
        }

        Color calculatedColor = m_colorScaler * new Color(r * m_rScaler, g * m_gScaler, b * m_bScaler);
        m_currentFFTColor = calculatedColor;
        

        HSLColor hslCol = new HSLColor(m_currentFFTColor);
        hslCol.h += m_colorHueOffset;
        hslCol.s += m_colorSaturationBoost;
        m_currentFFTColor = hslCol.ToRGBA();
        
        // do clamping of color
        m_currentFFTColAsVec3 = new Vector3(m_currentFFTColor.r, m_currentFFTColor.g, m_currentFFTColor.b);
        //m_currentFFTColAsVec3 = Vector3.ClampMagnitude(m_currentFFTColAsVec3, k_maxColorMagnitude);
        m_currentFFTColAsVec3.x = Mathf.Clamp(m_currentFFTColAsVec3.x, 0, k_maxColorMagnitude);
        m_currentFFTColAsVec3.y = Mathf.Clamp(m_currentFFTColAsVec3.y, 0, k_maxColorMagnitude);
        m_currentFFTColAsVec3.z = Mathf.Clamp(m_currentFFTColAsVec3.z, 0, k_maxColorMagnitude);
        m_currentFFTColor = new Color(m_currentFFTColAsVec3.x, m_currentFFTColAsVec3.y, m_currentFFTColAsVec3.z);

        m_smoothDampedFFTColAsVec3 = Vector3.SmoothDamp(m_smoothDampedFFTColAsVec3, m_currentFFTColAsVec3, ref m_sdFFTColVec3Vel, k_volSmoothDampTime);

        d_r = m_currentFFTColor.r;
        d_g = m_currentFFTColor.g;
        d_b = m_currentFFTColor.b;

        return m_currentFFTColor;
    }

    float[] GetFreshFFTData()
    {
        if (m_audioDataSource == AudioDataSources.AudioListener)
        {
            AudioListener.GetSpectrumData(m_currentRawFFTDataArray, 0, FFTWindow.BlackmanHarris);
            
        }
        else if(m_audioDataSource == AudioDataSources.AudioSource)
        {
            m_targetAudioSource.GetSpectrumData(m_currentRawFFTDataArray, 0, FFTWindow.BlackmanHarris);
        }
        else if(m_audioDataSource == AudioDataSources.LASP)
        {            
            AudioInput.RetrieveWaveform(FilterType.Bypass, m_laspWaveformArray);         
            m_manualFFTHandler.ExecuteManualFFT(m_laspWaveformArray);         
            m_currentRawFFTDataArray = m_manualFFTHandler.Get_ManualFFTDataReal();
        }
        else
        {
            m_liveAudioDataManager.m_liveAudioSource.GetSpectrumData(m_currentRawFFTDataArray, 0, FFTWindow.BlackmanHarris);
        }

        ProcessRawFFTData();
        return m_processedFFTDataArray;
    }

    // http://answers.unity3d.com/questions/165729/editing-height-relative-to-audio-levels.html
    float GetFreshVolume()
    {
        float vol = 0;

        if (m_audioDataSource == AudioDataSources.AudioListener)
        {
            AudioListener.GetOutputData(m_outputSamplesArray, 0);

        }
        else if(m_audioDataSource == AudioDataSources.AudioSource)
        {
            m_targetAudioSource.GetOutputData(m_outputSamplesArray, 0);
        }
        else if(m_audioDataSource == AudioDataSources.LASP)
        {
            // DO LASP VOL CHECK
            
        }
        else
        {
            m_liveAudioDataManager.m_liveAudioSource.GetOutputData(m_outputSamplesArray, 0);
        }

        float sum = 0;
        for(int i = 0; i < m_outputSamplesArray.Length; i++)
        {
            sum += m_outputSamplesArray[i] * m_outputSamplesArray[i];
        }

        // vol in RMS
        vol = Mathf.Sqrt(sum / (float)m_outputSamplesArray.Length);
        //vol =  (sum / (float)m_outputSamplesArray.Length);
        vol = Mathf.Sqrt(vol);

        //hack to bypass outsputsamples stuff
        if (m_audioDataSource == AudioDataSources.LASP)
        {
            // DO LASP VOL CHECK
            vol = AudioInput.CalculateRMS(FilterType.Bypass);            
        }

        m_currentVolume = m_globalVolumeDataScaler * vol;

        m_vollumeRollingAverageIndex = (m_vollumeRollingAverageIndex + 1) % k_vollumeRollingAverageCount;
        m_volumeRollingAverageArray[m_vollumeRollingAverageIndex] = m_currentVolume;


        sum = 0;
        for (int i = 0; i < k_vollumeRollingAverageCount; i++)
        {
            sum += m_volumeRollingAverageArray[i];
        }
        m_vollumeRollingAverage = sum / (float)k_vollumeRollingAverageCount;


        return vol;
    }

    int CalculateMaxExtraSamplesCountForSection(int targetSectionIndex)
    {
        int totalSamplesSum = 0 + m_samplesAccumulationStartIndexOffset;
        int sectionIndexCounter = 0;
        int sectionInterval = m_processedFFTDataArray.Length / m_samplesAccumulationPerSectionArray.Length;
        for (int i = 0; i < m_processedFFTDataArray.Length; i++)
        {
            if (i % sectionInterval == 0 && i != 0)
                sectionIndexCounter += 1;

            totalSamplesSum += m_samplesAccumulationPerSectionArray[sectionIndexCounter];
        }

        int samplesRemaining = m_currentRawFFTDataArray.Length - totalSamplesSum;
        int maxSamplesCountForIndex = samplesRemaining / m_samplesAccumulationPerSectionArray[targetSectionIndex];
        return maxSamplesCountForIndex;
    }

    public void IncrementFrequencyRangeSamplesCount(int sectionIndex, int samplesChangeCount)
    {
        int maxSamplesCount = m_samplesAccumulationPerSectionArray[sectionIndex] + CalculateMaxExtraSamplesCountForSection(sectionIndex);
        int changedSamplesCountRaw = m_samplesAccumulationPerSectionArray[sectionIndex] + samplesChangeCount;
        m_samplesAccumulationPerSectionArray[sectionIndex] = (int)Mathf.Clamp(changedSamplesCountRaw, 1, maxSamplesCount);
    }


}
