﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control_AudioTextureHeightByVol : MonoBehaviour
{
    AudioReactiveTextureManager m_artm;
    FrequencyDataManager m_fdm;

    public float m_minScale = 0.1f;
    public float m_maxScale = 2.0f;

    void Start()
    {
        m_artm = FindObjectOfType<AudioReactiveTextureManager>();
        m_fdm = FindObjectOfType<FrequencyDataManager>();
    }

    void Update()
    {
        float step = Mathf.SmoothStep(0, 1.0f, m_fdm.m_currentVolume);
        m_artm.m_heightScale = Mathf.Lerp(m_minScale, m_maxScale, step);
    }

}
