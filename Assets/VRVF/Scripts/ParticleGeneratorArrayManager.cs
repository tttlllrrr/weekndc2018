﻿using UnityEngine;
using System.Collections;

public class ParticleGeneratorArrayManager : MonoBehaviour
{
    public GameObject m_particleGeneratorPrefab;
    public int m_generatorsCount = 256;

    public float m_generatorsRadius = 1.0f;

    public Vector3 m_generatorsAnchorPositionOffset;

    ComputeParticlesGenerator[] m_generatorsArray;

    public enum GeneratorsLookAtTypes
    {
        Center,
        UpAligned,
        TangentAligned,
    };
    public GeneratorsLookAtTypes m_generatorLookAtType;

    public enum GeneratorsLayoutTypes
    {
        Circle,
        Spiral,
        Flower,
        Flat
    }
    public GeneratorsLayoutTypes m_generatorsLayoutType;
    public float m_flowerPetalsFrequency = 3.0f;

    public bool m_isScaleSpreadFFT = false;

    public int m_fftIndexFrequency = 1;
    

    FrequencyDataManager m_frequencyDataManager;

    void Start()
    {
        m_frequencyDataManager = FindObjectOfType<FrequencyDataManager>();
        m_generatorsArray = new ComputeParticlesGenerator[m_generatorsCount];
        GameObject temp;
        for (int i = 0; i < m_generatorsArray.Length; i++)
        {
            temp = (GameObject)Instantiate(m_particleGeneratorPrefab);
            temp.name = "ParticleGenerator_" + i.ToString();
            temp.transform.parent = transform;
            m_generatorsArray[i] = temp.GetComponent<ComputeParticlesGenerator>();
        }

        RefreshGeneratorsTransforms();        
    }

    void Update()
    {
        // should only do this on param cahnge, but in a rush
        RefreshGeneratorsTransforms();

        int fftDataArrayLength = m_frequencyDataManager.m_processedFFTDataArray.Length;
        float fftDataToGeneratorsRatio = (float)fftDataArrayLength / (float)m_generatorsArray.Length;
        
        for (int i = 0; i < m_generatorsArray.Length; i++)
        {
            // particle generators cound needs to be equal or higher than FFT data array size
            int fftIndex = (Mathf.FloorToInt((float)i * fftDataToGeneratorsRatio ) * m_fftIndexFrequency) % fftDataArrayLength;
            m_generatorsArray[i].m_fftIndex = fftIndex;
            m_generatorsArray[i].m_isScaleSpreadFFT = m_isScaleSpreadFFT;
        }

    }

    void RefreshGeneratorsTransforms()
    {
        for (int i = 0; i < m_generatorsArray.Length; i++)
        {
            float indexR = (float)i / (float)m_generatorsArray.Length;
            float indexR_Next = ((float)i + 0.001f) / (float)m_generatorsArray.Length;

            GameObject temp = m_generatorsArray[i].gameObject;
            temp.transform.localScale = Vector3.one;
            temp.transform.localRotation = Quaternion.identity;
            Vector3 towardNextPointVec = Vector3.zero;
            float spawnRadius = m_generatorsRadius;
            float spawnRadius_N = m_generatorsRadius;
            Vector3 forwardVec = transform.forward;

            if (m_generatorsLayoutType == GeneratorsLayoutTypes.Circle)
            {
                spawnRadius = m_generatorsRadius;
                temp.transform.localPosition = GeneratorLocalPosition_Circular(indexR, spawnRadius);
                towardNextPointVec = GeneratorLocalPosition_Circular(indexR_Next, spawnRadius) - temp.transform.localPosition;
            }
            else if (m_generatorsLayoutType == GeneratorsLayoutTypes.Spiral)
            {
                spawnRadius = indexR * m_generatorsRadius;
                temp.transform.localPosition = GeneratorLocalPosition_Circular(indexR, spawnRadius);
                spawnRadius_N = indexR_Next * m_generatorsRadius;
                towardNextPointVec = GeneratorLocalPosition_Circular(indexR_Next, spawnRadius_N) - temp.transform.localPosition;
            }
            else if (m_generatorsLayoutType == GeneratorsLayoutTypes.Flower)
            {
                spawnRadius = m_generatorsRadius * Mathf.Sin(m_flowerPetalsFrequency * indexR * 2.0f * Mathf.PI);
                temp.transform.localPosition = GeneratorLocalPosition_Circular(indexR, spawnRadius);
                spawnRadius_N = m_generatorsRadius * Mathf.Sin(m_flowerPetalsFrequency * indexR_Next * 2.0f * Mathf.PI);
                towardNextPointVec = GeneratorLocalPosition_Circular(indexR_Next, spawnRadius_N) - temp.transform.localPosition;
            }
            else if(m_generatorsLayoutType == GeneratorsLayoutTypes.Flat)
            {
                spawnRadius = m_generatorsRadius;
                temp.transform.localPosition = GeneratorLocalPosition_Flat(indexR, spawnRadius);
                towardNextPointVec = GeneratorLocalPosition_Flat(indexR_Next, spawnRadius) - temp.transform.localPosition;
            }
            // normalize direction
            towardNextPointVec = towardNextPointVec.normalized;


            if (m_generatorLookAtType == GeneratorsLookAtTypes.Center)
                temp.transform.LookAt(transform.position);
            else if (m_generatorLookAtType == GeneratorsLookAtTypes.UpAligned)
            {
                Vector3 upVec = Vector3.Cross(towardNextPointVec, Vector3.forward);
                //if (upVec == Vector3.forward)
                    temp.transform.localRotation = Quaternion.LookRotation(Vector3.forward, upVec);

            }
            else if (m_generatorLookAtType == GeneratorsLookAtTypes.TangentAligned)
            {
                Vector3 upVec = Vector3.Cross(towardNextPointVec, Vector3.forward);
                if (towardNextPointVec.magnitude != 0 )//if(towardNextPointVec.magnitude != 0 && upVec.magnitude!= 0)
                    temp.transform.localRotation = Quaternion.LookRotation(towardNextPointVec, upVec);
            }
        }


    }

    Vector3 GeneratorLocalPosition_Circular(float indexRatio, float spawnRadius)
    {
        Vector3 pos = Vector3.zero;

        pos.x = spawnRadius * Mathf.Cos(2.0f * Mathf.PI * indexRatio);
        pos.y = spawnRadius * Mathf.Sin(2.0f * Mathf.PI * indexRatio);
        pos.z = 0; 

        return pos + m_generatorsAnchorPositionOffset;
    }

    Vector3 GeneratorLocalPosition_Flat(float indexRatio, float spawnRadius)
    {
        Vector3 pos = Vector3.zero;
        float centered_x = Mathf.Lerp(-spawnRadius, spawnRadius, indexRatio);
        pos.x = centered_x;
        return pos + m_generatorsAnchorPositionOffset;
    }


}
