﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MeshesToParticlesConverter : MonoBehaviour
{
    public GameObject m_meshesHolder;
    MeshFilter[] m_meshFiltersArray;

    List<Vector3> m_meshesVerticesList; // world position of vertices
    List<Vector3> m_meshesNormalsList; // world space normals
    List<Color> m_meshVertexColorsList;

    ComputeParticlesGenerationManager m_computeParticlesGenerationManager;

    public bool m_isMeshParticlesSpawned = true;

    public float m_particlesScaler = 1.0f;

    bool m_isInit = false;

    void Start()
    {
        m_computeParticlesGenerationManager = FindObjectOfType<ComputeParticlesGenerationManager>();
        
    }


    void CollectMehsesData()
    {
        m_meshFiltersArray = m_meshesHolder.transform.GetComponentsInChildren<MeshFilter>();

        m_meshesVerticesList = new List<Vector3>();
        m_meshesNormalsList = new List<Vector3>();
        m_meshVertexColorsList = new List<Color>();
        for(int i = 0; i < m_meshFiltersArray.Length; i++)
        {
            Vector3[] meshFilterVertices = m_meshFiltersArray[i].mesh.vertices;
            Vector3[] meshFilterNormals = m_meshFiltersArray[i].mesh.normals;
            Color[] meshFilterVertexColors = m_meshFiltersArray[i].mesh.colors;

            Debug.Log("Mesh data: " + meshFilterVertices.Length + " | " + meshFilterNormals.Length + " | " + meshFilterVertexColors.Length);

            for(int j = 0; j < meshFilterVertices.Length; j++)
            {
                m_meshesVerticesList.Add(m_meshFiltersArray[i].transform.TransformPoint(meshFilterVertices[j]));
                m_meshesNormalsList.Add(m_meshFiltersArray[i].transform.TransformVector(meshFilterNormals[j]));
                //m_meshVertexColorsList.Add(meshFilterVertexColors[j]);
            }
        }   
    }
    

    void SpawnVerticesAsParticles()
    {
        Vector3 spawnPos = new Vector3();
        Vector3 spawnVel = new Vector3();
        Vector3 spawnForwardVec = Vector3.forward; // TODO: calculate surafece tangent for forward vec
        Vector3 spawnUpVec = Vector3.up;
        Color spawnColor = Color.white;

        for(int i = 0; i < m_meshesVerticesList.Count; i++)
        {
            spawnPos = m_meshesVerticesList[i];
            spawnUpVec = m_meshesNormalsList[i];
            //spawnColor = new Color(spawnUpVec.x, spawnUpVec.y, spawnUpVec.z, 1); //m_meshVertexColorsList[i];
            //spawnVel = spawnUpVec;
            m_computeParticlesGenerationManager.AddNewPartcileToSpawnList(spawnPos, spawnVel, spawnColor, spawnForwardVec, spawnUpVec, 1000.0f, m_particlesScaler);
        }

        m_computeParticlesGenerationManager.SpawnParticlesBasedFromListAndInjection();

        m_isMeshParticlesSpawned = true;
    }

    void Update()
    {
        if(m_isInit == false)
        {
            CollectMehsesData();
            m_isInit = true;
        }

        if(m_isMeshParticlesSpawned == false)
        {
            //CollectMehsesData();
            SpawnVerticesAsParticles();   
        }

    }

    public void SpawnParticles()
    {
        m_isMeshParticlesSpawned = false;
    }

    

}
