﻿using UnityEngine;
using System.Collections;

public class ForceSource : MonoBehaviour 
{
	public float m_forceMagnitude = 0.0f;
    public float m_forcePoloraity = 1.0f; // 1.0 for positive, -1.0 for negative/repulsion
    float m_awakeForceMagnitude = 0;

    RollingVelocityTracker m_rollingVelocityTracker;
    public bool m_isVelocitySourceActive = false;
    public Vector3 m_forceVelocity;
    public float m_forceVelocityScaler = 1.0f;
    public float m_forceDropoffExponent = 6.0f;
    
    public void Init()
    {
        m_awakeForceMagnitude = m_forceMagnitude;
    }

    public void TriggerHeld()
    {
        m_forceMagnitude = m_awakeForceMagnitude;
    }

    public void TriggerReleased()
    {
        m_forceMagnitude = 0.0f;
    }

    public float GetCurrentForceScale()
    {
        return m_forceMagnitude * m_forcePoloraity;
    }

    public Vector3 GetCurrentForceVelocity()
    {
        if (m_isVelocitySourceActive == true)
        {
            if(m_rollingVelocityTracker == null)
                m_rollingVelocityTracker = GetComponent<RollingVelocityTracker>();
            else
                m_forceVelocity = m_forceMagnitude * m_forceVelocityScaler * m_rollingVelocityTracker.GetCurrentRollingVelocity();
        }
        else
            m_forceVelocity = Vector3.zero;

        return m_forceVelocity;
    }

    public float GetCurrentForceDropoffExponent()
    {
        return m_forceDropoffExponent;
    }

}
