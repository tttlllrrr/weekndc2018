﻿using UnityEngine;
using System.Collections;

public class ComputeVectorFieldManager : MonoBehaviour
{
    public ComputeShader m_vectorFieldComputeShader;
    public ComputeShader m_copyTexutreComputeShader;
    public ComputeShader m_setVectorFieldUniformComputeShader;
    public ComputeShader m_flipVectorFieldCopmuteShader;
    public ComputeShader m_amplifyVectorFieldComputeShader;
    public ComputeShader m_setSinVectorFieldComputeShader;
    public ComputeShader m_setSphereVectorFieldComputeShader;

    RenderTexture m_vectorFieldTexture_Read;
    RenderTexture m_vectorFieldTexture_Write;

    public float m_decayFactor = 0; // where 0 is no decay, 1 is max

    public int k_fieldWidth = 16;
    public float k_fieldScale = 1.0f;
    public float k_forceClamp = 100.0f;
    
    struct VectorFieldSpecs
    {
        public int fieldWidth;
        public float fieldScale;
        public float forceClamp;
        public Vector3 fieldPos;        
    }
    VectorFieldSpecs[] m_vectorFieldSpecs;
    ComputeBuffer m_vectorFieldSpecsCB;


    ForceSource[] m_forceSourcesArray;
    struct ForceSourceSpecs
    {
        public Vector3 forcePos;
        public float forceScale;
        public Vector3 forceVel;
        public float forceDropoffExponent;
    }
    ForceSourceSpecs[] m_forcesSpecsArray;
    ComputeBuffer m_forcesSpecsCB;

    Vector3 m_uniformVectorSet;
    float m_amplifyScaler = 1.0f;

    bool m_isVectorFieldUpdating = true;
    bool m_isDispatch_UniformSet = false;
    bool m_isDispatch_Amplify = false;
    bool m_isDispatch_Flip = false;
    public bool m_isDispatch_SetSin = false;
    public float m_sinScaler = 1.0f;
    public bool m_isDispatch_SetSphere = false;
    public float m_sinFrequency = 0.5f;
    public float m_spherePowerScaler = 1.0f;


    void Start()
    {
        m_vectorFieldTexture_Read = new RenderTexture(k_fieldWidth, k_fieldWidth, 0);
        m_vectorFieldTexture_Read.format = RenderTextureFormat.ARGBFloat;
        m_vectorFieldTexture_Read.volumeDepth = k_fieldWidth;
        m_vectorFieldTexture_Read.dimension = UnityEngine.Rendering.TextureDimension.Tex3D;
        m_vectorFieldTexture_Read.enableRandomWrite = true; // should be false, but going off an example
        m_vectorFieldTexture_Read.Create();        

        m_vectorFieldTexture_Write = new RenderTexture(k_fieldWidth, k_fieldWidth, 0);
        m_vectorFieldTexture_Write.format = RenderTextureFormat.ARGBFloat;
        m_vectorFieldTexture_Write.volumeDepth = k_fieldWidth;
        m_vectorFieldTexture_Write.dimension = UnityEngine.Rendering.TextureDimension.Tex3D;
        m_vectorFieldTexture_Write.enableRandomWrite = true;
        m_vectorFieldTexture_Write.Create();        

        m_vectorFieldSpecs = new VectorFieldSpecs[1];
        m_vectorFieldSpecs[0].fieldPos = transform.position;
        m_vectorFieldSpecs[0].fieldScale = k_fieldScale;
        m_vectorFieldSpecs[0].fieldWidth = k_fieldWidth;
        m_vectorFieldSpecs[0].forceClamp = k_forceClamp;
        int sizeVFS = System.Runtime.InteropServices.Marshal.SizeOf(typeof(VectorFieldSpecs));        
        m_vectorFieldSpecsCB = new ComputeBuffer(m_vectorFieldSpecs.Length, sizeVFS);        
        m_vectorFieldSpecsCB.SetData(m_vectorFieldSpecs);

        m_forceSourcesArray = FindObjectsOfType<ForceSource>();
        m_forcesSpecsArray = new ForceSourceSpecs[m_forceSourcesArray.Length];
        for(int i = 0; i < m_forcesSpecsArray.Length; i++)
        {
            //m_forcesSpecsArray[i] = new ForceSourceSpecs();
            m_forcesSpecsArray[i].forcePos = m_forceSourcesArray[i].transform.position;
            m_forcesSpecsArray[i].forceScale = m_forceSourcesArray[i].GetCurrentForceScale();
            m_forcesSpecsArray[i].forceVel = m_forceSourcesArray[i].GetCurrentForceVelocity();
            m_forcesSpecsArray[i].forceDropoffExponent = m_forceSourcesArray[i].GetCurrentForceDropoffExponent();
        }
        int sizeFS = System.Runtime.InteropServices.Marshal.SizeOf(typeof(ForceSourceSpecs));
        m_forcesSpecsCB = new ComputeBuffer(m_forcesSpecsArray.Length, sizeFS);
        m_forcesSpecsCB.SetData(m_forcesSpecsArray);
             
        m_vectorFieldComputeShader.SetBuffer(0, "_vectorFieldSpecs", m_vectorFieldSpecsCB);
        m_vectorFieldComputeShader.SetBuffer(0, "_forcesSpecsArray", m_forcesSpecsCB);
        m_vectorFieldComputeShader.SetTexture(0, "_vectorFieldTex_Read", m_vectorFieldTexture_Read);
        m_vectorFieldComputeShader.SetTexture(0, "_vectorFieldTex_Write", m_vectorFieldTexture_Write);
        m_vectorFieldComputeShader.SetFloat("_vectorFieldScale", k_fieldScale);
        m_vectorFieldComputeShader.SetFloat("_delaTime", Time.deltaTime);
        m_vectorFieldComputeShader.SetFloat("_decayFactor", m_decayFactor);
        m_vectorFieldComputeShader.Dispatch(0, 8, 8, 8);

        m_copyTexutreComputeShader.SetTexture(0, "_vectorFieldTex_New", m_vectorFieldTexture_Write);
        m_copyTexutreComputeShader.SetTexture(0, "_vectorFieldTex_Old", m_vectorFieldTexture_Read);
        m_copyTexutreComputeShader.Dispatch(0, 8, 8, 8);

        
        m_setVectorFieldUniformComputeShader.SetTexture(0, "_vectorFieldTex_Write", m_vectorFieldTexture_Write);
        m_setVectorFieldUniformComputeShader.SetTexture(0, "_vectorFieldTex_Read", m_vectorFieldTexture_Read);
        m_setVectorFieldUniformComputeShader.SetFloats("_vectorToSet", ConvertVector3ToArray(Vector3.zero));
        m_setVectorFieldUniformComputeShader.Dispatch(0, 8, 8, 8);

        m_flipVectorFieldCopmuteShader.SetTexture(0, "_vectorFieldTex_Write", m_vectorFieldTexture_Write);
        m_flipVectorFieldCopmuteShader.SetTexture(0, "_vectorFieldTex_Read", m_vectorFieldTexture_Read);

        m_amplifyVectorFieldComputeShader.SetTexture(0, "_vectorFieldTex_Write", m_vectorFieldTexture_Write);
        m_amplifyVectorFieldComputeShader.SetTexture(0, "_vectorFieldTex_Read", m_vectorFieldTexture_Read);

        m_setSinVectorFieldComputeShader.SetTexture(0, "_vectorFieldTex_Write", m_vectorFieldTexture_Write);
        m_setSinVectorFieldComputeShader.SetTexture(0, "_vectorFieldTex_Read", m_vectorFieldTexture_Read);
        m_setSinVectorFieldComputeShader.SetBuffer(0, "_vectorFieldSpecs", m_vectorFieldSpecsCB);
        m_setSinVectorFieldComputeShader.SetFloat("_sinFrequency", m_sinFrequency);
        m_setSinVectorFieldComputeShader.SetFloat("_sinScaler", m_sinScaler);

        m_setSphereVectorFieldComputeShader.SetTexture(0, "_vectorFieldTex_Write", m_vectorFieldTexture_Write);
        m_setSphereVectorFieldComputeShader.SetTexture(0, "_vectorFieldTex_Read", m_vectorFieldTexture_Read);
        m_setSphereVectorFieldComputeShader.SetBuffer(0, "_vectorFieldSpecs", m_vectorFieldSpecsCB);
        m_setSphereVectorFieldComputeShader.SetFloat("_spherePowerScaler", m_spherePowerScaler);
        
    }

    float[] ConvertVector3ToArray(Vector3 vec)
    {
        float[] vArray = { vec.x, vec.y, vec.z };
        return vArray;
    }
    
    
    void Update()
    {
        // send fresh forces data
        // TODO:  oh gawsd fix this :/
        if (m_isVectorFieldUpdating == true)
        {
            m_forceSourcesArray = FindObjectsOfType<ForceSource>();
            for (int i = 0; i < m_forcesSpecsArray.Length; i++)
            {
                m_forcesSpecsArray[i].forcePos = m_forceSourcesArray[i].transform.position;
                m_forcesSpecsArray[i].forceScale = m_forceSourcesArray[i].GetCurrentForceScale();
                m_forcesSpecsArray[i].forceVel = m_forceSourcesArray[i].GetCurrentForceVelocity();
                m_forcesSpecsArray[i].forceDropoffExponent = m_forceSourcesArray[i].GetCurrentForceDropoffExponent();
                //Debug.Log(m_forcesSpecsArray[i].forceScale);
            }
            m_forcesSpecsCB.SetData(m_forcesSpecsArray);
            m_vectorFieldComputeShader.SetBuffer(0, "_forcesSpecsArray", m_forcesSpecsCB);
            m_vectorFieldComputeShader.SetFloat("_deltaTime", Time.deltaTime);
            m_vectorFieldComputeShader.SetFloat("_decayFactor", m_decayFactor);

            m_vectorFieldComputeShader.SetTexture(0, "_vectorFieldTex_Read", m_vectorFieldTexture_Read);
            m_vectorFieldComputeShader.SetTexture(0, "_vectorFieldTex_Write", m_vectorFieldTexture_Write);
            m_vectorFieldComputeShader.Dispatch(0, 8, 8, 8);

            // TODO: fix this read/write mess
            m_copyTexutreComputeShader.SetTexture(0, "_vectorFieldTex_New", m_vectorFieldTexture_Write);
            m_copyTexutreComputeShader.SetTexture(0, "_vectorFieldTex_Old", m_vectorFieldTexture_Read);
            m_copyTexutreComputeShader.Dispatch(0, 8, 8, 8);
        }

        if(m_isDispatch_Flip == true)
        {
            m_flipVectorFieldCopmuteShader.Dispatch(0, 8, 8, 8);
            m_isDispatch_Flip = false;
        }
        if(m_isDispatch_Amplify == true)
        {
            m_amplifyVectorFieldComputeShader.SetFloat("_amplifyScale", m_amplifyScaler);
            m_amplifyVectorFieldComputeShader.Dispatch(0, 8, 8, 8);
            m_isDispatch_Amplify = false;
        }
        if(m_isDispatch_UniformSet == true)
        {
            m_setVectorFieldUniformComputeShader.SetFloats("_vectorToSet", ConvertVector3ToArray(m_uniformVectorSet));
            m_setVectorFieldUniformComputeShader.Dispatch(0, 8, 8, 8);
            m_isDispatch_UniformSet = false;
        }
        if(m_isDispatch_SetSin == true)
        {
            m_setSinVectorFieldComputeShader.SetFloat("_sinFrequency", m_sinFrequency);
            m_setSinVectorFieldComputeShader.SetFloat("_sinScaler", m_sinScaler);
            m_setSinVectorFieldComputeShader.Dispatch(0, 8, 8, 8);
            m_isDispatch_SetSin = false;
        }
        if(m_isDispatch_SetSphere == true)
        {
            m_setSphereVectorFieldComputeShader.SetFloat("_spherePowerScaler", m_spherePowerScaler);            
            m_setSphereVectorFieldComputeShader.Dispatch(0, 8, 8, 8);
            m_isDispatch_SetSphere = false;
        }



        m_copyTexutreComputeShader.SetTexture(0, "_vectorFieldTex_New", m_vectorFieldTexture_Write);
        m_copyTexutreComputeShader.SetTexture(0, "_vectorFieldTex_Old", m_vectorFieldTexture_Read);
        m_copyTexutreComputeShader.Dispatch(0, 8, 8, 8);


    }

    public RenderTexture VectorFieldTexture_Read()
    {
        return m_vectorFieldTexture_Read;
    }
    public RenderTexture VectorFieldTexture_Write()
    {
        return m_vectorFieldTexture_Write;
    }

    void OnDestroy()
    {
        m_vectorFieldSpecsCB.Release();
        m_forcesSpecsCB.Release();
    }

    public float[] GetVectorFieldManagerPositionAsArray()
    {
        float[] posFloat = { transform.position.x, transform.position.y, transform.position.z };
        return posFloat;
    }

    public void SetVectorFieldVec_Uniform(Vector3 vec)
    {
        m_uniformVectorSet = vec;
        m_isDispatch_UniformSet = true;        
    }

    public void FlipVectorField()
    {
        m_isDispatch_Flip = true;
    }

    public void AmplifyVectorField(float scaler)
    {
        m_amplifyScaler = scaler;
        m_isDispatch_Amplify = true;
    }
    public void ToggleVectorFieldUpdate()
    {
        m_isVectorFieldUpdating = !m_isVectorFieldUpdating;
    }
}
