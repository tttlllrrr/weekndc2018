﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class ParticlesTest : MonoBehaviour
{
    ParticleSystem m_particleSystem;

    const int c_partcilesCount = 8000;
    ParticleSystem.Particle[] m_particlesArray;

    Vector3[] m_particlePositionsArray;
    Vector3[] m_particleVelocitiesArray;
    float[] m_particlesLifetimeArray;
    float m_particlesMaxLifetime = 5.0f;
    float m_particlesMaxVelocityMagnitude = 10.0f;

    [Range(0.01f, 1.0f)]
    public float m_spawnRadius = 1.0f;

    VectorFieldManager m_vectorFieldManager;

    void Start()
    {
        m_particleSystem = GetComponent<ParticleSystem>();
        m_particlesArray = new ParticleSystem.Particle[c_partcilesCount];
        m_particlePositionsArray = new Vector3[m_particlesArray.Length];
        m_particleVelocitiesArray = new Vector3[m_particlesArray.Length];
        m_particlesLifetimeArray = new float[m_particlesArray.Length];

        for (int i = 0; i < m_particlesArray.Length; i++)
        {
            m_particlePositionsArray[i] = GenerateParticleSpawnPosition() ;
            m_particleVelocitiesArray[i] = -0.0f * Random.Range(0.1f, 1.0f) * transform.up;

            m_particlesArray[i].position = m_particlePositionsArray[i];
            m_particlesArray[i].startColor = Color.white;
            m_particlesArray[i].startSize = 0.0125f;
            m_particlesLifetimeArray[i] = Random.Range(0.0f, 0.5f * m_particlesMaxLifetime);                       
        }
                     
        m_particleSystem.SetParticles(m_particlesArray, c_partcilesCount);

        m_vectorFieldManager = FindObjectOfType<VectorFieldManager>();      
    }

    Vector3 GenerateParticleSpawnPosition()
    {
        return transform.position + m_spawnRadius * Random.onUnitSphere;
    }

    void Update()
    {
        // put here as temp variable
        Vector4 closestVectorAndDistance = Vector4.zero;        
        Vector3 particlePos = Vector3.zero;
        float forceFalloff = 1.0f;

        for(int i = 0; i < m_particlesArray.Length; i++)
        {
            //Profiler.BeginSample("Particle calc setup");
            particlePos = m_particlePositionsArray[i];
            closestVectorAndDistance = m_vectorFieldManager.FindClosestForceVectorAndDist(particlePos);
            Vector3 closestVector = new Vector3(closestVectorAndDistance.x, closestVectorAndDistance.y, closestVectorAndDistance.z);
            float particleToVectorDist = closestVectorAndDistance.w;

            // hard limit falloff
            forceFalloff = 1.0f - Mathf.InverseLerp(0.0f, 1.0f, particleToVectorDist);     // TODO: do this right/nice instead of currently hacked linear falloff

            // clamped at a max of 1, linear
            //forceFalloff = 1.0f / (1.0f + particleToVectorDist);

            // clamped at a max of 1, quadratic
            //forceFalloff = 1.0f / (1.0f + Mathf.Pow(particleToVectorDist, 2.0f) );

            // clamped at a max of 1, high exp
            //forceFalloff = 1.0f / (1.0f + Mathf.Pow(particleToVectorDist, 16.0f));

            //Profiler.EndSample();


            m_particleVelocitiesArray[i] += closestVector * forceFalloff * Time.deltaTime;
            m_particleVelocitiesArray[i] = Vector3.ClampMagnitude(m_particleVelocitiesArray[i], m_particlesMaxVelocityMagnitude);
            m_particlePositionsArray[i] += m_particleVelocitiesArray[i] * Time.deltaTime;
            
            m_particlesArray[i].position = m_particlePositionsArray[i];
        
            // do particle lifetime
            m_particlesLifetimeArray[i] += Time.deltaTime;
            if (m_particlesLifetimeArray[i] > m_particlesMaxLifetime)
            {
                m_particlePositionsArray[i] = GenerateParticleSpawnPosition();
                m_particlesLifetimeArray[i] = 0;
                m_particleVelocitiesArray[i] = -0.250f * Random.Range(0.1f, 1.0f) * transform.up;
            }            
        }

        m_particleSystem.SetParticles(m_particlesArray, c_partcilesCount);        
    }

}
