﻿using UnityEngine;
using System.Collections;

public class FisheyeVideoPlayback : MonoBehaviour
{
    public MovieTexture m_fisheyeMovieTexture;
    public Material m_material;

    void Start()
    {
        m_material.mainTexture = m_fisheyeMovieTexture;
        m_fisheyeMovieTexture.Play();
    }

}
