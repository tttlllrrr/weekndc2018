﻿using UnityEngine;
using System.Collections;

public class ComputeParticlesLerpToTargetManager : MonoBehaviour
{
    public ComputeShader m_particlesSetTargetBuffersCS;
    public ComputeShader m_particlesLerpToTargetCS;
    
    // full data set
    ComputeBuffer m_particlesTarget_PositionCB;
    ComputeBuffer m_particlesTarget_VelocitiesCB;
    ComputeBuffer m_particlesTarget_ColorsCB;
    ComputeBuffer m_particlesTarget_UpVectorsCB;

    // partial data set, just for setting CBs
    ComputeBuffer m_setTargetPositionsCB;
    ComputeBuffer m_setTargetVelocitiesCB;
    ComputeBuffer m_setTargetColorsCB;
    ComputeBuffer m_setTargetUpVectorsCB;

    ComputeParticleSimulationManager m_computeParticlesSimulationManager;

    public float m_lerpScaler = 1.0f;
    public float m_lerpFactor = 0.0f; // only public for debug in inspector
    int m_previousBatchCount = 0;

    // assuming this runs after the sim script
    void Start()
    {
        m_computeParticlesSimulationManager = FindObjectOfType<ComputeParticleSimulationManager>();

        m_particlesTarget_PositionCB = new ComputeBuffer(m_computeParticlesSimulationManager.GetParticleCount(), sizeof(float) * 4);
        m_particlesTarget_VelocitiesCB = new ComputeBuffer(m_computeParticlesSimulationManager.GetParticleCount(), sizeof(float) * 4);
        m_particlesTarget_ColorsCB = new ComputeBuffer(m_computeParticlesSimulationManager.GetParticleCount(), sizeof(float) * 4);
        m_particlesTarget_UpVectorsCB = new ComputeBuffer(m_computeParticlesSimulationManager.GetParticleCount(), sizeof(float) * 4);


        // setting up buffers for the CS that does the target data dump
        m_particlesSetTargetBuffersCS.SetBuffer(0, "_targetPositions", m_particlesTarget_PositionCB);
        m_particlesSetTargetBuffersCS.SetBuffer(0, "_targetVelocities", m_particlesTarget_VelocitiesCB);
        m_particlesSetTargetBuffersCS.SetBuffer(0, "_targetColors", m_particlesTarget_ColorsCB);
        m_particlesSetTargetBuffersCS.SetBuffer(0, "_targetUpVectors", m_particlesTarget_UpVectorsCB);
        m_particlesSetTargetBuffersCS.SetInt("_particlesTotalCount", m_computeParticlesSimulationManager.GetParticleCount());
        m_particlesSetTargetBuffersCS.SetInt("k_threadGroupCount_x", m_computeParticlesSimulationManager.GetThreadGroupCount_X());
        m_particlesSetTargetBuffersCS.SetInt("k_threadGroupCount_y", m_computeParticlesSimulationManager.GetThreadGroupCount_Y());


        // set simualation data buffers, only needs to get set once, its the same buffer
        m_particlesLerpToTargetCS.SetBuffer(0, "_positions", m_computeParticlesSimulationManager.GetPositionsCB());
        m_particlesLerpToTargetCS.SetBuffer(0, "_velocities", m_computeParticlesSimulationManager.GetVelocitiesCB());
        m_particlesLerpToTargetCS.SetBuffer(0, "_colors", m_computeParticlesSimulationManager.GetColorsCB());
        m_particlesLerpToTargetCS.SetBuffer(0, "_upVectors", m_computeParticlesSimulationManager.GetUpVectorsCB());        

        m_particlesLerpToTargetCS.SetBuffer(0, "_targetPositions", m_particlesTarget_PositionCB);
        m_particlesLerpToTargetCS.SetBuffer(0, "_targetVelocities", m_particlesTarget_VelocitiesCB);
        m_particlesLerpToTargetCS.SetBuffer(0, "_targetColors", m_particlesTarget_ColorsCB);
        m_particlesLerpToTargetCS.SetBuffer(0, "_targetUpVectors", m_particlesTarget_ColorsCB);

        m_particlesLerpToTargetCS.SetInt("_partcilesPerThread", m_computeParticlesSimulationManager.GetParticlesPerThread()); 
        m_particlesLerpToTargetCS.SetInt("k_threadGroupCount_x", m_computeParticlesSimulationManager.GetThreadGroupCount_X());
        m_particlesLerpToTargetCS.SetInt("k_threadGroupCount_y", m_computeParticlesSimulationManager.GetThreadGroupCount_Y());
     

    }

    public void SetLerpTargetBuffers(Vector4[] positionsArray, Vector4[] velocitiesArray, Vector4[] colorsArray, Vector4[] upVectorsArray, int startIndex, int spawnCount)
    {
        if (m_setTargetPositionsCB != null)
        {
            m_setTargetPositionsCB.Release();
            m_setTargetVelocitiesCB.Release();
            m_setTargetColorsCB.Release();
            m_setTargetUpVectorsCB.Release();

            m_setTargetPositionsCB.Dispose();
            m_setTargetVelocitiesCB.Dispose();
            m_setTargetColorsCB.Dispose();
            m_setTargetUpVectorsCB.Dispose();
        }

        m_setTargetPositionsCB = new ComputeBuffer(spawnCount, sizeof(float) * 4);
        m_setTargetVelocitiesCB = new ComputeBuffer(spawnCount, sizeof(float) * 4);
        m_setTargetColorsCB = new ComputeBuffer(spawnCount, sizeof(float) * 4);
        m_setTargetUpVectorsCB = new ComputeBuffer(spawnCount, sizeof(float) * 4);

        m_setTargetPositionsCB.SetData(positionsArray);
        m_setTargetVelocitiesCB.SetData(velocitiesArray);
        m_setTargetColorsCB.SetData(colorsArray);
        m_setTargetUpVectorsCB.SetData(upVectorsArray);

        m_particlesSetTargetBuffersCS.SetBuffer(0, "_targetPositionsSource", m_setTargetPositionsCB);
        m_particlesSetTargetBuffersCS.SetBuffer(0, "_targetVelocitiesSource", m_setTargetVelocitiesCB);
        m_particlesSetTargetBuffersCS.SetBuffer(0, "_targetColorsSource", m_setTargetColorsCB);
        m_particlesSetTargetBuffersCS.SetBuffer(0, "_targetUpVectorsSource", m_setTargetUpVectorsCB);
        
        m_particlesSetTargetBuffersCS.SetInt("_spawnStartIndex", startIndex);
        m_particlesSetTargetBuffersCS.SetInt("_spawnCount", positionsArray.Length);
        m_particlesSetTargetBuffersCS.SetInt("_particlesTotalCount", m_computeParticlesSimulationManager.GetParticleCount());
        m_particlesSetTargetBuffersCS.SetInt("_partcilesPerThread", m_computeParticlesSimulationManager.GetParticlesPerThread());

        m_particlesSetTargetBuffersCS.Dispatch(0, m_computeParticlesSimulationManager.GetThreadGroupCount_X(), m_computeParticlesSimulationManager.GetThreadGroupCount_Y(), 1);

        //Debug.Log("Lerp set dispatched!");
    }

    public void DispatchParticlesLerpToTarget()
    {                
        m_particlesLerpToTargetCS.SetFloat("_lerpFactor", m_lerpFactor);
        m_particlesLerpToTargetCS.SetFloat("_deltaTime", Time.deltaTime);

        m_particlesLerpToTargetCS.Dispatch(0, m_computeParticlesSimulationManager.GetThreadGroupCount_X(), m_computeParticlesSimulationManager.GetThreadGroupCount_Y(), 1);     
    }

    public void LerpInputHeld(float value)
    {
        m_lerpFactor += m_lerpScaler * value * Time.deltaTime;
        m_lerpFactor = Mathf.Clamp(m_lerpFactor, 0, 1.1f);
    }
    public void LerpInputReleased()
    {
        m_lerpFactor = 0;
    }

    void OnDestroy()
    {
        if (m_particlesTarget_PositionCB != null)
        {
            m_particlesTarget_PositionCB.Release();
            m_particlesTarget_VelocitiesCB.Release();
            m_particlesTarget_ColorsCB.Release();
            m_particlesTarget_UpVectorsCB.Release();

            m_particlesTarget_PositionCB.Dispose();
            m_particlesTarget_VelocitiesCB.Dispose();
            m_particlesTarget_ColorsCB.Dispose();
            m_particlesTarget_UpVectorsCB.Dispose();
        }

        if(m_setTargetPositionsCB != null)
        {
            m_setTargetPositionsCB.Release();
            m_setTargetVelocitiesCB.Release();
            m_setTargetColorsCB.Release();
            m_setTargetUpVectorsCB.Release();

            m_setTargetPositionsCB.Dispose();
            m_setTargetVelocitiesCB.Dispose();
            m_setTargetColorsCB.Dispose();
            m_setTargetUpVectorsCB.Dispose();
        }
    }

}
