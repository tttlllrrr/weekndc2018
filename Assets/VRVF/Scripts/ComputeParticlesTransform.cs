﻿using UnityEngine;
using System.Collections;

public class ComputeParticlesTransform : MonoBehaviour
{
    public ComputeShader m_computeParticlesTransformCS;
    public Transform m_targetMatrixTransform;
    Matrix4x4 m_targetTransformMatrix;
    Vector3 m_targetTransformPosition;

    public struct TransformData
    {
        public Matrix4x4 transformMatrix;
        public Vector3 transformPosition;
    }
    TransformData[] m_transformDataStructArray;
    ComputeBuffer m_transformDataCB;

    ComputeParticleSimulationManager m_computeParticlesSimulationManager;

    int m_sizeTT;

    void Start()
    {
        m_computeParticlesSimulationManager = FindObjectOfType<ComputeParticleSimulationManager>();
        
        m_targetTransformMatrix = GetTransform().localToWorldMatrix;
        m_targetTransformPosition = GetTransform().position;
        
        
        m_transformDataStructArray = new TransformData[1];
        m_transformDataStructArray[0] = new TransformData();
        m_transformDataStructArray[0].transformMatrix = m_targetTransformMatrix;
        m_transformDataStructArray[0].transformPosition = m_targetTransformPosition;
        m_sizeTT = System.Runtime.InteropServices.Marshal.SizeOf(typeof(TransformData));
        m_transformDataCB = new ComputeBuffer(1, m_sizeTT);
        m_transformDataCB.SetData(m_transformDataStructArray);

        m_computeParticlesTransformCS.SetInt("k_threadGroupCount_x", m_computeParticlesSimulationManager.GetThreadGroupCount_X());
        m_computeParticlesTransformCS.SetInt("k_threadGroupCount_y", m_computeParticlesSimulationManager.GetThreadGroupCount_Y());
        m_computeParticlesTransformCS.SetInt("_partcilesPerThread", m_computeParticlesSimulationManager.GetParticlesPerThread());

        m_computeParticlesTransformCS.SetBuffer(0, "_simPositions", m_computeParticlesSimulationManager.GetPositionsCB());
        m_computeParticlesTransformCS.SetBuffer(0, "_simVelocities", m_computeParticlesSimulationManager.GetVelocitiesCB());
        m_computeParticlesTransformCS.SetBuffer(0, "_simForwardVectors", m_computeParticlesSimulationManager.GetForwardVectorsCB());
        m_computeParticlesTransformCS.SetBuffer(0, "_simUpVectors", m_computeParticlesSimulationManager.GetUpVectorsCB());        
    }

    public Transform GetTransform()
    {
        if (m_targetMatrixTransform == null)
        {
            return transform;
        }
        else
        {
            return m_targetMatrixTransform;
        }
    }

    public void ApplyTransformsToParticles()
    {
        
        m_targetTransformMatrix = GetTransform().localToWorldMatrix;
        m_targetTransformPosition = GetTransform().position;
        
        m_transformDataStructArray[0].transformMatrix = m_targetTransformMatrix;
        m_transformDataStructArray[0].transformPosition = m_targetTransformPosition;
        m_transformDataCB.SetData(m_transformDataStructArray);
        m_computeParticlesTransformCS.SetBuffer(0, "_transformDataArray", m_transformDataCB);

        /*
        ComputeBuffer debugBuff = new ComputeBuffer(1, sizeof(float));
        float[] debugFloatArray = new float[1];
        debugFloatArray[0] = 0;
        debugBuff.SetData(debugFloatArray);
        m_computeParticlesTransformCS.SetBuffer(0, "_debugFloat", debugBuff);
        */

        m_computeParticlesTransformCS.Dispatch(0, m_computeParticlesSimulationManager.GetThreadGroupCount_X(), m_computeParticlesSimulationManager.GetThreadGroupCount_Y(), 1);
        
        /*
        debugBuff.GetData(debugFloatArray);
        Debug.Log(debugFloatArray[0]);
        debugBuff.Release();
        debugBuff.Dispose();
        */

    }

    void OnDestroy()
    {
        if (m_transformDataCB != null)
        {
            m_transformDataCB.Release();
            
            m_transformDataCB.Dispose();
            
        }

    }


}
