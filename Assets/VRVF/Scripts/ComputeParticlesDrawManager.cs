﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputeParticlesDrawManager : MonoBehaviour
{
    public ComputeShader m_appendParticlesToDrawCS;
    
    // data for DrawProceduralIndirect stuff
    public Material[] m_proceduralParticlesMaterialArray;
    public int m_currentProceduralParticlesMaterialIndex = 0;
    public float m_proceduralMaterialSizeScaler = 1.0f;

    // data for DrawMeshInstancedIndirect stuff
    public Material[] m_meshInstancedParticlesMaterialArray;
    public int m_currentMeshInstancedParticlesMaterialIndex = 0;
    public float m_meshInstancedMaterialSizeScaler = 1.0f;
    public Mesh[] m_meshIndirectMeshesArray;
    public int m_currentMeshInstancedMeshIndex = 0;
    Bounds m_meshInstancedBounds;
    public float m_meshInstancedBoundsSizeScaler = 1000.0f;

    ComputeParticleSimulationManager m_computeParticlesSimulationManager;
    ComputeBuffer m_particlesToDrawIndicesAB;
    uint[] m_drawProceduralArgs = { 0, 1, 0, 0 }; // {vertex count per instance, instance count, start vertex location, start instance location}
    ComputeBuffer m_particlesToDrawIndirectArgsBuffer; // not sure why, but x instances  @ 1 vert is 5-6x slower than 1 vert at x instances
    // used to debug 
    ComputeBuffer m_particlesToDrawIndicesCountBuffer;    
    uint[] m_particlesToDrawIndicesCountBufferDump;
    public uint m_particlesToDrawCount = 0;

    uint[] m_drawMeshIndirectArgs = { 0, 0, 0, 0, 0 }; // {index count per instance, instance count, start index location, base vertex location, start instance location}
    ComputeBuffer m_meshInstancedDrawIndirectArgsBuffer;


    
    public enum DrawType
    {
        DrawProceduralIndirect,
        DrawMeshIntancedIndirect
    }
    public DrawType m_drawType = DrawType.DrawProceduralIndirect;
    public bool m_isRendering = true;

    void Start()
    {
        m_computeParticlesSimulationManager = FindObjectOfType<ComputeParticleSimulationManager>();

        m_particlesToDrawIndicesAB = new ComputeBuffer(m_computeParticlesSimulationManager.GetParticleCount(), sizeof(uint), ComputeBufferType.Append);
        m_particlesToDrawIndicesAB.SetCounterValue(0);

        m_particlesToDrawIndirectArgsBuffer = new ComputeBuffer(1, m_drawProceduralArgs.Length*sizeof(uint), ComputeBufferType.IndirectArguments);        
        m_particlesToDrawIndirectArgsBuffer.SetData(m_drawProceduralArgs);
        
        // just allocating here, will set all relevant data in Update
        m_meshInstancedDrawIndirectArgsBuffer = new ComputeBuffer(1, m_drawMeshIndirectArgs.Length*sizeof(uint), ComputeBufferType.IndirectArguments);

        //used to debug append compute shader particle count
        m_particlesToDrawIndicesCountBuffer = new ComputeBuffer(1, sizeof(uint), ComputeBufferType.Raw);
        m_particlesToDrawIndicesCountBufferDump = new uint[1];


        m_appendParticlesToDrawCS.SetBuffer(0, "_positions", m_computeParticlesSimulationManager.GetPositionsCB());
        m_appendParticlesToDrawCS.SetBuffer(0, "_particlesToSpawnIDs", m_particlesToDrawIndicesAB);        
        m_appendParticlesToDrawCS.SetInt("k_threadGroupCount_x", m_computeParticlesSimulationManager.GetThreadGroupCount_X());
        m_appendParticlesToDrawCS.SetInt("k_threadGroupCount_y", m_computeParticlesSimulationManager.GetThreadGroupCount_Y());

        // setup mats for DrawProceduralIndrect
        for (int i = 0; i < m_proceduralParticlesMaterialArray.Length; i++)
        {
            m_proceduralParticlesMaterialArray[i].SetBuffer("_Positions", m_computeParticlesSimulationManager.GetPositionsCB());
            m_proceduralParticlesMaterialArray[i].SetBuffer("_Colors", m_computeParticlesSimulationManager.GetColorsCB());
            m_proceduralParticlesMaterialArray[i].SetBuffer("_Velocities", m_computeParticlesSimulationManager.GetVelocitiesCB());
            m_proceduralParticlesMaterialArray[i].SetBuffer("_ForwardVectors", m_computeParticlesSimulationManager.GetForwardVectorsCB());
            m_proceduralParticlesMaterialArray[i].SetBuffer("_UpVectors", m_computeParticlesSimulationManager.GetUpVectorsCB());
            m_proceduralParticlesMaterialArray[i].SetBuffer("_ParticlesToSpawnIDs", m_particlesToDrawIndicesAB);

            m_proceduralParticlesMaterialArray[i].SetFloat("_SimSpeed", m_computeParticlesSimulationManager.m_particleSimSpeed);
        }

        // setup mats for DrawProceduralIndrect
        for (int i = 0; i < m_meshInstancedParticlesMaterialArray.Length; i++)
        {
            m_meshInstancedParticlesMaterialArray[i].SetBuffer("_Positions", m_computeParticlesSimulationManager.GetPositionsCB());
            m_meshInstancedParticlesMaterialArray[i].SetBuffer("_Colors", m_computeParticlesSimulationManager.GetColorsCB());
            m_meshInstancedParticlesMaterialArray[i].SetBuffer("_Velocities", m_computeParticlesSimulationManager.GetVelocitiesCB());
            m_meshInstancedParticlesMaterialArray[i].SetBuffer("_ForwardVectors", m_computeParticlesSimulationManager.GetForwardVectorsCB());
            m_meshInstancedParticlesMaterialArray[i].SetBuffer("_UpVectors", m_computeParticlesSimulationManager.GetUpVectorsCB());
            m_meshInstancedParticlesMaterialArray[i].SetBuffer("_ParticlesToSpawnIDs", m_particlesToDrawIndicesAB);

            m_meshInstancedParticlesMaterialArray[i].SetFloat("_SimSpeed", m_computeParticlesSimulationManager.m_particleSimSpeed);
        }
        m_meshInstancedBounds = new Bounds(Vector3.zero, Vector3.one);
        m_meshInstancedBounds.size = Vector3.one * m_meshInstancedBoundsSizeScaler;

    }

    void OnDestroy()
    {
        if(m_particlesToDrawIndicesAB != null)
        {
            m_particlesToDrawIndicesAB.Release();
            m_particlesToDrawIndicesAB.Dispose();
        }
        if(m_particlesToDrawIndirectArgsBuffer != null)
        {
            m_particlesToDrawIndirectArgsBuffer.Release();
            m_particlesToDrawIndirectArgsBuffer.Dispose();
        }
        if(m_meshInstancedDrawIndirectArgsBuffer != null)
        {
            m_meshInstancedDrawIndirectArgsBuffer.Release();
            m_meshInstancedDrawIndirectArgsBuffer.Dispose();
        }

        if(m_particlesToDrawIndicesCountBuffer != null)
        {
            m_particlesToDrawIndicesCountBuffer.Release();
            m_particlesToDrawIndicesCountBuffer.Dispose();
        }

    }

    void Update()
    {
        // reset the append buffer count
        m_particlesToDrawIndicesAB.SetCounterValue(0);
        // do append buffer appending to create draw list of indices
        m_appendParticlesToDrawCS.Dispatch(0, m_computeParticlesSimulationManager.GetThreadGroupCount_X(), m_computeParticlesSimulationManager.GetThreadGroupCount_Y(), 1);
        // retrieve append count
        // buffer for DrawProceduralIndirect
        ComputeBuffer.CopyCount(m_particlesToDrawIndicesAB, m_particlesToDrawIndirectArgsBuffer, 0);


        // debug retrieve append count
        //UnityEngine.Profiling.Profiler.BeginSample("Copy Count");
        //ComputeBuffer.CopyCount(m_particlesToDrawIndicesAB, m_particlesToDrawIndicesCountBuffer, 0);
        //ComputeBuffer.CopyCount(m_computeParticlesSimulationManager.GetParticlesDeadListBuffer(), m_particlesToDrawIndicesCountBuffer, 0);
        //ComputeBuffer.CopyCount(m_computeParticlesSimulationManager.GetParticlesAliveListBuffer_Consume(), m_particlesToDrawIndicesCountBuffer, 0);
        //ComputeBuffer.CopyCount(m_computeParticlesSimulationManager.GetParticlesAliveListBuffer_Append(), m_particlesToDrawIndicesCountBuffer, 0);
        //ComputeBuffer.CopyCount(m_particlesToDrawIndicesAB, m_particlesToDrawIndicesCountBuffer, 0);
        
        //UnityEngine.Profiling.Profiler.EndSample();
        //UnityEngine.Profiling.Profiler.BeginSample("Get Data");
        //m_particlesToDrawIndicesCountBuffer.GetData(m_particlesToDrawIndicesCountBufferDump);
        //UnityEngine.Profiling.Profiler.EndSample();
        //m_particlesToDrawCount = m_particlesToDrawIndicesCountBufferDump[0];
        //Debug.Log(m_particlesToDrawCount);

        if (m_isRendering == true)
        {
            if (m_drawType == DrawType.DrawMeshIntancedIndirect)
            {

                m_meshInstancedParticlesMaterialArray[m_currentMeshInstancedParticlesMaterialIndex].SetFloat("_SimSpeed", m_computeParticlesSimulationManager.m_particleSimSpeed);
                m_meshInstancedParticlesMaterialArray[m_currentMeshInstancedParticlesMaterialIndex].SetFloat("_SizeScaler", m_meshInstancedMaterialSizeScaler);
                //m_meshInstancedParticlesMaterialArray[m_currentProceduralParticlesMaterialIndex].SetPass(0);

                Mesh mesh = m_meshIndirectMeshesArray[m_currentMeshInstancedMeshIndex];
                Material mat = m_meshInstancedParticlesMaterialArray[m_currentMeshInstancedParticlesMaterialIndex];
                m_meshInstancedBounds.size = Vector3.one * m_meshInstancedBoundsSizeScaler;
                m_drawMeshIndirectArgs[0] = mesh.GetIndexCount(0);
                m_meshInstancedDrawIndirectArgsBuffer.SetData(m_drawMeshIndirectArgs);
                // buffer for DrawMeshInstancedIndirect
                ComputeBuffer.CopyCount(m_particlesToDrawIndicesAB, m_meshInstancedDrawIndirectArgsBuffer, 4); // different location for count

                Graphics.DrawMeshInstancedIndirect(mesh, 0, mat, m_meshInstancedBounds, m_meshInstancedDrawIndirectArgsBuffer, 0, null, UnityEngine.Rendering.ShadowCastingMode.On, true);
            }
        }

    }
    

    void OnRenderObject()
    {
        if (Camera.current.CompareTag("ParticlesRenderCamera") == false && Camera.current.name != "SceneCamera")
            return;
        
        if (m_isRendering == true)
        {
            if (m_drawType == DrawType.DrawProceduralIndirect)
            {
                m_proceduralParticlesMaterialArray[m_currentProceduralParticlesMaterialIndex].SetFloat("_SimSpeed", m_computeParticlesSimulationManager.m_particleSimSpeed);
                m_proceduralParticlesMaterialArray[m_currentProceduralParticlesMaterialIndex].SetFloat("_SizeScaler", m_proceduralMaterialSizeScaler);
                m_proceduralParticlesMaterialArray[m_currentProceduralParticlesMaterialIndex].SetPass(0);

                //debug
                //uint[] argsBaseData = { m_particlesToDrawCount, 1, 0, 0 }; // {vertex count per instance, instance count, start vertex location, start instance location}
                //m_particlesToDrawIndirectArgumentsBuffer.SetData(argsBaseData);
                Graphics.DrawProceduralIndirect(MeshTopology.Points, m_particlesToDrawIndirectArgsBuffer, 0);
                // debug append count check version
                //Graphics.DrawProcedural(MeshTopology.Points, (int)m_particlesToDrawCount);

                
            }
        }
    }


}
