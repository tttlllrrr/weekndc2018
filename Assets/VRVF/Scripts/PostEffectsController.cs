﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class PostEffectsController : MonoBehaviour 
{
	public PostProcessingBehaviour[] m_postProcessingBehaviours;
	PostProcessingProfile[] m_postProcessingProfiles;
	ColorGradingModel.Settings[] m_colorGradingSettingsArray;

	void Start()
	{
		m_colorGradingSettingsArray = new ColorGradingModel.Settings[m_postProcessingBehaviours.Length];
		m_postProcessingProfiles = new PostProcessingProfile[m_postProcessingBehaviours.Length];
		for(int i = 0; i < m_colorGradingSettingsArray.Length; i++)
		{
			m_postProcessingProfiles[i] = Instantiate(m_postProcessingBehaviours[i].profile);
			m_postProcessingBehaviours[i].profile = m_postProcessingProfiles[i];
			m_colorGradingSettingsArray[i] = m_postProcessingBehaviours[i].profile.colorGrading.settings;
		}
		
	}

	public void Set_PostExposure(int index, float value)
	{
		m_colorGradingSettingsArray[index].basic.postExposure = value;
		m_postProcessingProfiles[index].colorGrading.settings = m_colorGradingSettingsArray[index];
	}
	public void Set_Temperature(int index, float value)
	{
		m_colorGradingSettingsArray[index].basic.temperature = value;
		m_postProcessingProfiles[index].colorGrading.settings = m_colorGradingSettingsArray[index];
	}
	public void Set_Tint(int index, float value)
	{
		m_colorGradingSettingsArray[index].basic.tint = value;
		m_postProcessingProfiles[index].colorGrading.settings = m_colorGradingSettingsArray[index];
	}
	public void Set_HueShift(int index, float value)
	{
		m_colorGradingSettingsArray[index].basic.hueShift = value;
		m_postProcessingProfiles[index].colorGrading.settings = m_colorGradingSettingsArray[index];
	}
	public void Set_Saturation(int index, float value)
	{
		m_colorGradingSettingsArray[index].basic.saturation = value;
		m_postProcessingProfiles[index].colorGrading.settings = m_colorGradingSettingsArray[index];
	}
	public void Set_Contrast(int index, float value)
	{
		m_colorGradingSettingsArray[index].basic.contrast = value;
		m_postProcessingProfiles[index].colorGrading.settings = m_colorGradingSettingsArray[index];
	}

}
