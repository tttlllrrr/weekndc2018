﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class SubtitlesController : MonoBehaviour 
{
	const string m_textFilename = "jpText.txt";
	//string fileAllText;
	string[] fileAllLines;

	int m_currentReadIndex = 0;

	public Text m_subtitlesText;
	//bool isSubtitlesVisible = true;
	public Slider m_subtitlesYShifter;
	float m_yShiftRange = 200.0f;

	void Start()
	{
		string filePath = Application.streamingAssetsPath + "/" + m_textFilename;
		//fileAllText = File.ReadAllText(filePath);
		fileAllLines = File.ReadAllLines(filePath);

	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.LeftArrow))
			m_currentReadIndex --;
		if(Input.GetKeyDown(KeyCode.RightArrow))
			m_currentReadIndex ++;
		Mathf.Clamp(m_currentReadIndex, 0, fileAllLines.Length);

		if(Input.GetKeyDown(KeyCode.UpArrow))
			m_subtitlesText.enabled = true;
		if(Input.GetKeyDown(KeyCode.DownArrow))
			m_subtitlesText.enabled = false;

		m_subtitlesText.text = fileAllLines[m_currentReadIndex];
	}

	public void ShiftSubtitlesY()
	{
		Vector3 lp = m_subtitlesText.transform.localPosition;
		lp.y = Mathf.Lerp(-m_yShiftRange, m_yShiftRange, m_subtitlesYShifter.value);
		m_subtitlesText.transform.localPosition = lp;
	}

}
