﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IM_PrefabsSpawner : MonoBehaviour
{
    public int m_spawnCount = 1000;
    public float m_spawnRadius = 100.0f;

    public GameObject m_IMPrefab;
    public Transform m_holderTransform;
    GameObject[] m_IM_GOs;
    Transform[] m_IM_Transforms;

    void Start()
    {
        m_IM_GOs = new GameObject[m_spawnCount];
        m_IM_Transforms = new Transform[m_spawnCount];
        for(int i = 0; i < m_IM_GOs.Length; i++)
        {
            m_IM_GOs[i] = (GameObject)Instantiate(m_IMPrefab);
            m_IM_GOs[i].name += i.ToString();
            m_IM_Transforms[i] = m_IM_GOs[i].transform;
            m_IM_Transforms[i].parent = m_holderTransform;

            // spawn pos
            m_IM_Transforms[i].localPosition = Random.insideUnitSphere * m_spawnRadius;
            m_IM_Transforms[i].localRotation = Random.rotationUniform;
        }
    }



}
