﻿using UnityEngine;
using System.Collections;

public class ComputeVectorFieldViewerManager : MonoBehaviour
{
    public ComputeShader m_vectorFieldViewerSimCS;
    public Material m_vectorFieldViewerMaterial;

    ComputeBuffer m_positionsCB;
    ComputeBuffer m_forceVectorsCB;
    ComputeBuffer m_colorsCB;

    ComputeVectorFieldManager m_computeVectorFieldManager;
    int k_vectorFieldSize;
    int k_vectorFieldElementsCount;

    public float m_spritesSizeScaler = 1.0f;

    const int k_numThreads = 4;

    public bool m_isRendering = true;

    void Start()
    {
        m_computeVectorFieldManager = FindObjectOfType<ComputeVectorFieldManager>();
        k_vectorFieldSize = m_computeVectorFieldManager.k_fieldWidth;
        k_vectorFieldElementsCount = k_vectorFieldSize * k_vectorFieldSize * k_vectorFieldSize;

        m_positionsCB = new ComputeBuffer(k_vectorFieldElementsCount, sizeof(float) * 4);
        m_forceVectorsCB = new ComputeBuffer(k_vectorFieldElementsCount, sizeof(float) * 4);
        m_colorsCB = new ComputeBuffer(k_vectorFieldElementsCount, sizeof(float) * 4);

        Vector4[] tempArray = new Vector4[k_vectorFieldElementsCount];
        for (int i = 0; i < tempArray.Length; i++)
        {
            tempArray[i] = new Vector4();
        }
        // set dummy data
        m_positionsCB.SetData(tempArray);
        m_forceVectorsCB.SetData(tempArray);
        m_colorsCB.SetData(tempArray);

        m_vectorFieldViewerSimCS.SetBuffer(0, "_positions", m_positionsCB);
        m_vectorFieldViewerSimCS.SetBuffer(0, "_forceVectors", m_forceVectorsCB);
        m_vectorFieldViewerSimCS.SetBuffer(0, "_colors", m_colorsCB);


        m_vectorFieldViewerMaterial.SetBuffer("_Positions", m_positionsCB);
        m_vectorFieldViewerMaterial.SetBuffer("_Velocities", m_forceVectorsCB);
        m_vectorFieldViewerMaterial.SetBuffer("_Colors", m_colorsCB);        

    }

    void Update()
    {
        m_vectorFieldViewerSimCS.SetTexture(0, "_vectorFieldTex", m_computeVectorFieldManager.VectorFieldTexture_Write());
        m_vectorFieldViewerSimCS.SetFloats("_vectorFieldManagerPos", m_computeVectorFieldManager.GetVectorFieldManagerPositionAsArray());
        m_vectorFieldViewerSimCS.SetFloat("_vectorFieldScale", m_computeVectorFieldManager.k_fieldScale);

        m_vectorFieldViewerSimCS.Dispatch(0, k_vectorFieldSize/ k_numThreads, k_vectorFieldSize/ k_numThreads, k_vectorFieldSize/ k_numThreads);

    }


    void OnRenderObject()
    {        
        if (Camera.current.CompareTag("ParticlesRenderCamera") == false && Camera.current.name != "SceneCamera")
            return;
        

        if (m_isRendering == true)
        {
            m_vectorFieldViewerMaterial.SetFloat("_SizeScaler", m_spritesSizeScaler);
            m_vectorFieldViewerMaterial.SetPass(0);

            Graphics.DrawProcedural(MeshTopology.Points, k_vectorFieldElementsCount);
        }
    }

    void OnDestroy()
    {
        m_positionsCB.Release();
        m_forceVectorsCB.Release();
        m_colorsCB.Release();
    }


}
