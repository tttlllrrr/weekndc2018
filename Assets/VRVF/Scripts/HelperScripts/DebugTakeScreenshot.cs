﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugTakeScreenshot : MonoBehaviour
{
    public string folder = "ScreenshotFolder";

    void Start()
    {
        // Create the folder
        System.IO.Directory.CreateDirectory(folder);
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.P))
        {
            
            // Append filename to folder name (format is '0005 shot.png"')
            string name = string.Format("{0}/{1:D04} shot.png", folder, Time.frameCount);

            // Capture the screenshot to the specified file.
            ScreenCapture.CaptureScreenshot(name);
            
            
        }
    }


}
