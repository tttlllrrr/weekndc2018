﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplaysActivator : MonoBehaviour
{
    void Start()
    {
        Debug.Log("Number of displays: " + Display.displays.Length);

        if (Display.displays.Length > 1)
            Display.displays[1].Activate();

        if (Display.displays.Length > 2)
        {
            Display.displays[2].SetRenderingResolution(160, 90);
            Display.displays[2].Activate();
        }
        if (Display.displays.Length > 3)
        {
            Display.displays[3].SetRenderingResolution(160, 90);
            Display.displays[3].Activate();
        }

    }
}
