﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTransformLocal : MonoBehaviour
{
    public Vector3 m_rotationVec;

    void Update()
    {
        transform.localEulerAngles += transform.localRotation * m_rotationVec * Time.deltaTime;
    }

}
