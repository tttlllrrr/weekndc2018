﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetTargetFramerate : MonoBehaviour
{
    public int m_targetFramerate = 60;

    void Start()
    {
        Application.targetFrameRate = m_targetFramerate;
    }
}
