﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// NOTE !!!!
// Requires that camera calls Clear Random Write Target !!!!

public class ComputeParticlesMaterialBasedGeneration : MonoBehaviour
{
    ComputeParticleSimulationManager m_computeParticlesSimulationManager;
    ComputeParticlesGenerationManager m_computeParticlesGenerationManager;

    public Material[] m_targetMaterials;

    // used to save set of spawn points/data so it can be resused (instead of live perspective only)
    ComputeBuffer m_tempStore_ParticleSpawnData_Buffer;
    ComputeBuffer m_tempStore_ParticleSpawnData_Counter;

    // these are the ones that actually get sent to the Generation Compute Shader, need an extra copy to use CS kernel as is since it Consumes the buffer
    // need to recopy data over every use. TODO: Change this
    ComputeBuffer m_tempStore_ParticleSpawnData_Consume;
    ComputeBuffer m_tempStore_ParticleSpawnData_Consume_Counter;


    public ComputeShader m_copyParticleSpawnDataCS;

    public enum GenerationType
    {
        Live,
        StaticCopy
    }
    public GenerationType m_generationType = GenerationType.Live;

    public bool m_isGenerating = true;

    void Start()
    {
        m_computeParticlesSimulationManager = FindObjectOfType<ComputeParticleSimulationManager>();
        m_computeParticlesGenerationManager = FindObjectOfType<ComputeParticlesGenerationManager>();

        for(int i = 0; i < m_targetMaterials.Length; i++)
        {
            m_targetMaterials[i].SetBuffer("_positions", m_computeParticlesSimulationManager.GetPositionsCB());
            m_targetMaterials[i].SetBuffer("_velocities", m_computeParticlesSimulationManager.GetVelocitiesCB());
            m_targetMaterials[i].SetBuffer("_colors", m_computeParticlesSimulationManager.GetColorsCB());
            m_targetMaterials[i].SetBuffer("_forwardVectors", m_computeParticlesSimulationManager.GetForwardVectorsCB());
            m_targetMaterials[i].SetBuffer("_upVectors", m_computeParticlesSimulationManager.GetUpVectorsCB());

            m_targetMaterials[i].SetBuffer("_particlesAliveListBuffer_Append", m_computeParticlesSimulationManager.GetParticlesAliveListBuffer_Append());
            m_targetMaterials[i].SetBuffer("_particlesDeadListBuffer_Consume", m_computeParticlesSimulationManager.GetParticlesDeadListBuffer());

            m_targetMaterials[i].SetBuffer("_particleSpawnData", m_computeParticlesGenerationManager.GetParticleSpawnDataAB());
        }

        int stride = System.Runtime.InteropServices.Marshal.SizeOf(typeof(ComputeParticlesGenerationManager.ParticleSpawnData));
        m_tempStore_ParticleSpawnData_Buffer = new ComputeBuffer(m_computeParticlesSimulationManager.GetParticleCount(), stride , ComputeBufferType.Append);
        m_tempStore_ParticleSpawnData_Buffer.SetCounterValue(0);
        m_tempStore_ParticleSpawnData_Counter = new ComputeBuffer(1, sizeof(uint), ComputeBufferType.Raw);

        m_tempStore_ParticleSpawnData_Consume = new ComputeBuffer(m_computeParticlesSimulationManager.GetParticleCount(), stride, ComputeBufferType.Append);
        m_tempStore_ParticleSpawnData_Consume.SetCounterValue(0);
        m_tempStore_ParticleSpawnData_Consume_Counter = new ComputeBuffer(1, sizeof(uint), ComputeBufferType.Raw);

    }

    

    void LateUpdate()
    {
        for (int i = 0; i < m_targetMaterials.Length; i++)
        {            
            // need to refresh becuase they get swapped on the back end
            m_targetMaterials[i].SetBuffer("_particlesAliveListBuffer_Append", m_computeParticlesSimulationManager.GetParticlesAliveListBuffer_Append());
            m_targetMaterials[i].SetBuffer("_particlesDeadListBuffer_Consume", m_computeParticlesSimulationManager.GetParticlesDeadListBuffer());

            m_targetMaterials[i].SetBuffer("_particleSpawnData", m_computeParticlesGenerationManager.GetParticleSpawnDataAB());
        }

        if (m_isGenerating == true)
        {
            if (m_generationType == GenerationType.Live)
            {
                m_computeParticlesGenerationManager.SpawnParticles_AppendFrom_ParticleSpawnAB(m_computeParticlesGenerationManager.GetParticleSpawnDataAB(), m_computeParticlesGenerationManager.GetFresh_ParticleSpawnDataCounter());
            }
            else
            {
                // NOTE: this is silly but hacky workaround: 
                // need to copy spawn data to extra buffer every frame in order to use the same Generation kernel as the standard one (which relies on Consume -ing the ParticleSpawnData buffer)
                CopyLocalTempPSDToConsume();
                m_computeParticlesGenerationManager.SpawnParticles_AppendFrom_ParticleSpawnAB(m_tempStore_ParticleSpawnData_Consume, m_tempStore_ParticleSpawnData_Consume_Counter);
            }
        }

        // NOTE: OPTIONAL if want a clean slate every frame -->  clean up list after generating them 
        m_computeParticlesGenerationManager.SetParticleSpawnABCounter(0);
    }

    void CopyParticleSpawnDataToAppendBuffer(ComputeBuffer psd_AB_Source, ComputeBuffer psd_Counter_Source, ComputeBuffer psd_AB_Destination, ComputeBuffer psd_Counter_Destination)
    {
        int kernelIndex = 0;
        m_copyParticleSpawnDataCS.SetInt("k_threadGroupCount_x", m_computeParticlesSimulationManager.GetThreadGroupCount_X());
        m_copyParticleSpawnDataCS.SetInt("k_threadGroupCount_y", m_computeParticlesSimulationManager.GetThreadGroupCount_Y());
        m_copyParticleSpawnDataCS.SetBuffer(kernelIndex, "_particleSpawnData_Buffer_Source", psd_AB_Source);
        m_copyParticleSpawnDataCS.SetBuffer(kernelIndex, "_particleSpawnData_Counter_Source", psd_Counter_Source);
        // empty it before it gets Append into it in the CS
        psd_AB_Destination.SetCounterValue(0);
        m_copyParticleSpawnDataCS.SetBuffer(kernelIndex, "_particleSpawnData_Buffer_Destination", psd_AB_Destination);

        // run the copy/append compute shader
        m_copyParticleSpawnDataCS.Dispatch(kernelIndex, m_computeParticlesSimulationManager.GetThreadGroupCount_X(), m_computeParticlesSimulationManager.GetThreadGroupCount_Y(), 1);

        // once copy is complete, refresh the counter for the destination buffer
        ComputeBuffer.CopyCount(psd_AB_Destination, psd_Counter_Destination, 0);
    }

    // designed to be called once when "freeze" in data is desired
    public void CopyLivePSDToLocalTemp()
    {
        CopyParticleSpawnDataToAppendBuffer(m_computeParticlesGenerationManager.GetParticleSpawnDataAB(), m_computeParticlesGenerationManager.GetFresh_ParticleSpawnDataCounter(), m_tempStore_ParticleSpawnData_Buffer, m_tempStore_ParticleSpawnData_Counter);
        Debug.Log("SAVED LIVE PSD SNAPSHOT");
    }
    // designed to be called before everytime CS Generation is desired
    void CopyLocalTempPSDToConsume()
    {
        CopyParticleSpawnDataToAppendBuffer(m_tempStore_ParticleSpawnData_Buffer, m_tempStore_ParticleSpawnData_Counter, m_tempStore_ParticleSpawnData_Consume, m_tempStore_ParticleSpawnData_Consume_Counter);
    }


    
    //// DEBUG CONTROLS TODO: NO CONTROLS SHOULD BE HERE
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            CopyLivePSDToLocalTemp();
        }
    }

    void OnDestroy()
    {
        if (m_tempStore_ParticleSpawnData_Buffer != null)
        {
            m_tempStore_ParticleSpawnData_Buffer.Release();
            m_tempStore_ParticleSpawnData_Counter.Release();
            m_tempStore_ParticleSpawnData_Consume.Release();
            m_tempStore_ParticleSpawnData_Consume_Counter.Release();

            m_tempStore_ParticleSpawnData_Buffer.Dispose();
            m_tempStore_ParticleSpawnData_Counter.Dispose();
            m_tempStore_ParticleSpawnData_Consume.Dispose();
            m_tempStore_ParticleSpawnData_Consume_Counter.Dispose();
        }
    }


}
