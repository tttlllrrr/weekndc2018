﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// assumes this will get attached to gameobject with mesh/material
public class SurfEmissoinMatController : MonoBehaviour
{

    Material m_duplicateMaterial;

    public float m_scrollIn_Duration = 1.0f;
    public AnimationCurve m_scrollIn_AnimationCurve = AnimationCurve.EaseInOut(0,0,1,1);

    public bool m_isActiveAfterStart = true;
    

    void Start()
    {
        m_duplicateMaterial = GetComponent<MeshRenderer>().material;
        
        gameObject.SetActive(m_isActiveAfterStart);
    }

    void OnEnable()
    {
        if (m_duplicateMaterial == null)
            return;
        //Debug.Log("LAUNCHING");
        StopAllCoroutines();
        StartCoroutine(Animate_ScrollIng());
    }

    IEnumerator Animate_ScrollIng()
    {
        float timeCounter = 0;
        m_duplicateMaterial.SetFloat("_EmmitUV_Target_Y", 0);
        while (timeCounter < m_scrollIn_Duration)
        {
            float progress = timeCounter / m_scrollIn_Duration;
            float step = m_scrollIn_AnimationCurve.Evaluate(progress);

            m_duplicateMaterial.SetFloat("_EmmitUV_Target_Y", step);
            timeCounter += Time.deltaTime;
            yield return null;
        }
        m_duplicateMaterial.SetFloat("_EmmitUV_Target_Y", 1);
    }


}
