﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class ManuallySetLightDirectionToMat : MonoBehaviour
{
    public Transform m_lightTransform;
    public Material[] m_targetMaterials;
    string m_lightDirectionPropertyName = "_LightDirection";

    void Update()
    {
        Vector3 lightDir = m_lightTransform.forward;
        Vector4 lightDirVec = new Vector4(lightDir.x, lightDir.y, lightDir.z, 1);
        for(int i = 0; i < m_targetMaterials.Length; i++)
        {
            m_targetMaterials[i].SetVector(m_lightDirectionPropertyName, lightDirVec); // TODO: USE INT VALUE INSTEAD OF STRING
        }

    }


}
