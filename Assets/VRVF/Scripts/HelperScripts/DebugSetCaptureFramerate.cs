﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugSetCaptureFramerate : MonoBehaviour
{
    public int m_targetCaptureFramerate = 60;

    void Start()
    {
        Time.captureFramerate = m_targetCaptureFramerate;
    }


}
