﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraDynamicRenderTextureController : MonoBehaviour
{
    public Camera m_targetCamera;

    public int m_maxRes_X = 1920;
    public int m_maxRes_Y = 1080;
    public int m_stepCount = 20;
    RenderTexture[] m_renderTexturesArray;

    [Range(0, 42)]
    public int m_currentRenderTextureIndex = 0;

    int m_previousRTIndex = 0;

    void Start()
    {
        m_renderTexturesArray = new RenderTexture[m_stepCount];
        for(int i = 0; i < m_renderTexturesArray.Length; i++)
        {
            int numerator = m_stepCount - i;
            float div = (float)numerator / (float)m_stepCount;
            int stepRez_X = Mathf.FloorToInt((float)m_maxRes_X * div);
            int stepRez_Y = Mathf.FloorToInt((float)m_maxRes_Y * div);
            m_renderTexturesArray[i] = new RenderTexture(stepRez_X, stepRez_Y, 0, RenderTextureFormat.ARGB32);            
        }

        SetCameraRenderTexture(m_currentRenderTextureIndex);
        m_previousRTIndex = m_currentRenderTextureIndex;
    }

    // TODO : BLARGH, DEBUG CONTROLS < NEED TO GET RID OF THIS
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
            m_currentRenderTextureIndex += 1;
        if (Input.GetKeyDown(KeyCode.LeftArrow))
            m_currentRenderTextureIndex -= 1;
        m_currentRenderTextureIndex = Mathf.Clamp(m_currentRenderTextureIndex, 0, m_stepCount - 1);

        if (m_currentRenderTextureIndex != m_previousRTIndex)
            SetCameraRenderTexture(m_currentRenderTextureIndex);

        m_previousRTIndex = m_currentRenderTextureIndex;
    }

    void SetCameraRenderTexture(int rtIndex)
    {
        m_targetCamera.targetTexture = m_renderTexturesArray[rtIndex];
    }
    
}
