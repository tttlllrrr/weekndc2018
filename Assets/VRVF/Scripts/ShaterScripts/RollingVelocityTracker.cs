﻿using UnityEngine;
using System.Collections;

public class RollingVelocityTracker : MonoBehaviour
{
    const int k_rollingAverageSamples = 5;    
    int m_currentSampleIndex = 0;
    
    Vector3[] m_rollingAverageArray;    
    Vector3 m_previousPosition;

    Vector3 m_currentRollingAverageVelocity;

    void Start()
    {
        m_rollingAverageArray = new Vector3[k_rollingAverageSamples];    
        m_previousPosition = transform.position;
    }

    void Update()
    {
        Vector3 currentPosition = transform.position;
        Vector3 currentVel = (currentPosition - m_previousPosition) / Time.deltaTime;

        m_rollingAverageArray[m_currentSampleIndex] = currentVel;
        m_currentSampleIndex += 1;
        m_currentSampleIndex = m_currentSampleIndex % (k_rollingAverageSamples - 1);

        Vector3 summerVec = Vector3.zero;
        for(int i = 0; i < m_rollingAverageArray.Length; i++)
        {
            summerVec += m_rollingAverageArray[i];
        }
        m_currentRollingAverageVelocity = summerVec / (float)k_rollingAverageSamples;        
        m_previousPosition = currentPosition;

        //Debug.Log(gameObject.name + " rolling vel: " + GetCurrentRollingSpeed());
    }

    public Vector3 GetCurrentRollingVelocity()
    {
        return m_currentRollingAverageVelocity;
    }

    public float GetCurrentRollingSpeed()
    {
        return m_currentRollingAverageVelocity.magnitude;
    }

}
