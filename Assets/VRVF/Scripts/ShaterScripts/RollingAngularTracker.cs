﻿using UnityEngine;
using System.Collections;

public class RollingAngularTracker : MonoBehaviour
{
    const int rollingAverageSamples = 15;
    int m_currentSampleIndex = 0;

    Vector3[] m_angularSamplesArray;
    Quaternion m_previousRotation;

    Vector3 m_currentRollingAverageAngular;

	GameObject m_calculatorObject;
	Rigidbody m_calculatorRigidbody;

    void Start()
    {
        m_angularSamplesArray = new Vector3[rollingAverageSamples];
        m_previousRotation = transform.rotation;

		m_calculatorObject = new GameObject(gameObject.name + "Rolling Angular Calculator");
		m_calculatorObject.AddComponent<Rigidbody> ();
		m_calculatorRigidbody = m_calculatorObject.GetComponent<Rigidbody> ();
		m_calculatorRigidbody.isKinematic = true;
		m_calculatorRigidbody.useGravity = false;
    }

    void Update()
    {
		/*
        Quaternion currentRotation = transform.rotation;
        Vector3 currentDelta = (currentRotation * Quaternion.Inverse(m_previousRotation)) .eulerAngles;

		// disgusting hack to handle the fact that 0 deg rot gets reported sometimes as 360 deg
		if ((int)currentDelta.x == 359)
			currentDelta.x = 0;
		if ((int)currentDelta.y == 359)
			currentDelta.y = 0;
		if ((int)currentDelta.z == 359)
			currentDelta.z = 0;
		*/

		m_angularSamplesArray [m_currentSampleIndex] = m_calculatorRigidbody.angularVelocity; // currentDelta;
        m_currentSampleIndex += 1;
        m_currentSampleIndex = m_currentSampleIndex % (rollingAverageSamples - 1);

        Vector3 summerVec = Vector3.zero;
        for(int i = 0; i < m_angularSamplesArray.Length; i++)
        {
            summerVec += m_angularSamplesArray[i];
        }
        m_currentRollingAverageAngular = summerVec / (float)rollingAverageSamples;
        //m_previousRotation = currentRotation;

		m_calculatorRigidbody.MoveRotation(transform.rotation);

    }

    public Vector3 GetCurrentRolling_AngularVelocity()
    {
        return m_currentRollingAverageAngular;
    }

	public float GetCurrentRolling_AngularMagnitude()
	{
		return m_currentRollingAverageAngular.magnitude;
	}

	// cleanup dependecies
	void OnDestroy()
	{
		Destroy (m_calculatorObject);
	}

}
