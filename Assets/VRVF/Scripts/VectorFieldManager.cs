﻿using UnityEngine;
using System.Collections;

public class VectorFieldManager : MonoBehaviour 
{
	public Vector3[][][] m_vectorField3DArray;
	public int c_fieldSizeCount = 10;
	public float c_fieldSizeScale = 1.0f; // world size scaler
	public float m_forceMagnitudeClamp = 10.0f;

	ForceSource[] m_forceSourcesArray;
	Vector3[] m_forceSourcesPreviousPositionArray;

	public delegate void VectorFieldUpdated();
	public event VectorFieldUpdated e_VectorFieldUpdated;

    public Texture3D m_vectorField3DTexture;
    Color[] m_vectorFieldAsColorsArray;

	void Start()
	{
		m_vectorField3DArray = new Vector3[c_fieldSizeCount][][];
        m_vectorField3DTexture = new Texture3D(c_fieldSizeCount, c_fieldSizeCount, c_fieldSizeCount, TextureFormat.RGBA32, false);
        m_vectorFieldAsColorsArray = new Color[c_fieldSizeCount * c_fieldSizeCount * c_fieldSizeCount];

        int linearIndex = 0;
        for (int i = 0; i < c_fieldSizeCount; i++)
		{
			m_vectorField3DArray[i] = new Vector3[c_fieldSizeCount][];
			for(int j = 0; j < c_fieldSizeCount; j++)
			{
				m_vectorField3DArray[i][j] = new Vector3[c_fieldSizeCount];
                for (int k = 0; k < c_fieldSizeCount; k++)
                {
                    linearIndex = i + j * c_fieldSizeCount + k * c_fieldSizeCount * c_fieldSizeCount;

                    m_vectorField3DArray[i][j][k] = Random.onUnitSphere; //new Vector3(0,0,0);
                    m_vectorFieldAsColorsArray[linearIndex] = new Color();

                    m_vectorFieldAsColorsArray[linearIndex].r = m_vectorField3DArray[i][j][k].x;
                    m_vectorFieldAsColorsArray[linearIndex].g = m_vectorField3DArray[i][j][k].y;
                    m_vectorFieldAsColorsArray[linearIndex].b = m_vectorField3DArray[i][j][k].z;

                    m_vectorFieldAsColorsArray[linearIndex] = new Color(0, 0, 0, 0);

                    //linearIndex++;
                }
			}
		}
        m_vectorField3DTexture.SetPixels(m_vectorFieldAsColorsArray);
        m_vectorField3DTexture.Apply();

		m_forceSourcesArray = FindObjectsOfType<ForceSource>();
		m_forceSourcesPreviousPositionArray = new Vector3[m_forceSourcesArray.Length];
		for(int i = 0; i < m_forceSourcesPreviousPositionArray.Length; i++)
			m_forceSourcesPreviousPositionArray[i] = m_forceSourcesArray[i].transform.position;

        
	}

	void Update()
	{
		// check to see if vector field should be updated
		for(int i = 0; i < m_forceSourcesArray.Length; i++)
		{
			if(m_forceSourcesArray[i].transform.position != m_forceSourcesPreviousPositionArray[i])
				UpdateVectorFieldWithForce(m_forceSourcesArray[i]);

		}


		for(int i = 0; i < m_forceSourcesPreviousPositionArray.Length; i++)
			m_forceSourcesPreviousPositionArray[i] = m_forceSourcesArray[i].transform.position;
	}

	// TODO :  change this so that all relevant force sources get passed in at once, to avoid extra loops for every source
	void UpdateVectorFieldWithForce(ForceSource forceSource)
	{
        
        // update the vector field math
        int linearIndex = 0;
        for (int i = 0; i < c_fieldSizeCount; i ++)
		{
			for(int j = 0; j < c_fieldSizeCount; j++)
			{
				for(int k = 0; k < c_fieldSizeCount; k++)
				{
                    linearIndex = i + j * c_fieldSizeCount + k * c_fieldSizeCount * c_fieldSizeCount;

                    Vector3 vectorPos = GetFieldVectorWorldPosition(i,j,k);
					Vector3 vectorBefore = m_vectorField3DArray[i][j][k];
					m_vectorField3DArray[i][j][k] = CalculateFieldVectorAfterForce(vectorBefore, vectorPos, forceSource);

                    //m_vectorField3DArray[i][j][k] = new Vector3(i, j, k)/(float)c_fieldSizeCount;


                    m_vectorFieldAsColorsArray[linearIndex].r = Mathf.Abs(m_vectorField3DArray[i][j][k].x);
                    m_vectorFieldAsColorsArray[linearIndex].g = Mathf.Abs(m_vectorField3DArray[i][j][k].y);
                    m_vectorFieldAsColorsArray[linearIndex].b = Mathf.Abs(m_vectorField3DArray[i][j][k].z);
                    
                    //linearIndex++;
                }
			}
		}
        m_vectorField3DTexture.SetPixels(m_vectorFieldAsColorsArray);
        m_vectorField3DTexture.Apply();

        if (e_VectorFieldUpdated != null)
			e_VectorFieldUpdated();
	}

	public Vector3 GetFieldVectorWorldPosition(int index_X, int index_Y, int index_Z)
	{
		return new Vector3(index_X * c_fieldSizeScale, index_Y * c_fieldSizeScale, index_Z * c_fieldSizeScale) + transform.position;
	}

	Vector3 CalculateFieldVectorAfterForce(Vector3 originalFieldVector, Vector3 orignalFieldVectorPosition, ForceSource forceSource)
	{
		Vector3 forceSourceDirection = (forceSource.transform.position - orignalFieldVectorPosition).normalized;
		float distanceToForceSource = Vector3.Distance(orignalFieldVectorPosition, forceSource.transform.position);

		float forceSourceDropoff;
		// linear force falloff
		forceSourceDropoff = 1.0f/(1.0f + distanceToForceSource);

		Vector3 resultVector;
		resultVector = originalFieldVector + ( forceSourceDirection * forceSource.GetCurrentForceScale() * forceSourceDropoff );
		resultVector =  Vector3.ClampMagnitude(resultVector, m_forceMagnitudeClamp);

		return resultVector;
	}

    public Vector4 FindClosestForceVectorAndDist(Vector3 particlePosition)
    {
        //Profiler.BeginSample("FindClosestForceVector");

        Vector3 forceVector = Vector3.zero;
        float distanceToForceVector = 0.0f;
        // do position offset
        Vector3 relativeParticlePos = particlePosition - transform.position;

        // clamp the offset position to field limit to ensure we always get affected by a vector
        float fieldRange = c_fieldSizeScale * c_fieldSizeCount;
        Vector3 particlePosInField = new Vector3(Mathf.Clamp(relativeParticlePos.x, -fieldRange, fieldRange),
                                                 Mathf.Clamp(relativeParticlePos.y, -fieldRange, fieldRange),
                                                 Mathf.Clamp(relativeParticlePos.z, -fieldRange, fieldRange));

        // offset again by 1 times scale (to fix off by 1 index error)...?
       // particlePosInField -= c_fieldSizeScale * Vector3.one;

        // normalize pos by dividing by field size scale value
        int vectorIndex_x = Mathf.RoundToInt(particlePosInField.x/ c_fieldSizeScale);
        int vectorIndex_y = Mathf.RoundToInt(particlePosInField.y/ c_fieldSizeScale);
        int vectorIndex_z = Mathf.RoundToInt(particlePosInField.z/ c_fieldSizeScale);

        // sigh, cheating for now (it's practically 3am :/)  by clamping indices
        int maxIndex = c_fieldSizeCount - 1;
        vectorIndex_x = Mathf.Clamp(vectorIndex_x, 0, maxIndex);
        vectorIndex_y = Mathf.Clamp(vectorIndex_y, 0, maxIndex);
        vectorIndex_z = Mathf.Clamp(vectorIndex_z, 0, maxIndex);

        forceVector = m_vectorField3DArray[vectorIndex_x][vectorIndex_y][vectorIndex_z];
        distanceToForceVector = Vector3.Distance(particlePosition, GetFieldVectorWorldPosition(vectorIndex_x, vectorIndex_y, vectorIndex_z));

        //Profiler.EndSample();

        return new Vector4(forceVector.x, forceVector.y, forceVector.z, distanceToForceVector);
    }

}
