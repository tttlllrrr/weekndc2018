﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraModeController : MonoBehaviour 
{
	public TransformLerper m_cameraTransformLerper;
	public Transform m_cameraOrbitTarget;
	public Transform m_cameraChaseTarget;
	public Transform m_cameraChaseLookAtTarget;


	public bool m_isCameraChasingType = false;

	void Update()
	{	
		if(m_isCameraChasingType == true)
		{
			m_cameraTransformLerper.m_targetTransform = m_cameraChaseTarget;
			m_cameraTransformLerper.m_lookAtTarget = m_cameraChaseLookAtTarget;
			m_cameraTransformLerper.m_isLookAt = true;
		}
		else
		{
			m_cameraTransformLerper.m_targetTransform = m_cameraOrbitTarget;
			m_cameraTransformLerper.m_isLookAt = false;
		}

	}

	public void ToggleCameraTarget()
	{
		m_isCameraChasingType = !m_isCameraChasingType;
	}


}
