﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IM_MPBController : MonoBehaviour
{
    public Transform m_meshesHolderTransform;
    MeshRenderer[] m_meshRenderersArray;
    MaterialPropertyBlock[] m_mpbArray;
    public bool m_isInitComplete = false;
    
    // global overiders
    public float m_uvScale_x = 1.0f;
    public float m_uvOffset_x = 0.0f;
    public float m_uvScaler_y = 1.0f;

    AudioReactiveTextureManager m_artm;

    void Start()
    {
        m_artm = FindObjectOfType<AudioReactiveTextureManager>();
    }

    void Init_IM_MPBController()
    {
        // seems to come out ordered correctly, following hiearchy, so no need to sort again
        m_meshRenderersArray = m_meshesHolderTransform.GetComponentsInChildren<MeshRenderer>();
        if (m_meshRenderersArray.Length == 0)
            return;

        m_mpbArray = new MaterialPropertyBlock[m_meshRenderersArray.Length];
        for (int i = 0; i < m_mpbArray.Length; i++)
            m_mpbArray[i] = new MaterialPropertyBlock();

        SetMPBValues_TexturesSystem();
        m_isInitComplete = true;
    }

    void Update()
    {
        if(m_isInitComplete == false)
        {
            Init_IM_MPBController();
        }

        //SetMPBValues_TexturesSystem();
    }

    // global setter
    void SetMPBValues_TexturesSystem()
    {
        for (int i = 0; i < m_mpbArray.Length; i++)
        {
            float uvScale_x = Random.Range(0.5f, m_uvScale_x); //m_uvScale_x;
            float uvScale_y = Random.Range(0.27f, m_uvScaler_y); //m_uvScaler_y * (1.0f / (float)m_mpbArray.Length);
            float uvOffset_x = Random.Range(0.000f, m_uvOffset_x);//m_uvOffset_x;
            float uvOffset_y = 0.0f; //(1.0f - uvScale_y * (float)i);
            m_mpbArray[i].SetFloat("_HeightScale", m_artm.m_heightScale);
            m_mpbArray[i].SetFloat("_TextureScaleX", uvScale_x);
            m_mpbArray[i].SetFloat("_TextureScaleY", uvScale_y);
            m_mpbArray[i].SetFloat("_TextureOffsetX", uvOffset_x);
            m_mpbArray[i].SetFloat("_TextureOffsetY", uvOffset_y);

            m_meshRenderersArray[i].SetPropertyBlock(m_mpbArray[i]);
        }
    }



}
