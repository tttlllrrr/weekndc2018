﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

public static class WindowController
{
    private delegate bool EnumThreadDelegate(IntPtr Hwnd, IntPtr lParam);

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool EnumThreadWindows(uint dwThreadId, EnumThreadDelegate lpfn, IntPtr lParam);

    [DllImport("kernel32.dll")]
    private static extern uint GetCurrentThreadId();

    [DllImport("user32.dll")]
    private static extern IntPtr GetParent(IntPtr wnd);

    public static IntPtr GetOurWindow()
    {
        IntPtr ret = IntPtr.Zero;
        EnumThreadWindows(GetCurrentThreadId(), (wnd, lParam) =>
        {
            if (GetParent(wnd) == IntPtr.Zero)
            {
                ret = wnd;
                return false;
            }
            else
            {
                return true;
            }
        }, IntPtr.Zero);
        return ret;
    }

    [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
    public static extern IntPtr SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int wFlags);

    private const int SWP_NOSIZE = 0x1;
    private const int SWP_NOMOVE = 0x2;
    private const int SWP_NOZORDER = 0x4;
    private const int SWP_NOREDRAW = 0x8;
    private const int SWP_NOACTIVATE = 0x10;
    private const int SWP_NOOWNERZORDER = 0x20;

	public static void SetWindowResolution(int x, int y)
	{
		Debug.Log("Setting screen resoulution: " + x + ", " + y);
		Screen.SetResolution(x, y, false);
	}

    public static void SetOurWindowPosition(int x, int y)
    {
        IntPtr ourWindow = GetOurWindow();
        if (ourWindow == IntPtr.Zero)
            Debug.Log("Failed to find our window!!");
        else
        {
            Debug.Log(string.Format("Found our window handle - 0x{0:X8}", ourWindow.ToInt32()));
            SetWindowPos(ourWindow,
                                0,
                                x,
                                y,
                                0,
                                0,
                                SWP_NOSIZE);
        }

    }

}