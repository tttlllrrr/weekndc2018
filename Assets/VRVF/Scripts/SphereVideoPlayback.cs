﻿using UnityEngine;
using System.Collections;

public class SphereVideoPlayback : MonoBehaviour
{
    public Material[] m_movieTexturesMaterials;
    public MovieTexture m_movieTexture;

    void Start()
    {
        for(int i = 0; i < m_movieTexturesMaterials.Length; i++)
        {
            m_movieTexturesMaterials[i].mainTexture = m_movieTexture;
        }
        m_movieTexture.Play();
    }



}
