﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomActionLauncher : MonoBehaviour 
{
	ComputeParticleSimulationManager m_simManager;
	ParametersManager m_parametersManager;

	float m_nextTimeTrigger;
	float m_timeCounter = 0;

    public float m_minTime = 0.250f;
    public float m_maxTime = 5.0f;

    void Start()
	{	
		m_simManager = FindObjectOfType<ComputeParticleSimulationManager>();
		m_parametersManager = FindObjectOfType<ParametersManager>();

		m_nextTimeTrigger = GetRandomTimeRange();
	}

	float GetRandomTimeRange()
	{
		return Random.Range(m_minTime, m_maxTime);
	}

	void Update()
	{
		m_timeCounter += Time.deltaTime;
		if(m_timeCounter >  m_nextTimeTrigger)
		{
			int randomChoice = Random.Range(0,11);
			if(randomChoice < 1)
			{
				m_simManager.m_particleSimSpeed = Random.Range(-2.2750f,2.2750f);
			}
			else if(randomChoice < 2)
			{
				m_simManager.m_particleSimSpeed = Random.Range(-2.0f,2.0f);
			}
			else if(randomChoice < 3)
			{
				m_parametersManager.ClearVectorField();
                m_simManager.m_particleSimSpeed = 0.25f;
            }
			else if(randomChoice < 4)
			{
				m_parametersManager.FlipVectorField();
                m_simManager.m_particleSimSpeed = 0.5f;
            }
			else if(randomChoice < 5)
			{
				m_parametersManager.SetGeneratorTypeTo_Circle();
			}
			else if(randomChoice < 6)
			{
				m_parametersManager.SetGeneratorTypeTo_Flower();
			}
			else if(randomChoice < 7)
			{
				m_parametersManager.VectorField_SetSin_Dispatch();
			}
			else if(randomChoice < 8)
			{
				m_parametersManager.VectorField_SetSphere_Dispatch();
			}
			else if(randomChoice < 9)
			{
				m_simManager.m_particleSimSpeed = Random.Range(-2.250f,2.250f);
			}
			else if(randomChoice < 10)
			{
				m_parametersManager.ClearVectorField();
                m_simManager.m_particleSimSpeed = 2.25f;
            }
			else if(randomChoice < 11)
			{
				m_parametersManager.FlipVectorField();
                m_simManager.m_particleSimSpeed = 1.15f;
            }
			else if(randomChoice < 12)
			{
				m_simManager.m_particleSimSpeed = Random.Range(-1,1);
			}

			//Debug.Log("Action LAunched: " + randomChoice);
			m_timeCounter = 0;
		}


	}

}
