﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class ParametersManager : MonoBehaviour
{
    public Camera m_mainCamera;
    Bloom m_bloom;
    MotionBlur m_motionBlur;
    Color m_backGroundColor;
    ComputeVectorFieldManager m_computeVectorFieldManager;
    ComputeParticleSimulationManager m_computeParticlesSimulationManager;
    ComputeVectorFieldViewerManager m_computeVectorFieldViewerManager;
    ParticleGeneratorArrayManager m_particleGeneratorArrayManager;
    FrequencyDataManager m_frequencyDataManager;
    ComputeParticlesGenerationManager m_computeParticlesGenerationManager;
    DebugSimSpeedControllByVolRM m_debugSimSpeedByVol;
    CameraModeController m_cameraModeController;
    
    float m_particlesMaterialScalerMax = 20.0f;

    public Orbiter m_cameraOrbiter;
    float m_cameraOrbitRadiusMax = 32.0f;
    float m_cameraOrbitSpeedMax = 5.0f;
    float m_cameraOrbitRotationAngleMax = 360.0f;
    float m_bloomIntensityMax = 2.0f;
    float m_motionBlurMax = 1.0f;

    public Renderer m_planeRenderer;
    public ForceSource m_forceSource;

    float m_forceSourceMagnitudeMax = 10.0f;
    float m_forceSourceMaxHeight = 10.0f;
    
    public Orbiter m_particlesGeneratorOrbiter;
    public TransformRotator m_particlesGeneratorRotator;

    void Start()
    {
        m_backGroundColor = m_mainCamera.backgroundColor;
        m_computeParticlesSimulationManager = FindObjectOfType<ComputeParticleSimulationManager>();
        m_computeVectorFieldViewerManager = FindObjectOfType<ComputeVectorFieldViewerManager>();
        m_bloom = m_mainCamera.GetComponent<Bloom>();
        m_motionBlur = m_mainCamera.GetComponent<MotionBlur>();

        m_particleGeneratorArrayManager = FindObjectOfType<ParticleGeneratorArrayManager>();
        m_frequencyDataManager = FindObjectOfType<FrequencyDataManager>();
        m_computeParticlesGenerationManager = FindObjectOfType<ComputeParticlesGenerationManager>();
        m_computeVectorFieldManager = FindObjectOfType<ComputeVectorFieldManager>();

        m_debugSimSpeedByVol = FindObjectOfType<DebugSimSpeedControllByVolRM>();
        m_cameraModeController = FindObjectOfType<CameraModeController>();

        /*
        Matrix4x4 mat = Camera.main.projectionMatrix;
        mat *= Matrix4x4.Scale(new Vector3(-1, 1, 1));
        Camera.main.projectionMatrix = mat;
        */
    }

    void Update()
    {
        // debug reset
        if (Input.GetKey(KeyCode.Escape) == true)
            Application.LoadLevel(Application.loadedLevel);


    }

    public void SetParticleSimulationSpeed(float knobPos)
    {
        float speed = Mathf.Lerp(-4.0f, 4.0f, knobPos); //knobPos - 0.5f; // middle is 0
        m_computeParticlesSimulationManager.m_particleSimSpeed = speed;
    }
    public void SetParticlesMaterialScale(float knobValue)
    {
        float scale = Mathf.Lerp(0, m_particlesMaterialScalerMax, knobValue);
        m_computeParticlesSimulationManager.m_materialSizeScaler = scale;
    }
    public void ToggleFreezeParticleSimulationSpeed(float knobValue)
    {
        bool freezeState = false;
        if (knobValue > 0)
            freezeState = true;
        m_computeParticlesSimulationManager.m_isSimFrozen = freezeState;
    }
    public void SpawnAtOnce()
    {
        m_computeParticlesSimulationManager.m_isSpawned = false;
    }
    public void ToggleAudioVolParticleSpeed()
    {
        m_debugSimSpeedByVol.m_isActive = !m_debugSimSpeedByVol.m_isActive;
    }
    
    public void ToggleVectorFieldViewer()
    {
       m_computeVectorFieldViewerManager.m_isRendering = !m_computeVectorFieldViewerManager.m_isRendering;
    }

    

    public void RefreshBackgroundColor_R(float r)
    {
        m_backGroundColor.r = r;
        m_mainCamera.backgroundColor = m_backGroundColor;
    }
    public void RefreshBackgroundColor_G(float g)
    {
        m_backGroundColor.g = g;
        m_mainCamera.backgroundColor = m_backGroundColor;
    }
    public void RefreshBackgroundColor_B(float b)
    {
        m_backGroundColor.b = b;
        m_mainCamera.backgroundColor = m_backGroundColor;
    }

    public void SetCameraOrbitRadius(float knobValue)
    {
        float oRadius = Mathf.Lerp(0, m_cameraOrbitRadiusMax, knobValue);
        m_cameraOrbiter.m_orbitRadius = oRadius;
    }

    public void SetCameraOrbitSpeed(float knobValue)
    {
        float oSpeed = Mathf.Lerp(0, m_cameraOrbitSpeedMax, knobValue);
        m_cameraOrbiter.m_orbitSpeed = oSpeed;
    }

    public void ToggleCameraOrbitDirection()
    {
        m_cameraOrbiter.m_orbitDirection *= -1;
    }

    public void SetCameraRotationAngle_x(float knobValue)
    {
        float angle = Mathf.Lerp(0, m_cameraOrbitRotationAngleMax, knobValue);
        m_cameraOrbiter.m_orbitRotation.x = angle;
    }
    public void SetCameraRotationAngle_y(float knobValue)
    {
        float angle = Mathf.Lerp(0, m_cameraOrbitRotationAngleMax, knobValue);
        m_cameraOrbiter.m_orbitRotation.y = angle;
    }
    public void SetCameraRotationAngle_z(float knobValue)
    {
        float angle = Mathf.Lerp(0, m_cameraOrbitRotationAngleMax, knobValue);
        m_cameraOrbiter.m_orbitRotation.z = angle;
    }
    public void ToggleCameraTarget()
    {
        m_cameraModeController.ToggleCameraTarget();
    }


    public void SetParticlesGeneratorOrbitRadius(float knobValue)
    {
        m_particlesGeneratorOrbiter.m_orbitRadius = Mathf.Lerp(0,16, knobValue);
    }
    public void SetParticlesGeneratorOrbitSpeed(float knobValue)
    {
        m_particlesGeneratorOrbiter.m_orbitSpeed = Mathf.Lerp(0,2, knobValue);
    }
    public void SetParticlesGeneratorOrbitOffsetAmplitude(float knobValue)
    {
        m_particlesGeneratorOrbiter.m_sineOffsetAmplitude = new Vector3(0, Mathf.Lerp(0,16, knobValue), 0);
    }
    public void SetParticlesGeneratorRotationSpeed(float knobValue)
    {
        m_particlesGeneratorRotator.m_rotationSpeed = Vector3.one * Mathf.Lerp(0,180.0f, knobValue);
    }

    public void SetGeneratorTypeTo_Circle()
    {
        if (m_particleGeneratorArrayManager == null)
            return;

        m_particleGeneratorArrayManager.m_generatorsLayoutType = ParticleGeneratorArrayManager.GeneratorsLayoutTypes.Circle;
    }
    public void SetGeneratorTypeTo_Spriral()
    {
        if (m_particleGeneratorArrayManager == null)
            return;

        m_particleGeneratorArrayManager.m_generatorsLayoutType = ParticleGeneratorArrayManager.GeneratorsLayoutTypes.Spiral;
    }
    public void SetGeneratorTypeTo_Flower()
    {
        if (m_particleGeneratorArrayManager == null)
            return;

        m_particleGeneratorArrayManager.m_generatorsLayoutType = ParticleGeneratorArrayManager.GeneratorsLayoutTypes.Flower;
    }
    public void SetGeneratorTypeTo_Flat()
    {
        if (m_particleGeneratorArrayManager == null)
            return;

        m_particleGeneratorArrayManager.m_generatorsLayoutType = ParticleGeneratorArrayManager.GeneratorsLayoutTypes.Flat;
    }
    public void SetGeneratorLookAtType_UpAligned()
    {
        if (m_particleGeneratorArrayManager == null)
            return;

        m_particleGeneratorArrayManager.m_generatorLookAtType = ParticleGeneratorArrayManager.GeneratorsLookAtTypes.UpAligned;
    }
    public void SetGeneratorLookAtType_TangentAligned()
    {
        if (m_particleGeneratorArrayManager == null)
            return;

        m_particleGeneratorArrayManager.m_generatorLookAtType = ParticleGeneratorArrayManager.GeneratorsLookAtTypes.TangentAligned;
    }
    public void SetGeneratorLookAtType_Center()
    {
        if (m_particleGeneratorArrayManager == null)
            return;

        m_particleGeneratorArrayManager.m_generatorLookAtType = ParticleGeneratorArrayManager.GeneratorsLookAtTypes.Center;
    }
    
    

    public void SetGeneratorRadius(float knobValue)
    {
        float radius = Mathf.Lerp(0, 9.0f, knobValue);
        m_particleGeneratorArrayManager.m_generatorsRadius = radius;
    }
    public void SetGeneratorPetalFrequency(float knobValue)
    {
        float freq = Mathf.Lerp(0, 7, knobValue);
        m_particleGeneratorArrayManager.m_flowerPetalsFrequency = freq;
    }
    
    public void FlipForceSourceDirection(float knobValue)
    {
        if (knobValue > 0)
            m_forceSource.m_forcePoloraity *= -1.0f;
    }
    public void SetForceSourceMagnitude(float knobValue)
    {
        float force = Mathf.Lerp( 0, m_forceSourceMagnitudeMax, knobValue);
        m_forceSource.m_forceMagnitude = force;
    }
    public void SetForceSourceHeight(float knobValue)
    {
        float height = Mathf.Lerp(-m_forceSourceMaxHeight, m_forceSourceMaxHeight, knobValue);
        Vector3 pos = m_forceSource.transform.position;
        pos.y = height;
        m_forceSource.transform.position = pos;
    }

    public void SaturationBoost(float knobValue)
    {
        float extraSaturation = Mathf.Lerp(-2.0f, 2.0f, knobValue);
        m_frequencyDataManager.m_colorSaturationBoost = extraSaturation;

    }


    public void ClearVectorField()
    {
        m_computeVectorFieldManager.SetVectorFieldVec_Uniform(Vector3.zero);
    }
    public void FlipVectorField()
    {
        m_computeVectorFieldManager.FlipVectorField();
    }
    public void HalfVectorField()
    {
        m_computeVectorFieldManager.AmplifyVectorField(0.5f);
    }
    public void DoubleVectorField()
    {
        m_computeVectorFieldManager.AmplifyVectorField(2.0f);
    }
    public void ToggleVectorFieldUpdate()
    {
        m_computeVectorFieldManager.ToggleVectorFieldUpdate();
    }
    public void VectorField_SetSin_Dispatch()
    {
        m_computeVectorFieldManager.m_isDispatch_SetSin = true;
    }
    public void VectorField_SetSin_Scaler(float knobVaue)
    {
        float scalerValue = Mathf.Lerp(-4,4, knobVaue);
        m_computeVectorFieldManager.m_sinScaler = scalerValue;
    }
    public void VectorField_SetSin_SetScalerSign()
    {
        m_computeVectorFieldManager.m_sinScaler = -m_computeVectorFieldManager.m_sinScaler;        
    }
    public void VectorField_SetSin_Frequency(float knobValue)
    {
        float frequencyValue = Mathf.Lerp(-2.0f, 2.0f, knobValue);
        m_computeVectorFieldManager.m_sinFrequency = frequencyValue;
    }
    public void VectorField_SetSphere_Dispatch()
    {
        m_computeVectorFieldManager.m_isDispatch_SetSphere = true;
    }
    public void VectorField_SetSphere_SetScalerValue(float knobValue)
    {
        m_computeVectorFieldManager.m_spherePowerScaler = Mathf.Lerp(-1,1, knobValue);
    }



    public void CycleParticlesMaterial()
    {
        m_computeParticlesSimulationManager.CyclePartcilesMaterial();
    }

    

}
