﻿using UnityEngine;
using System.Collections;

public class ComputeParticleSimulationManager : MonoBehaviour
{
    public ComputeShader m_particleSimCS;
    public Material[] m_particlesMaterialArray;

    const int k_threadGroupSize_x = 16;
    const int k_threadGroupSize_y = 16;
    const int k_threadGroupCount_x = 128;
    const int k_threadGroupCount_y = 128;
    const int k_partcilesPerThread = 1;

    ComputeBuffer m_positionsCB;
    ComputeBuffer m_velocitiesCB;
    ComputeBuffer m_colorsCB;
    ComputeBuffer m_forwardVectorsCB;
    ComputeBuffer m_upVectorsCB;

    // need 2 becuase can't append/consume from same one, will to to swap over every use, like pouring water back and forth between glasses
    // should never be referenced directly, always through Get functions to ensure correct one is being used
    ComputeBuffer m_particlesAliveListBuffer_0;
    ComputeBuffer m_particlesAliveListBuffer_1;
    // also should only be changed via function to ensure consistency if other scripts end up needing it
    bool m_isAliveListBufferSwapped = false; // if false, _0 is append, _1 is consume
        
    ComputeBuffer m_particlesDeadListBuffer;
    ComputeBuffer m_particlesAliveCounterBuffer;
    ComputeBuffer m_particlesDeadCounterBuffer;

    
    public float m_particlesVelClamp = 5.0f;

    [Range(-4,4)]
    public float m_particleSimSpeed = 0.25f;
    public float m_particleSimDirection = 1.0f;

    [Range(0, 10)]
    public float d_debugFloat;

    public float m_materialSizeScaler = 1.0f;  

    ComputeVectorFieldManager m_computeVectorFieldManager;
    ComputeParticlesGenerationManager m_computeParticlesGenerationManager;
    ComputeParticlesLerpToTargetManager m_computeParticlesLerpToTargetManager;
    

    public bool m_isRendering = true;
    public bool m_isSimFrozen = false;

    public int m_currentParticlesMaterialIndex = 0;

    public bool m_isSpawned = true;

    public bool m_isParticlesLerpToTarget = false;

    Vector3[] m_randomSphereShellPositionsArray;
    Vector3[] m_randomSphereShellUpVecArray;
    Vector3[] m_randomSphereShellForwardVecArray;

    int spawnAtOnceCounter = 0;
    public float m_externalSimSpeedScaler = 1.0f;
    public bool m_isSimSpeedExtern;
    float[] radiArray = {4, 8, 12, 16};

    public float m_fieldInfluenceOnParticle = 1.0f;
    public float m_particleInfluenceOnField = 1.0f;

    void Start()
    {
        // initial sim setup

        m_computeVectorFieldManager = FindObjectOfType<ComputeVectorFieldManager>();
        m_computeParticlesGenerationManager = FindObjectOfType<ComputeParticlesGenerationManager>();
        m_computeParticlesLerpToTargetManager = FindObjectOfType<ComputeParticlesLerpToTargetManager>();
        

        Debug.Log("Particle count: " + GetParticleCount());
        
        m_positionsCB = new ComputeBuffer(GetParticleCount(), sizeof(float) * 4);
        m_velocitiesCB = new ComputeBuffer(GetParticleCount(), sizeof(float) * 4);
        m_colorsCB = new ComputeBuffer(GetParticleCount(), sizeof(float) * 4);
        m_forwardVectorsCB = new ComputeBuffer(GetParticleCount(), sizeof(float) * 4);
        m_upVectorsCB = new ComputeBuffer(GetParticleCount(), sizeof(float) * 4);

        m_particlesAliveListBuffer_0 = new ComputeBuffer(GetParticleCount(), sizeof(uint), ComputeBufferType.Append);
        m_particlesAliveListBuffer_1 = new ComputeBuffer(GetParticleCount(), sizeof(uint), ComputeBufferType.Append);
        m_particlesDeadListBuffer = new ComputeBuffer(GetParticleCount(), sizeof(uint), ComputeBufferType.Append);

        m_particlesAliveCounterBuffer = new ComputeBuffer(1, sizeof(uint), ComputeBufferType.Raw);
        m_particlesDeadCounterBuffer = new ComputeBuffer(1, sizeof(uint), ComputeBufferType.Raw);


        


        m_particlesAliveListBuffer_0.SetCounterValue(0);
        m_particlesAliveListBuffer_1.SetCounterValue(0);
        uint[] indicesArray = new uint[GetParticleCount()];
        for (int i = 0; i < indicesArray.Length; i++)
            indicesArray[i] = (uint)i;
        m_particlesDeadListBuffer.SetData(indicesArray);
        m_particlesDeadListBuffer.SetCounterValue((uint)GetParticleCount());
        //m_particlesDeadListBuffer.SetCounterValue(0);

        
        // TODO: fix init order issues so most buffers can be set here once
        //SetCSParamsAndBuffers();

        //m_particleSimCS.Dispatch(0, k_threadGroupCount_x, k_threadGroupCount_y, 1);

        // set up all the materials here for easy switch
        for (int i = 0; i < m_particlesMaterialArray.Length; i++)
        {
            m_particlesMaterialArray[i].SetBuffer("_Positions", m_positionsCB);
            m_particlesMaterialArray[i].SetBuffer("_Colors", m_colorsCB);
            m_particlesMaterialArray[i].SetBuffer("_Velocities", m_velocitiesCB);
            m_particlesMaterialArray[i].SetBuffer("_ForwardVectors", m_forwardVectorsCB);
            m_particlesMaterialArray[i].SetBuffer("_UpVectors", m_upVectorsCB);
        }

        m_randomSphereShellPositionsArray = new Vector3[GetParticleCount()];
        m_randomSphereShellForwardVecArray = new Vector3[GetParticleCount()];
        m_randomSphereShellUpVecArray = new Vector3[GetParticleCount()];
        for(int i = 0; i < GetParticleCount(); i++)
        {
            m_randomSphereShellPositionsArray[i] = Random.onUnitSphere;
            m_randomSphereShellForwardVecArray[i] = Vector3.Normalize(m_randomSphereShellPositionsArray[i]);
            m_randomSphereShellUpVecArray[i] = Quaternion.AngleAxis(90, Vector3.right) * m_randomSphereShellForwardVecArray[i];
        }        
    }

    void SetCSParamsAndBuffers()
    {
        //Debug.Log(m_particleSimCS.FindKernel("CSParticleSim"));

        m_particleSimCS.SetBuffer(0, "_positions", m_positionsCB);
        m_particleSimCS.SetBuffer(0, "_velocities", m_velocitiesCB);
        m_particleSimCS.SetBuffer(0, "_colors", m_colorsCB);
        m_particleSimCS.SetBuffer(0, "_forwardVectors", m_forwardVectorsCB);
        m_particleSimCS.SetBuffer(0, "_upVectors", m_upVectorsCB);


        m_particleSimCS.SetInt("k_threadGroupCount_x", k_threadGroupCount_x);
        m_particleSimCS.SetInt("k_threadGroupCount_y", k_threadGroupCount_y);
        m_particleSimCS.SetInt("_partcilesPerThread", k_partcilesPerThread);

        m_particleSimCS.SetTexture(0, "_vectorFieldTex_Write", m_computeVectorFieldManager.VectorFieldTexture_Read());
        m_particleSimCS.SetTexture(0, "_vectorFieldTex_Read", m_computeVectorFieldManager.VectorFieldTexture_Write());
        m_particleSimCS.SetFloat("_deltaTime", Time.deltaTime * m_particleSimSpeed * Mathf.Sign(m_particleSimDirection));
        m_particleSimCS.SetFloats("_vectorFieldManagerPos", m_computeVectorFieldManager.GetVectorFieldManagerPositionAsArray());
        m_particleSimCS.SetFloat("_vectorFieldSizeCount", m_computeVectorFieldManager.k_fieldWidth);
        m_particleSimCS.SetFloat("_vectorFieldScale", m_computeVectorFieldManager.k_fieldScale);
        m_particleSimCS.SetFloat("_particleVelClamp", m_particlesVelClamp);
        m_particleSimCS.SetFloat("_debugFloat", d_debugFloat);



        m_particleSimCS.SetBuffer(1, "_positions", m_positionsCB);
        m_particleSimCS.SetBuffer(1, "_velocities", m_velocitiesCB);
        m_particleSimCS.SetBuffer(1, "_colors", m_colorsCB);
        m_particleSimCS.SetBuffer(1, "_forwardVectors", m_forwardVectorsCB);
        m_particleSimCS.SetBuffer(1, "_upVectors", m_upVectorsCB);        
        m_particleSimCS.SetTexture(1, "_vectorFieldTex_Write", m_computeVectorFieldManager.VectorFieldTexture_Read());
        m_particleSimCS.SetTexture(1, "_vectorFieldTex_Read", m_computeVectorFieldManager.VectorFieldTexture_Write());
        //m_particleSimCS.SetBuffer(1, "_particlesAliveListBuffer", m_particlesAliveListBuffer);
        //m_particleSimCS.SetBuffer(1, "_particlesDeadListBuffer", m_particlesDeadListBuffer);
        //m_particleSimCS.SetBuffer(1, "_particlesAliveListBufferRead", m_particlesAliveListBuffer);
        
        m_particleSimCS.SetFloat("_fieldInfluenceOnParticle_Scaler", m_fieldInfluenceOnParticle);
        m_particleSimCS.SetFloat("_particleInfluenceOnField_Scaler", m_particleInfluenceOnField);

    }

    void SpawnAllAtOnceFraction(int fraction)
    {
        // test initial position layouts
        Vector3 spawnPos = 2.0f * Random.onUnitSphere;
        Vector3 spawnVel = new Vector3(0, 0, 0);
        Vector3 spawnForwardVec = Vector3.forward;
        Vector3 spawnUpVec = Vector3.up;
        Color spawnCol = Color.white * 0.5f;
        float spawnLife = 60.0f;
        float spawnScale = 1.0f;

        if(fraction == 0)
            fraction = 1;

        float radius = radiArray[spawnAtOnceCounter];
        spawnAtOnceCounter += 1;
        spawnAtOnceCounter = spawnAtOnceCounter%4; 

        UnityEngine.Profiling.Profiler.BeginSample("Spawn All At Once for loop");
        for (int i = 0; i < GetParticleCount()/fraction; i++)
        {
            //spawnPos = 9.0f * m_randomSphereShellPositionsArray[i]; //Random.onUnitSphere;
            //spawnUpVec = m_randomSphereShellUpVecArray[i]; //Vector3.Normalize(spawnPos);
            //spawnForwardVec = m_randomSphereShellForwardVecArray[i]; //Quaternion.AngleAxis(90, Vector3.right) * spawnUpVec;
            //spawnVel = new Vector3(0, 0.00f, 0);
            //m_computeParticlesGenerationManager.AddNewPartcileToSpawnList(spawnPos, spawnVel, spawnCol, spawnForwardVec, spawnUpVec, spawnLife, spawnScale);

            m_computeParticlesGenerationManager.AddNewPartcileToSpawnList( radius * m_randomSphereShellPositionsArray[i],
                                                                           spawnVel,
                                                                           spawnCol,
                                                                           m_randomSphereShellForwardVecArray[i],
                                                                           m_randomSphereShellUpVecArray[i],
                                                                           spawnLife,
                                                                           spawnScale);
        }
        UnityEngine.Profiling.Profiler.EndSample();

        UnityEngine.Profiling.Profiler.BeginSample("SpawnParticlesBasedFromListAndIndjection");
        m_computeParticlesGenerationManager.SpawnParticlesBasedFromListAndInjection();
        UnityEngine.Profiling.Profiler.EndSample();

        m_isSpawned = true;
    }

    public void CyclePartcilesMaterial()
    {
        m_currentParticlesMaterialIndex = (m_currentParticlesMaterialIndex + 1) % m_particlesMaterialArray.Length;
    }

    public int GetParticleCount()
    {
        return k_threadGroupSize_x * k_threadGroupSize_y * k_threadGroupCount_x * k_threadGroupCount_y * k_partcilesPerThread; ;
    }
    public ComputeBuffer GetPositionsCB()
    {
        return m_positionsCB;
    }
    public ComputeBuffer GetVelocitiesCB()
    {
        return m_velocitiesCB;
    }
    public ComputeBuffer GetColorsCB()
    {
        return m_colorsCB;
    }
    public ComputeBuffer GetForwardVectorsCB()
    {
        return m_forwardVectorsCB;
    }
    public ComputeBuffer GetUpVectorsCB()
    {
        return m_upVectorsCB;
    }

    public ComputeBuffer GetParticlesAliveListBuffer_Append()
    {
        if (m_isAliveListBufferSwapped == false)
            return m_particlesAliveListBuffer_0;
        else
            return m_particlesAliveListBuffer_1;
    }

    public ComputeBuffer GetParticlesAliveListBuffer_Consume()
    {
        if (m_isAliveListBufferSwapped == true)
            return m_particlesAliveListBuffer_0;
        else
            return m_particlesAliveListBuffer_1;
    }

    public void Flip_PariclesAliveAppendConsume()
    {
        m_isAliveListBufferSwapped = !m_isAliveListBufferSwapped;
    }

    // used to try to get updated/latest/accurate list count
    public ComputeBuffer GetFreshParticlesAliveCounterBuffer()
    {
        ComputeBuffer.CopyCount(GetParticlesAliveListBuffer_Consume(), m_particlesAliveCounterBuffer, 0);
        return m_particlesAliveCounterBuffer;
    }
    public ComputeBuffer GetFreshParticlesDeadCounterBuffer()
    {
        ComputeBuffer.CopyCount(GetParticlesDeadListBuffer(), m_particlesDeadCounterBuffer, 0);
        return m_particlesDeadCounterBuffer;
    }

    public ComputeBuffer GetParticlesDeadListBuffer()
    {
        return m_particlesDeadListBuffer;
    }
    
    public int GetThreadGroupCount_X()
    {
        return k_threadGroupCount_x;
    }
    public int GetThreadGroupCount_Y()
    {
        return k_threadGroupCount_y;
    }
    public int GetParticlesPerThread()
    {
        return k_partcilesPerThread;
    }

    void Update()
    {
        if (m_isSimFrozen == true)
            return;

        //if (m_isSpawned == false)
        //    SpawnAllAtOnceFraction(5);

        if (m_isSimSpeedExtern == true)
            m_particleSimSpeed = m_externalSimSpeedScaler;

        if (m_isParticlesLerpToTarget == false)
        {


            UnityEngine.Profiling.Profiler.BeginSample("pre dispatch prep");
            SetCSParamsAndBuffers();
            UnityEngine.Profiling.Profiler.EndSample();

            UnityEngine.Profiling.Profiler.BeginSample("dispatch buffers");
            // old bruteforce
            //m_particleSimCS.Dispatch(0, k_threadGroupCount_x, k_threadGroupCount_y, 1);

            // new append/consume            
            // TODO dispatch dynamic load based on alive count
            m_particleSimCS.SetBuffer(1, "_particlesDeadListBuffer", m_particlesDeadListBuffer);
            m_particleSimCS.SetBuffer(1, "_particlesAliveListBuffer_Consume", GetParticlesAliveListBuffer_Consume());
            m_particleSimCS.SetBuffer(1, "_particlesAliveListBuffer_Append", GetParticlesAliveListBuffer_Append());

            // copy append/consume counters to allow for bounds checkin compute shader            
            m_particleSimCS.SetBuffer(1, "_particlesDeadListCounter", GetFreshParticlesDeadCounterBuffer());
            m_particleSimCS.SetBuffer(1, "_particlesAliveListCounter", GetFreshParticlesAliveCounterBuffer());

            UnityEngine.Profiling.Profiler.EndSample();

            UnityEngine.Profiling.Profiler.BeginSample("pure dispatch");
            m_particleSimCS.Dispatch(1, k_threadGroupCount_x, k_threadGroupCount_y, 1);
            UnityEngine.Profiling.Profiler.EndSample();

            // swap buffers
            // flip append consume since poured once over
            Flip_PariclesAliveAppendConsume();


            // need to call these to allow use from pixel shader
            //Graphics.SetRandomWriteTarget(5, GetParticlesAliveListBuffer_Consume(), true);
            //Graphics.SetRandomWriteTarget(6, m_particlesDeadListBuffer, true);
            //Graphics.SetRandomWriteTarget(7, m_positionsCB, true);
            //Graphics.SetRandomWriteTarget(8, m_velocitiesCB, true);
            //Graphics.SetRandomWriteTarget(9, m_colorsCB, true);
            //Graphics.SetRandomWriteTarget(10, m_forwardVectorsCB, true);
            //Graphics.SetRandomWriteTarget(11, m_upVectorsCB, true);

        }
        else
            m_computeParticlesLerpToTargetManager.DispatchParticlesLerpToTarget();

        //m_computeParticlesTransform.ApplyTransformsToParticles();
        
    }

    // based off of GPU_GEMS_NBodySim Unity example 
    // http://scrawkblog.com/category/particles/

    void OnRenderObject()
    {
        if (Camera.current.CompareTag("ParticlesRenderCamera") == false && Camera.current.name != "SceneCamera")
            return;

        if (m_isRendering == true)
        {
            m_particlesMaterialArray[m_currentParticlesMaterialIndex].SetFloat("_SizeScaler", m_materialSizeScaler);
            m_particlesMaterialArray[m_currentParticlesMaterialIndex].SetPass(0);

            Graphics.DrawProcedural(MeshTopology.Points, GetParticleCount());
        }
    }

    void OnDestroy()
    {
        if (m_positionsCB != null)
        {
            m_positionsCB.Release();
            m_velocitiesCB.Release();
            m_colorsCB.Release();
            m_forwardVectorsCB.Release();
            m_upVectorsCB.Release();
            m_particlesAliveListBuffer_0.Release();
            m_particlesAliveListBuffer_1.Release();
            m_particlesDeadListBuffer.Release();
            m_particlesAliveCounterBuffer.Release();
            m_particlesDeadCounterBuffer.Release();

            m_positionsCB.Dispose();
            m_velocitiesCB.Dispose();
            m_colorsCB.Dispose();
            m_forwardVectorsCB.Dispose();
            m_upVectorsCB.Dispose();
            m_particlesAliveListBuffer_0.Dispose();
            m_particlesAliveListBuffer_1.Dispose();
            m_particlesDeadListBuffer.Dispose();
            m_particlesAliveCounterBuffer.Dispose();
            m_particlesDeadCounterBuffer.Dispose();
        }
    }
    

}
