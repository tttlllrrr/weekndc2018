﻿using UnityEngine;
using System.Collections;

public class VectorFieldViewer : MonoBehaviour 
{
	public GameObject p_vectorViewPrefab;
	GameObject[][][] m_vectorFieldArrowsObjects3DArray;
	VectorFieldManager m_vectorFieldManager;

	float m_vectorViewObjectScale_Min = 0.1f;
	float m_vectorViewObjectScale_Max = 1.0f;

	void Start()
	{
		m_vectorFieldManager = FindObjectOfType<VectorFieldManager>();

		m_vectorFieldArrowsObjects3DArray = new GameObject[m_vectorFieldManager.c_fieldSizeCount][][];
		for(int i = 0; i < m_vectorFieldManager.c_fieldSizeCount; i ++)
		{
			m_vectorFieldArrowsObjects3DArray[i] = new GameObject[m_vectorFieldManager.c_fieldSizeCount][];
			for(int j = 0; j < m_vectorFieldManager.c_fieldSizeCount; j++)
			{
				m_vectorFieldArrowsObjects3DArray[i][j] = new GameObject[m_vectorFieldManager.c_fieldSizeCount];
				for(int k = 0; k < m_vectorFieldManager.c_fieldSizeCount; k++)
				{
					Vector3 vectorWorldPosition = m_vectorFieldManager.GetFieldVectorWorldPosition(i,j,k);
					//Quaternion vectorWorldRotation = Quaternion.LookRotation(m_vectorFieldManager.m_vectorField3DArray[i][j][k].normalized); 
					float vectorScale = Mathf.Lerp(m_vectorViewObjectScale_Min, m_vectorViewObjectScale_Max, m_vectorFieldManager.m_vectorField3DArray[i][j][k].magnitude);

                    m_vectorFieldArrowsObjects3DArray[i][j][k] = (GameObject)Instantiate(p_vectorViewPrefab, vectorWorldPosition, Quaternion.identity);//vectorWorldRotation);
					m_vectorFieldArrowsObjects3DArray[i][j][k].transform.parent = transform;
					m_vectorFieldArrowsObjects3DArray[i][j][k].transform.localScale = new Vector3(vectorScale, vectorScale, vectorScale);
					m_vectorFieldArrowsObjects3DArray[i][j][k].name = "Vector Arrow : (" + i + ", " + j + ", " + k + ")";

				}
			}
		}

		m_vectorFieldManager.e_VectorFieldUpdated += new VectorFieldManager.VectorFieldUpdated(UpdateViewObjects);
	}	



	void UpdateViewObjects()
	{		
		for(int i = 0; i < m_vectorFieldManager.c_fieldSizeCount; i ++)
		{
			for(int j = 0; j < m_vectorFieldManager.c_fieldSizeCount; j++)
			{
				for(int k = 0; k < m_vectorFieldManager.c_fieldSizeCount; k++)
				{
					Vector3 vectorWorldPosition = m_vectorFieldManager.GetFieldVectorWorldPosition(i,j,k); 
					Quaternion vectorWorldRotation = Quaternion.LookRotation(m_vectorFieldManager.m_vectorField3DArray[i][j][k].normalized);
					float vectorScale = Mathf.Lerp(m_vectorViewObjectScale_Min, m_vectorViewObjectScale_Max, m_vectorFieldManager.m_vectorField3DArray[i][j][k].magnitude);

					m_vectorFieldArrowsObjects3DArray[i][j][k].transform.rotation = vectorWorldRotation;
					m_vectorFieldArrowsObjects3DArray[i][j][k].transform.localScale = new Vector3(vectorScale, vectorScale, vectorScale);
					
				}
			}
		}
	}

    void Update()
    {
        // force fixed position so that children arrow objects don't get moved around because of viewer parent object
        transform.position = Vector3.zero;

    }

}
