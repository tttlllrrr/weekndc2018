﻿using UnityEngine;
using System.Collections;

public class ThetaSphereController : MonoBehaviour
{

    public GameObject m_thetaSphere_1;
    public GameObject m_thetaSphere_2;
    GameObject m_currentlyActiveSphere;

    Vector3 m_sphereActivePosition = Vector3.zero;
    Vector3 m_sphereDeactivePosition = new Vector3(200, 0, 0);

    bool m_isCurrentThetaSphere_1 = true;

    float m_sphereRotVel_Yaw = 0;
    float m_sphereRotVel_Pitch = 0;
    float m_sphereRotVel_YawMax = 100.0f;
    float m_sphereRotVel_PitchMax = 100.0f;
    float m_sphereToVelDecay = 1.17f;
    bool m_isFreshYaw = false;
    bool m_isFreshPitch = false;
    
    void Start()
    {
        RefreshThetaSphereActive();
    }

    void RefreshThetaSphereActive()
    {
        m_thetaSphere_1.SetActive(m_isCurrentThetaSphere_1);
        m_thetaSphere_2.SetActive(!m_isCurrentThetaSphere_1);

        if(m_isCurrentThetaSphere_1 == true)
        {
            m_thetaSphere_1.transform.position = m_sphereActivePosition;
            m_thetaSphere_2.transform.position = m_sphereDeactivePosition;
            m_currentlyActiveSphere = m_thetaSphere_1;
        }
        else
        {
            m_thetaSphere_1.transform.position = m_sphereDeactivePosition;
            m_thetaSphere_2.transform.position = m_sphereActivePosition;
            m_currentlyActiveSphere = m_thetaSphere_2;
        }
    }

    public void SwitchThetaSphere()
    {
        m_isCurrentThetaSphere_1 = !m_isCurrentThetaSphere_1;
        RefreshThetaSphereActive();
    }

    public void IncrementRotationVelocity(float yaw, float pitch)
    {
        m_sphereRotVel_Yaw = Mathf.Clamp(m_sphereRotVel_Yaw + yaw, -m_sphereRotVel_YawMax, m_sphereRotVel_YawMax);
        m_sphereRotVel_Pitch = Mathf.Clamp(m_sphereRotVel_Pitch + pitch, -m_sphereRotVel_PitchMax, m_sphereRotVel_PitchMax);

        if (yaw != 0)
            m_isFreshYaw = true;
        if (pitch != 0)
            m_isFreshPitch = true;

    }

    void Update()
    {
        if(m_isFreshYaw == false)
            m_sphereRotVel_Yaw -= m_sphereRotVel_Yaw * m_sphereToVelDecay * Time.deltaTime;
        if(m_isFreshPitch == false)
            m_sphereRotVel_Pitch -= m_sphereRotVel_Pitch * m_sphereToVelDecay * Time.deltaTime;

        m_currentlyActiveSphere.transform.Rotate(m_sphereRotVel_Pitch * Time.deltaTime, -m_sphereRotVel_Yaw * Time.deltaTime, 0);


        m_isFreshYaw = false;
        m_isFreshPitch = false;
    }

}
