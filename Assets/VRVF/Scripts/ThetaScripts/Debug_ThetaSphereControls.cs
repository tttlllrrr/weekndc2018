﻿using UnityEngine;
using System.Collections;

public class Debug_ThetaSphereControls : MonoBehaviour
{
    ThetaSphereController m_thetaSphereController;

    void Start()
    {
        m_thetaSphereController = FindObjectOfType<ThetaSphereController>();
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.T))
        {
            m_thetaSphereController.SwitchThetaSphere();
        }

        float yawInput = 0;
        if (Input.GetKey(KeyCode.LeftArrow))
            yawInput = -1;
        if (Input.GetKey(KeyCode.RightArrow))
            yawInput = 1;

        float pitchInput = 0;
        if (Input.GetKey(KeyCode.UpArrow))
            pitchInput = 1;
        if (Input.GetKey(KeyCode.DownArrow))
            pitchInput = -1;

        m_thetaSphereController.IncrementRotationVelocity(10.0f * yawInput * Time.deltaTime, 10.0f * pitchInput * Time.deltaTime);

    }




}
