﻿using UnityEngine;
using System.Collections;

public class ApplyThetaWebcamTexture : MonoBehaviour 
{
	ThetaTextureManager m_thetaTextureManager;

	bool m_isApplyComplete = false;

	public int m_deviceIndex = 0;

	void Start()
	{
		m_isApplyComplete = false;
	}

	void Update()
	{
		if(m_isApplyComplete == false)
		{
			m_thetaTextureManager = FindObjectOfType<ThetaTextureManager>();
			GetComponent<Renderer>().material.mainTexture = m_thetaTextureManager.GetThetaWebcamTexture(m_deviceIndex);
			m_isApplyComplete = true;
			Debug.Log("Applied texutere");
		}
		

	}

	


}
