﻿using UnityEngine;
using System.Collections;
using System;

public class ThetaTextureManager : MonoBehaviour 
{
	const int k_thetaCount = 2;

	public int m_webcamDeviceIndex = 0;
	WebCamTexture[] m_thetaWebcamTexturesArray;

	void Start()
	{
		m_thetaWebcamTexturesArray = new WebCamTexture[k_thetaCount];
		WebCamDevice[] webcamDevices = WebCamTexture.devices;
        int textureCounter = 0;
		for(int i = 0; i< webcamDevices.Length; i++)
		{
			string webcamDeviceName = webcamDevices[i].name;
			Debug.Log(webcamDeviceName);
			if(webcamDeviceName.Contains("RICOH THETA S") == false)
				continue;

			Debug.Log("attempting webcam device " + i + ": " + webcamDeviceName);
			try
			{				
				m_thetaWebcamTexturesArray[textureCounter] = new WebCamTexture(webcamDeviceName, 1280, 720);
				m_thetaWebcamTexturesArray[textureCounter].Play();
				Debug.Log("Webcam texture for " + webcamDeviceName + " is playing");
                textureCounter++;
			}
			catch(Exception e)
			{
				Debug.LogError(e);
			}
		}
	}

	public WebCamTexture GetThetaWebcamTexture(int index)
	{
		return m_thetaWebcamTexturesArray[index];
	}

}
