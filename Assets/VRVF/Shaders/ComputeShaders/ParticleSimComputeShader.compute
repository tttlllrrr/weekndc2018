﻿// Each #kernel tells which function to compile; you can have many kernels
#pragma kernel CSParticleSim
#pragma kernel CSParticleSim_AppendConsume


#define k_threadGroupSize_x 16
#define k_threadGroupSize_y 16

uint k_threadGroupCount_x;
uint k_threadGroupCount_y;

float _deltaTime;
RWStructuredBuffer<float4> _positions; // encode remaining life in w 
RWStructuredBuffer<float4> _velocities; // encode particle individual scale in w
RWStructuredBuffer<float4> _colors;
RWStructuredBuffer<float4> _forwardVectors;
RWStructuredBuffer<float4> _upVectors;

Texture3D<float4> _vectorFieldTex_Read;
RWTexture3D<float4> _vectorFieldTex_Write;

float3 _vectorFieldManagerPos;
float _vectorFieldSizeCount;
float _vectorFieldScale;
float _particleVelClamp;
int _partcilesPerThread;
float3 fieldPosRatio;

float _debugFloat;

float _fieldInfluenceOnParticle_Scaler;
float _particleInfluenceOnField_Scaler;

// functions ported over from original c# version in VectorFieldManager
float3 CalculateVectorWorldPosition(uint3 indices)
{
	float3 vecPos = (_vectorFieldScale * float3(indices.x, indices.y, indices.z)) + _vectorFieldManagerPos;
	return vecPos;
}

// assumes unit lengths
float3 BilinearInterpolation(float2 p, float3 p_00, float3 v_00,
									   float3 p_10, float3 v_10,
									   float3 p_01, float3 v_01,
									   float3 p_11, float3 v_11)
{
	float lerp_x = p.x - p_00.x;
	float3 a = lerp(v_00, v_10, lerp_x);
	float3 b = lerp(v_01, v_11, lerp_x);
	float lerp_y = p.y - p_00.y;
	float3 c = lerp(a, b, lerp_y);
	return c;
}

float4 FindClosestVectorAndDist(float3 particlePos, uint particleIndex)
{
	float3 forceVector = float3(0.0,0.0,0.0);
	//return float4(0, 0.0001, 0, 0);
	float distanceParticleToVector = 0.0;

	// do position offset
	float3 relativeParticlePos = particlePos - _vectorFieldManagerPos;
	
	// clamp the offset position to field limit to ensure we always get affect by a vector/can access in array
	float fieldRange = _vectorFieldSizeCount * _vectorFieldScale;
	float3 particlePosInField = float3( clamp(relativeParticlePos.x, -fieldRange, fieldRange),
										clamp(relativeParticlePos.y, -fieldRange, fieldRange),
										clamp(relativeParticlePos.z, -fieldRange, fieldRange));
	
	uint3 vectorIndices = round(particlePosInField/_vectorFieldScale);
	
	// sigh, cheating for now (it's practically 3am :/)  by clamping indices
	uint maxIndex = _vectorFieldSizeCount -1;
	vectorIndices = clamp(vectorIndices, 0, maxIndex);	
	fieldPosRatio = particlePosInField/fieldRange;
	
	float3 pIF = particlePosInField/_vectorFieldScale;
	float3 pif_CCC = float3( ceil(pIF.x), ceil(pIF.y), ceil(pIF.z) );
	float3 pif_CCF = float3( ceil(pIF.x), ceil(pIF.y), floor(pIF.z) );
	float3 pif_CFF = float3( ceil(pIF.x), floor(pIF.y), floor(pIF.z) );
	float3 pif_CFC = float3( ceil(pIF.x), floor(pIF.y), ceil(pIF.z) );
	float3 pif_FCC = float3( floor(pIF.x), ceil(pIF.y), ceil(pIF.z) );
	float3 pif_FCF = float3( floor(pIF.x), ceil(pIF.y), floor(pIF.z) );
	float3 pif_FFC = float3( floor(pIF.x), floor(pIF.y), ceil(pIF.z) );
	float3 pif_FFF = float3( floor(pIF.x), floor(pIF.y), floor(pIF.z) );

	float3 vec_CCC = _vectorFieldTex_Read[ uint3(pif_CCC) ].xyz;
	float3 vec_CCF = _vectorFieldTex_Read[ uint3(pif_CCF) ].xyz;
	float3 vec_CFF = _vectorFieldTex_Read[ uint3(pif_CFF) ].xyz;
	float3 vec_CFC = _vectorFieldTex_Read[ uint3(pif_CFC) ].xyz;
	float3 vec_FCC = _vectorFieldTex_Read[ uint3(pif_FCC) ].xyz;
	float3 vec_FCF = _vectorFieldTex_Read[ uint3(pif_FCF) ].xyz;
	float3 vec_FFC = _vectorFieldTex_Read[ uint3(pif_FFC) ].xyz;
	float3 vec_FFF = _vectorFieldTex_Read[ uint3(pif_FFF) ].xyz;

	// trilinear method
	// doing xy planes first
	
	float3 vec_floor_xy = BilinearInterpolation(pIF, pif_FFF, vec_FFF,
													 pif_CFF, vec_CFF,
													 pif_FCF, vec_FCF,
													 pif_CCF, vec_CCF);

	float3 vec_ceil_xy = BilinearInterpolation(pIF, pif_FFC, vec_FFC,
													pif_CFC, vec_CFC,
													pif_FCC, vec_FCC,
													pif_CCC, vec_CCC);
	
	float lerp_z = pIF.z - pif_FFF.z;
	float3 vec_trilinear = lerp(vec_floor_xy, vec_ceil_xy, lerp_z);
	float4 res = float4(vec_trilinear, 1); // ignoring the w component, irrelevant 
	//return res;
	
	/*
	 // weighted method
	float maxDist = distance(pif_CCC, pif_FFF);
	float ratio_CCC = 1.0 -(distance(pIF, pif_CCC))/maxDist;
	float ratio_CCF = 1.0 -(distance(pIF, pif_CCF))/maxDist;
	float ratio_CFF = 1.0 -(distance(pIF, pif_CFF))/maxDist;
	float ratio_CFC = 1.0 -(distance(pIF, pif_CFC))/maxDist;
	float ratio_FCC = 1.0 -(distance(pIF, pif_FCC))/maxDist;
	float ratio_FCF = 1.0 -(distance(pIF, pif_FCF))/maxDist;
	float ratio_FFC = 1.0 -(distance(pIF, pif_FFC))/maxDist;
	float ratio_FFF = 1.0 -(distance(pIF, pif_FFF))/maxDist;

	float3 forceWeighted = float3(0,0,0);	
	forceWeighted += pow(ratio_CCC, 2) * vec_CCC;
	forceWeighted += pow(ratio_CCF, 2) * vec_CCF;
	forceWeighted += pow(ratio_CFF, 2) * vec_CFF;
	forceWeighted += pow(ratio_CFC, 2) * vec_CFC;
	forceWeighted += pow(ratio_FCC, 2) * vec_FCC;
	forceWeighted += pow(ratio_FCF, 2) * vec_FCF;
	forceWeighted += pow(ratio_FFC, 2) * vec_FFC;
	forceWeighted += pow(ratio_FFF, 2) * vec_FFF;
	
	forceVector.xyz = forceWeighted;	
	//forceVector = _vectorFieldTex_Read[vectorIndices.xyz].xyz;	
	distanceParticleToVector = distance(particlePos, CalculateVectorWorldPosition(vectorIndices.xyz)); 	
	float4 res = float4(forceVector.xyz, distanceParticleToVector);
	*/
	
	float3 vel = _velocities[particleIndex].xyz;
	float speed = length(vel);
	//_vectorFieldTex_Write[uint3(pif_FCF)].xyzw = float4(vec_FCF, 0) - float4(vel, 0) * 10.0 * (_deltaTime); // normal version
	// blargh hackd up test, TODO: need a normalized params based implementation
	
	float lifeRemaining = max(0, _positions[particleIndex].w);
	float maxLife = _upVectors[particleIndex].w; //5;// BLARGH HARD CODING
	float lifeRatio = min((lifeRemaining / maxLife), 1);

	float4 feedbackInfluence =  float4(vel, 0) * (1.0+pow(1.0-lifeRatio, 2)) * (_deltaTime) * ( 0.125*pow(1.0 + _velocities[particleIndex].w, 3)); // using scale --> i.e. FFT of particle weight into account
	feedbackInfluence *= _particleInfluenceOnField_Scaler;

	//_vectorFieldTex_Write[uint3(pif_FCF)].xyzw = float4(vec_FCF, 0) - feedbackInfluence;
	/*
	float maxDist = distance(pif_CCC, pif_FFF);
	float ratio_CCC = 1.0 - (distance(pIF, pif_CCC)) / maxDist;
	float ratio_CCF = 1.0 - (distance(pIF, pif_CCF)) / maxDist;
	float ratio_CFF = 1.0 - (distance(pIF, pif_CFF)) / maxDist;
	float ratio_CFC = 1.0 - (distance(pIF, pif_CFC)) / maxDist;
	float ratio_FCC = 1.0 - (distance(pIF, pif_FCC)) / maxDist;
	float ratio_FCF = 1.0 - (distance(pIF, pif_FCF)) / maxDist;
	float ratio_FFC = 1.0 - (distance(pIF, pif_FFC)) / maxDist;
	float ratio_FFF = 1.0 - (distance(pIF, pif_FFF)) / maxDist;

	
	_vectorFieldTex_Write[uint3(pif_CCC)].xyzw = float4(vec_CCC, 0) - feedbackInfluence * ratio_CCC;
	_vectorFieldTex_Write[uint3(pif_CCF)].xyzw = float4(vec_CCF, 0) - feedbackInfluence * ratio_CCF;
	_vectorFieldTex_Write[uint3(pif_CFF)].xyzw = float4(vec_CFF, 0) - feedbackInfluence * ratio_CFF;
	_vectorFieldTex_Write[uint3(pif_CFC)].xyzw = float4(vec_CFC, 0) - feedbackInfluence * ratio_CFC;
	_vectorFieldTex_Write[uint3(pif_FCC)].xyzw = float4(vec_FCC, 0) - feedbackInfluence * ratio_FCC;
	_vectorFieldTex_Write[uint3(pif_FCF)].xyzw = float4(vec_FCF, 0) - feedbackInfluence * ratio_FCF;
	_vectorFieldTex_Write[uint3(pif_FFC)].xyzw = float4(vec_FFC, 0) - feedbackInfluence * ratio_FFC;
	_vectorFieldTex_Write[uint3(pif_FFF)].xyzw = float4(vec_FFF, 0) - feedbackInfluence * ratio_FFF;
	*/

	_vectorFieldTex_Write[uint3(pif_CCC)].xyzw = float4(vec_CCC, 0) - feedbackInfluence ;
	_vectorFieldTex_Write[uint3(pif_CCF)].xyzw = float4(vec_CCF, 0) - feedbackInfluence ;
	_vectorFieldTex_Write[uint3(pif_CFF)].xyzw = float4(vec_CFF, 0) - feedbackInfluence ;
	_vectorFieldTex_Write[uint3(pif_CFC)].xyzw = float4(vec_CFC, 0) - feedbackInfluence ;
	_vectorFieldTex_Write[uint3(pif_FCC)].xyzw = float4(vec_FCC, 0) - feedbackInfluence ;
	_vectorFieldTex_Write[uint3(pif_FCF)].xyzw = float4(vec_FCF, 0) - feedbackInfluence ;
	_vectorFieldTex_Write[uint3(pif_FFC)].xyzw = float4(vec_FFC, 0) - feedbackInfluence ;
	_vectorFieldTex_Write[uint3(pif_FFF)].xyzw = float4(vec_FFF, 0) - feedbackInfluence ;

	return res;
}

// returns value from 0 to 1
float InverseLerp(float min, float max, float val)
{	
	float clampedVal = clamp(val, min, max);	
	return clamp( (clampedVal-min)/(max-min), 0, 1);
}

float3 ClampMagnitude(float3 vec, float clampVal)
{
	float vecLength = length(vec);
	// oh glob please no TOOO: PURGE THIS BRANCH NON SENSE
	if(vecLength == 0)
		return vec;
	
	float scaleFactor = abs(min(clampVal,vecLength))/vecLength;
	return scaleFactor * vec;
}

// old brute force approach
[numthreads( k_threadGroupSize_x, k_threadGroupSize_y, 1 )]
void CSParticleSim (uint3 dispatchID : SV_DispatchThreadID)
{	
	uint threadIndex = dispatchID.x + dispatchID.y * k_threadGroupSize_y * k_threadGroupCount_y;   

	for(int i = 0; i < _partcilesPerThread; i++)
	{
		uint index = threadIndex*_partcilesPerThread + i;
		

		// decrement lifetime
		_positions[index].w -= abs(_deltaTime);
		// not a fan or the branching, but saving a lot of perf by skipping work on dead particles
		if (_positions[index].w < 0)
			return;
		

		float3 particlePos = _positions[index].xyz;
		
		//float4 closestForceAndDist = FindClosestVectorAndDist(particlePos);
		float4 closestForceAndDist = FindClosestVectorAndDist(_positions[index].xyz, index);
		// don't mess with the w component
		//closestForceAndDist.xyz = mul(transformMatrix, closestForceAndDist.xyz + transformPosition).xyz ;


		_velocities[index].xyz += closestForceAndDist.xyz * abs(_deltaTime) * sign(_deltaTime);

		// urgh, need to fix this		
		_velocities[index].xyz = ClampMagnitude( _velocities[index].xyz, _particleVelClamp);
		_velocities[index].xyz -= _debugFloat * _velocities[index].xyz * _deltaTime;

		// testing drag
		_velocities[index].xyz = _velocities[index].xyz ;
		_positions[index].xyz += _velocities[index].xyz * abs(_deltaTime)* sign(_deltaTime);

	

		float3 velCol = abs(normalize(_velocities[index]).xyz);
		float3 fieldCol = fieldPosRatio; //closestForceAndDist.w * float3(1,1,1);
		//_colors[index].xyz = normalize(velCol); //max(fieldCol, defaultCol);	
		//_colors[index].xyz = fieldCol;	
		//_colors[index].w = 1.0;		

		if(length(_velocities[index].xyz) != 0)
		{
			float3 oldForwardVec = _forwardVectors[index].xyz;
			_forwardVectors[index].xyz = normalize(_velocities[index].xyz).xyz;
			if(length(oldForwardVec - _forwardVectors[index].xyz) != 0)
			{
				float3 forwardCross = cross(oldForwardVec, _forwardVectors[index].xyz);
				_upVectors[index].xyz = normalize(cross(forwardCross, _forwardVectors[index].xyz));
			}

		}
	} 		
}


// Buffers specific to Append/Consume based spawning 
ConsumeStructuredBuffer<uint> _particlesAliveListBuffer_Consume;
AppendStructuredBuffer<uint> _particlesAliveListBuffer_Append;
AppendStructuredBuffer<uint> _particlesDeadListBuffer;

StructuredBuffer<uint> _particlesAliveListCounter;
StructuredBuffer<uint> _particlesDeadListCounter;

[numthreads(k_threadGroupSize_x, k_threadGroupSize_y, 1)]
void CSParticleSim_AppendConsume(uint3 dispatchID : SV_DispatchThreadID)
{
	uint threadIndex = dispatchID.x + dispatchID.y * k_threadGroupSize_y * k_threadGroupCount_y;			
	// check if passed alive list, abort if so
	if (threadIndex >= _particlesAliveListCounter[0])
		return;

	uint index = _particlesAliveListBuffer_Consume.Consume();
	// not a fan or the branching, but saving a lot of perf by skipping work on dead particles
	if (_positions[index].w < 0)
	{			
		_particlesDeadListBuffer.Append(index);		
		return;
	}

	
	float3 particlePos = _positions[index].xyz;	
	float4 closestForceAndDist = FindClosestVectorAndDist(_positions[index].xyz, index);
	_velocities[index].xyz += closestForceAndDist.xyz * abs(_deltaTime) * sign(_deltaTime) * _fieldInfluenceOnParticle_Scaler;

	// urgh, need to fix this		
	_velocities[index].xyz = ClampMagnitude(_velocities[index].xyz, _particleVelClamp);
	_velocities[index].xyz -= _debugFloat * _velocities[index].xyz * _deltaTime;

	// testing drag
	_velocities[index].xyz = _velocities[index].xyz;
	_positions[index].xyz += _velocities[index].xyz * abs(_deltaTime)* sign(_deltaTime);

	//float3 velCol = abs(normalize(_velocities[index]).xyz);
	//float3 fieldCol = fieldPosRatio; 
	

	/*
	if (length(_velocities[index].xyz) != 0)
	{
		float3 oldForwardVec = _forwardVectors[index].xyz;
		_forwardVectors[index].xyz = normalize(_velocities[index].xyz).xyz;
		if (length(oldForwardVec - _forwardVectors[index].xyz) != 0)
		{
			float3 forwardCross = cross(oldForwardVec, _forwardVectors[index].xyz);
			_upVectors[index].xyz = normalize(cross(forwardCross, _forwardVectors[index].xyz));
		}
	}
	*/

	// decrement lifetime
	_positions[index].w -= abs(_deltaTime);
	_particlesAliveListBuffer_Append.Append(index);
}