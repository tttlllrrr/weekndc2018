﻿// Upgrade NOTE: upgraded instancing buffer 'Props' to new syntax.

Shader "AudioReactiveShaders/AudioReactiveShader_CHN_Standard_Instanced" 
{
	Properties 
	{
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_HeightTex ("Height Tex", 2D) = "black" {}
        _NormalsTex ("Normals Tex", 2D) = "black" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		//Cull Front

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows vertex:vert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0
		//#pragma glsl


		#include "UnityCG.cginc"

		sampler2D _MainTex;
		sampler2D _HeightTex;
		sampler2D _NormalsTex;


		struct Input
		{
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
		// put more per-instance properties here

			UNITY_DEFINE_INSTANCED_PROP(float, _HeightScale)
#define _HeightScale_arr Props
			UNITY_DEFINE_INSTANCED_PROP(float, _TextureOffsetX)
#define _TextureOffsetX_arr Props
			UNITY_DEFINE_INSTANCED_PROP(float, _TextureOffsetY)
#define _TextureOffsetY_arr Props
			UNITY_DEFINE_INSTANCED_PROP(float, _TextureScaleX)
#define _TextureScaleX_arr Props
			UNITY_DEFINE_INSTANCED_PROP(float, _TextureScaleY)
#define _TextureScaleY_arr Props

		UNITY_INSTANCING_BUFFER_END(Props)


		float HeightAtUV(float2 targetUV)
		{
			float heightScale = UNITY_ACCESS_INSTANCED_PROP(_HeightScale_arr, _HeightScale);
			return heightScale *tex2Dlod(_HeightTex, float4(targetUV,0,0));
		}

		void vert(inout appdata_full v, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input,o);

			float texOffset_x = UNITY_ACCESS_INSTANCED_PROP(_TextureOffsetX_arr, _TextureOffsetX);
			float texOffset_y = UNITY_ACCESS_INSTANCED_PROP(_TextureOffsetY_arr, _TextureOffsetY);
			float texScale_x = UNITY_ACCESS_INSTANCED_PROP(_TextureScaleX_arr, _TextureScaleX);
			float texScale_y = UNITY_ACCESS_INSTANCED_PROP(_TextureScaleY_arr, _TextureScaleY);


			float2 uv = v.texcoord.xy * float2(texScale_x, texScale_y) + float2(texOffset_x, texOffset_y);

			float3 originalN = v.normal.xyz;
			float3 rawNormal = tex2Dlod(_NormalsTex, float4(uv, 0, 0));
			float3 heightNormal = 2.0*rawNormal - 1.0;
			// rotation logic from VR_VF
			float epsN = 0.000000000000000001; // using an epsilon to avoid 0 cross vector when orignal normal is striaght up or down
			float3 upDir = float3(epsN, 1, epsN);
			float3 pDir = originalN; 

		    // tried, got this from http://stackoverflow.com/questions/18558910/direction-vector-to-rotation-matrix			
			float3 xAxis = normalize(cross(upDir, pDir));
			float3 yAxis = normalize(cross(pDir, xAxis));

			// had to use float4x4 instead of matrix, 
			// since it doesn;t seem to recognized in vert shader,
			// original matrix version was in a geometry shader
			float4x4 rotMat = float4x4(xAxis.x, pDir.x, yAxis.x, 0,
									   xAxis.y, pDir.y, yAxis.y, 0,
									   xAxis.z, pDir.z, yAxis.z, 0,
									   0, 0, 0, 1);
			float heightSnapper = 1.0;
			float snapEps = 0.000000001f;
			// BARGDFHDFG NEED A BETTER WAY OF DOING THIS
			if (v.texcoord.x < snapEps || v.texcoord.x == 1 || v.texcoord.y < snapEps || v.texcoord.y == 1)
				heightSnapper = 0;

				
			v.vertex.xyz += v.normal.xyz * HeightAtUV(uv) * heightSnapper;
			// rotate original normal by height value normal
			v.normal.xyz = normalize((mul((rotMat), heightNormal.xyz).xyz));
		}


		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			float texOffset_x = UNITY_ACCESS_INSTANCED_PROP(_TextureOffsetX_arr, _TextureOffsetX);
			float texOffset_y = UNITY_ACCESS_INSTANCED_PROP(_TextureOffsetY_arr, _TextureOffsetY);
			float texScale_x = UNITY_ACCESS_INSTANCED_PROP(_TextureScaleX_arr, _TextureScaleX);
			float texScale_y = UNITY_ACCESS_INSTANCED_PROP(_TextureScaleY_arr, _TextureScaleY);

			float2 uv = IN.uv_MainTex * float2(texScale_x, texScale_y) + float2(texOffset_x, texOffset_y);
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D(_MainTex, uv);			
			//c.xyz = (o.Normal).xzy;
			//c.xyz = 0.50*(o.Normal).xzy +0.5;			
			//c = tex2Dlod(_NormalsTex, float4(uv, 0, 0));

			// height based modifiers, should help focus on important parts
			c.xyz *= pow((10.0 * (float)(tex2Dlod(_HeightTex, float4(uv,0,0)))), 2.00);
			// meh
			//c.xyz += c.xyz * pow(( 1.0 + (float)( tex2Dlod(_HeightTex, float4(uv,0,0) )) ), 10.00);

			//c.xyz = 0.7;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
