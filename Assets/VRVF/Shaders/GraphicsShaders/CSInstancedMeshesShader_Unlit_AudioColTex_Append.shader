﻿Shader "ComputeShaderInstancedMeshes/CSInstancedMeshesShader_Unlit_AudioColTex_Append"
{
	/// Reference --> https://docs.unity3d.com/ScriptReference/Graphics.DrawMeshInstancedIndirect.html

	Properties
	{
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_Sprite("Sprite", 2D) = "white" {}
		_Color("Color", Color) = (0.8,0.6,0.8,1.0)
		_Size("Size", float) = 0.1		
		_SizeScaler("SizeScaler", float) = 1.0
		_TextureScaleX("TextureScale X", Range(0,1)) = 1.0
		_TextureScaleY("TextureScale Y", Range(0,1)) = 1.0
		_MaxLife("Max Life", Range(0,120)) = 60.0
		_SimSpeed("Sim Speed", float) = 1
	}
	SubShader
	{
		//Tags { "Queue" = "Transparent"}
		//Tags{ "LightMode" = "ForwardBase" }
		Tags{ "RenderType" = "Opaque" }
		LOD 200


		Pass
		{
			ZWrite On
			//Cull Off
			//Blend one one

			CGPROGRAM
			#pragma target 5.0

			#pragma vertex vert			
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			StructuredBuffer<float4> _Positions;
			StructuredBuffer<float4> _Colors;
			StructuredBuffer<float4> _Velocities;
			StructuredBuffer<float4> _ForwardVectors;
			StructuredBuffer<float4> _UpVectors;
			StructuredBuffer<uint> _ParticlesToSpawnIDs; // should be AppendStructuredBuffer but that seems to cause compile error


			fixed4 _Color;
			float _Size;
			float _SizeScaler;
			sampler2D _Sprite;
			float4 _Sprite_TexelSize;

			sampler2D _MainTex;
			half _TextureScaleX;
			half _TextureScaleY;
			half _MaxLife;
			half _SimSpeed;
			
			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv_MainTex : TEXCOORD0;
				uint id : uint;
				float4 color : FLOAT4;
				//float4 col : COLOR;
				//float4 vel : FLOAT4;				
			};

			v2f vert(appdata_full v, uint instanceID : SV_InstanceID) //SV_InstanceID is about 5-6x slower than SV_VertexID when using DrawProceduralIndirect, shame
			{				
				uint spawnID = _ParticlesToSpawnIDs[instanceID]; // get proper spawn ID once, pass it along OUT.id afetrwards								
				float4 spawnPosData = _Positions[spawnID];
				float lifeRemaining = max(0, spawnPosData.w);
				float maxLife = _MaxLife;// BLARGH HARD CODING
				float lifeRatio = min((lifeRemaining / maxLife), 1);
				lifeRatio = 1.0 - pow(1.0 - lifeRatio, 0.50);

				// POSITION
				// do vertex/mesh scaling here
				float4 spawnVelData = _Velocities[spawnID];
				float spriteScale = lifeRatio * spawnVelData.w;
				float dy = 1.0 * abs(_SimSpeed) * abs(length(spawnVelData.xyz));
				v.vertex.xyz *= 1.0 * _Size * _SizeScaler;
				v.vertex.y *= (1.0+dy);
				float3 worldPos = spawnPosData.xyz + v.vertex.xyz;

				// COLOR
				float2 audioColUV = float2(_TextureScaleX, _TextureScaleY * (1.0 - lifeRatio)); //+ IN.idOffset));
				float lifeColScaler = 1.0; //max((lifeRatio + 0.17), 0.5);
				float4 audioCol = lifeColScaler * tex2Dlod(_MainTex, float4(audioColUV, 0, 0));
				
				
				v2f o;
				UNITY_INITIALIZE_OUTPUT(v2f, o);
				o.pos = mul(UNITY_MATRIX_VP, float4(worldPos, 1.0f));
				o.uv_MainTex = v.texcoord;
				o.id = spawnID;
				o.color = audioCol;
				
				return o;
			}

	
			
			
			float4 frag(v2f IN, bool isFragFrontFace : SV_IsFrontFace) : COLOR
			{				
				float4 audioCol = IN.color;//lifeCol; //tex2D(_MainTex, audioColUV);
				float audioColScale = 1.0;
				return tex2D(_Sprite, IN.uv_MainTex) * audioCol * audioColScale;
			}


			ENDCG
		}
	}
}
