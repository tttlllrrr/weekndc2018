﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/CSStandard_SurfEmitParticlesToAB_InUVRange" 
{
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_SpawnedParticle_Life("Spawned Particle Life", Range(0,100)) = 4.0
		_SpawnedParticle_Scale("Spawned Particle Size", Range(0,10)) = 1.0
		_SpawnedParticle_VelScale("Spawned Particle Vel Scale", Range(-10,10)) = 1.0

		_EmmitUV_Target_X("Emmit UV Target X", Range(0,1)) = 0.0
		_EmmitUV_Range_X_Left("Emmit UV Range X Left", Range(0,1)) = 0.0
		_EmmitUV_Range_X_Right("Emmit UV Range X Right", Range(0,1)) = 1.0

		_EmmitUV_Target_Y("Emmit UV Target Y", Range(0,1)) = 0.0
		_EmmitUV_Range_Y_Left("Emmit UV Range Y Left", Range(0,1)) = 0.0
		_EmmitUV_Range_Y_Right("Emmit UV Range Y Right", Range(0,1)) = 1.0

		_PixelDiscardToggle("Pixel Discard Toggle", int) = 0
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		Cull Off

		CGPROGRAM
		#pragma target 5.0

		#include "UnityCG.cginc"
		
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard //vertex:vert //fullforwardshadows
		
		
		struct Input 
		{
			float2 uv_MainTex;
			float3 worldPos;
			float3 worldNormal;
			float3 worldRefl;
			float3 viewDir;
		};

		sampler2D _MainTex;


		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		half _SpawnedParticle_Life;
		half _SpawnedParticle_Scale;
		half _SpawnedParticle_VelScale;

		half _EmmitUV_Target_X;
		half _EmmitUV_Range_X_Left;
		half _EmmitUV_Range_X_Right;

		half _EmmitUV_Target_Y;
		half _EmmitUV_Range_Y_Left;
		half _EmmitUV_Range_Y_Right;

		int _PixelDiscardToggle;
		
		struct ParticleSpwanData
		{
			float4 spawnPos;
			float4 spawnVel;
			float4 spawnCol;
			float4 spawnUpVec;
			float4 spawnForwardVec;
		};

		#ifdef SHADER_API_D3D11
		AppendStructuredBuffer<uint> _particlesAliveListBuffer_Append : register(u5);
		ConsumeStructuredBuffer<uint> _particlesDeadListBuffer_Consume :  register(u6);					
		AppendStructuredBuffer<ParticleSpwanData> _particleSpawnData : register(u7); // encode remaining life in w 
		#endif
		
		

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;

			
			/// check if we'll be emitting, in this case wrt uv.x

			// exptected range is 0-1, mapps to uv
			half target_x = _EmmitUV_Target_X;
			half range_x_left = _EmmitUV_Range_X_Left;
			half range_x_right = _EmmitUV_Range_X_Right;
			half current_x = clamp(IN.uv_MainTex.x, 0, 1);

			half target_y = _EmmitUV_Target_Y;
			half range_y_left = _EmmitUV_Range_Y_Left;
			half range_y_right = _EmmitUV_Range_Y_Right;
			half current_y = clamp(IN.uv_MainTex.y, 0, 1);

			bool is_UV_InRange_x = (current_x >= saturate(target_x - range_x_left)) && (current_x <= saturate(target_x + range_x_right));
			bool is_UV_InRange_y = (current_y >= saturate(target_y - range_y_left)) && (current_y <= saturate(target_y + range_y_right));
			// TODO : re-implement this. BLARGH ugly branchy hack fix for single pixel on 0 range bug
			bool isRange0_x = false;
			bool isRange0_y = false;
			if ((range_x_left + range_x_right) == 0)
				isRange0_x = true;
			if ((range_y_left + range_y_right) == 0)
				isRange0_y = true;
			if(is_UV_InRange_x && is_UV_InRange_y)
			{
				//o.Albedo = 0.0*float3(1, 4, 1);
				if( (!isRange0_x && !isRange0_y)  )
				{ 

					o.Albedo = float3(4, 4, 4);

					float spawnLife = _SpawnedParticle_Life; //4.0;
					float spawnScale = _SpawnedParticle_Scale; //1.0f;

					// for some reason the IN. stuff doesn't seem to work well inside of #ifdef SHADER_API_D3D11, so doing all data lookup before that

					//float3 worldPos = IN.worldNormal;
					float3 worldPos = IN.worldPos;
					float3 worldNormal = IN.worldNormal;
					float3 worldReflection = IN.worldRefl;
					float3 up = float3(0, 1, 0);
					float3 viewDir = IN.viewDir;

					float4 spawnPos = float4(worldPos.xyz, spawnLife);
					float4 spawnVel = float4(_SpawnedParticle_VelScale * worldNormal, spawnScale);
					float4 spawnCol = float4(1, 1, 1, 1);
					float4 spawnUpVec = float4(worldReflection, spawnLife);
					float4 spawnForwardVec = float4(worldNormal, 0);


					ParticleSpwanData particleSpawnData;
					particleSpawnData.spawnPos = spawnPos;
					particleSpawnData.spawnVel = spawnVel;
					particleSpawnData.spawnCol = spawnCol;
					particleSpawnData.spawnUpVec = spawnUpVec;
					particleSpawnData.spawnForwardVec = spawnForwardVec;


					#ifdef SHADER_API_D3D11						
					// add spawn data for this pixel to the append buffer, will actually get generated in a Compute Shader next frame
					_particleSpawnData.Append(particleSpawnData);
					#endif

					//o.Albedo = worldNormal;
					//o.Albedo = worldPos;
					//o.Albedo  = float3(1,0,0);
					//o.Albedo = IN.worldNormal;
				
					// NOTE: really weird hack to make sure worldPos etc doesn't get compiled into oblivion: need to reference the IN. variable near the end as a way to "anchor" them
					// if not, all those (thus also spawn data) seems to get defaulted to 0, maybe bug caused bythe #ifdef SHADER_API_D3D11	that cuts through the function
					o.Alpha = worldPos.x + worldNormal.x + worldReflection.x + viewDir.x; //worldNormal.x; //c.a;

					//clip(-1);
				}
			}
			if( (current_y > (target_y + range_y_right))  ) //(current_x > target_x) && (current_y > target_y) )
			{
				clip(_PixelDiscardToggle * -1);
			}
			
		}
		ENDCG
	}
	FallBack "Diffuse"
}
