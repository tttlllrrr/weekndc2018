﻿Shader "ComputeShaderSprites/CSParticleSpriteShader_UTB_AudioColTex"
{
	// based off of GPU_GEMS_NBodySim Unity example 
	// http://scrawkblog.com/category/particles/
	Properties
	{
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_Sprite("Sprite", 2D) = "white" {}
		_Color("Color", Color) = (0.8,0.6,0.8,1.0)
		_Size("Size", float) = 0.1		
		_SizeScaler("SizeScaler", float) = 1.0
		_TextureScaleX("TextureScale X", Range(0,1)) = 1.0
		_TextureScaleY("TextureScale Y", Range(0,1)) = 1.0
		_MaxLife("Max Life", Range(0,120)) = 60.0
	}
	SubShader
	{
		Tags { "Queue" = "Transparent"}
		//Tags{ "LightMode" = "ForwardBase" }

		Pass
		{
			ZWrite Off
			ZTest Always
			Cull Off
			Fog{ Mode Off }
			Blend one one

			
			CGPROGRAM
			#pragma target 5.0

			#pragma vertex vert
			#pragma geometry geom
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			StructuredBuffer<float4> _Positions;
			StructuredBuffer<float4> _Velocities;
			StructuredBuffer<float4> _Colors;

			fixed4 _Color;
			float _Size;
			float _SizeScaler;
			sampler2D _Sprite;
			float4 _Sprite_TexelSize;
			float _Theta_x;
			float _Theta_y;
			float _Theta_z;

			sampler2D _MainTex;
			half _TextureScaleX;
			half _TextureScaleY;
			half _MaxLife;

			struct v2g
			{
				float4 pos : SV_POSITION;
				int id : TEXCOORD0;
				//float4 col : COLOR;
				//float4 vel : FLOAT4;
			};

			v2g vert(uint id : SV_VertexID)
			{
				v2g OUT;
				float3 worldPos = _Positions[id].xyz;
				OUT.pos = mul(UNITY_MATRIX_VP, float4(worldPos, 1.0f));
				OUT.id = id;
				//OUT.col = _Colors[id];
				//OUT.vel = _Velocities[id];
				return OUT;

			}

			struct g2f 
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 col : COLOR;				
				float3 normal : FLOAT3;
				uint id : uint;
				float idOffset : FLOAT;
			};

			[maxvertexcount(4)]
			void geom(point v2g IN[1], inout TriangleStream<g2f> outStream)
			{
				uint id = IN[0].id;
				
				float lifeRemaining = max(0, _Positions[id].w);
				float maxLife = _MaxLife; //60.0; // BLARGH HARD CODING
				float lifeRatio = lifeRemaining / maxLife;
				
				float spriteScale = _Velocities[id].w;
				float dx = spriteScale * _Size * _SizeScaler; //* velSizeScale;
				float dy = spriteScale * _Size * _SizeScaler * _ScreenParams.x / _ScreenParams.y;// *_Sprite_TexelSize.w / _Sprite_TexelSize.z;//_ScreenParams.x / _ScreenParams.y * velSizeScale;
				g2f OUT;
				UNITY_INITIALIZE_OUTPUT(g2f, OUT);
				//OUT.col = IN[0].col;
				OUT.id = id;

				OUT.pos = IN[0].pos + float4(-dx, dy, 0, 0); OUT.uv = float2(0, 0); outStream.Append(OUT);
				OUT.pos = IN[0].pos + float4(dx, dy, 0, 0); OUT.uv = float2(1, 0); outStream.Append(OUT);
				OUT.pos = IN[0].pos + float4(-dx, -dy, 0, 0); OUT.uv = float2(0, 1); outStream.Append(OUT);
				OUT.pos = IN[0].pos + float4(dx, -dy, 0, 0); OUT.uv = float2(1, 1); outStream.Append(OUT);

				OUT.idOffset = float(id % 256) / 1048576; // generated per frame / total (active) count
			}
			
			float4 frag(g2f IN, bool isFragFrontFace : SV_IsFrontFace) : COLOR
			{	
				uint id = IN.id;
				float lifeRemaining = max(0, _Positions[id].w);
				float maxLife = 60.0; // BLARGH HARD CODING
				float lifeRatio = 1.0 - min(lifeRemaining / maxLife, 1);

				float2 audioColUV = float2(_TextureScaleX, _TextureScaleY * (lifeRatio + IN.idOffset));
				float4 audioCol = tex2D(_MainTex, audioColUV);

				//return audioCol;
				return tex2D(_Sprite, IN.uv) * audioCol; //_Colors[IN.id]; 
			}

			ENDCG
		}
	}
}
