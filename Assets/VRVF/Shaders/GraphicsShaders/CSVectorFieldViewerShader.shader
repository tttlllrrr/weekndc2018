﻿Shader "ComputeShaderSprites/CSVectorFieldViewerShader"
{
	Properties
	{
		_Sprite("Sprite", 2D) = "white" {}
		_Color("Color", Color) = (0.8,0.6,0.8,1.0)
		_Size("Size", float) = 0.1
		_SizeScaler("SizeScaler", float) = 1.0
	}
	SubShader
	{
		//Tags { "Queue" = "Transparent"}
		Tags{ "LightMode" = "ForwardBase" }

		Pass
		{
			Cull Off
			
			CGPROGRAM
			#pragma target 5.0

			#pragma vertex vert
			#pragma geometry geom
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			StructuredBuffer<float4> _Positions;
			StructuredBuffer<float4> _Colors;
			StructuredBuffer<float4> _Velocities;
			fixed4 _Color;
			float _Size;
			float _SizeScaler;
			sampler2D _Sprite;
			
			struct v2g
			{
				float4 pos : SV_POSITION;
				int id : TEXCOORD0;
				float4 col : COLOR;
				float4 vel : FLOAT4;
			};

			v2g vert(uint id : SV_VertexID)
			{
				v2g OUT;
				UNITY_INITIALIZE_OUTPUT(v2g, OUT);
				float3 worldPos = _Positions[id].xyz;
				OUT.pos = float4(worldPos, 1.0f);//mul(UNITY_MATRIX_VP, float4(worldPos, 1.0f));
				OUT.id = id;
				OUT.col = _Colors[id];
				OUT.vel = _Velocities[id];
				return OUT;
			}

			struct g2f 
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 col : COLOR;				
				float3 normal : FLOAT3;
			};

			[maxvertexcount(4)]
			void geom(point v2g IN[1], inout TriangleStream<g2f> outStream)
			{
				float velSizeScale = length(IN[0].vel.xyz);
				//velSizeScale = 1.0;
				float dx = _Size * _SizeScaler ;
				float dy = _Size * _SizeScaler * 3.0f *velSizeScale; //_ScreenParams.x / _ScreenParams.y * velSizeScale;
				g2f OUT;
				UNITY_INITIALIZE_OUTPUT(g2f, OUT);
				OUT.col = IN[0].col;

				float3 normedVel = normalize(-IN[0].vel.xyz).xyz;
				
				float3 pDir = normedVel;
				float3 upDir = float3(0, 1, 0);				
				// tried, got this from http://stackoverflow.com/questions/18558910/direction-vector-to-rotation-matrix
				
				float3 xAxis = normalize(cross(upDir, pDir));
				float3 yAxis = normalize(cross(pDir, xAxis));

				matrix rotMat = matrix(xAxis.x, pDir.x, yAxis.x, 0,
								xAxis.y, pDir.y, yAxis.y, 0,
								xAxis.z, pDir.z, yAxis.z, 0,
								0, 0, 0, 1);
				
				OUT.normal = yAxis;
			
				// figured out billboarind thanks to this: http://www.java-gaming.org/topics/solved-stop-geometry-shader-from-auto-billboarding-shapes/35693/view.html
				
				// normal fron side quad
				OUT.pos = mul(UNITY_MATRIX_VP, IN[0].pos + mul(rotMat, float4(-dx, dy, 0, 0)));
				OUT.uv = float2(0, 0); 
				//OUT.normal = yAxis; 
				outStream.Append(OUT);

				OUT.pos = mul(UNITY_MATRIX_VP, IN[0].pos + mul(rotMat, float4(dx, dy, 0, 0)));
				OUT.uv = float2(1, 0); 
				//OUT.normal = yAxis; 
				outStream.Append(OUT);

				OUT.pos = mul(UNITY_MATRIX_VP, IN[0].pos + mul(rotMat, float4(-dx, -dy, 0, 0)));
				OUT.uv = float2(0, 1);
				//OUT.normal = yAxis; 
				outStream.Append(OUT);

				OUT.pos = mul(UNITY_MATRIX_VP, IN[0].pos + mul(rotMat, float4(dx, -dy, 0, 0)));
				OUT.uv = float2(1, 1);
				//OUT.normal = yAxis; 
				outStream.Append(OUT);
				
				outStream.RestartStrip();

			}
			
			float4 frag(g2f IN, bool isFragFrontFace : SV_IsFrontFace) : COLOR
			{
				float4 color = IN.col * _Color;
				float4 minimalBoost = float4(0.1, 0.1, 0.1, 1.0);

				float4 texCol = tex2D(_Sprite, IN.uv);
				if (texCol.w == 0)
					discard;

				return texCol * color + minimalBoost; //* IN.col *IN.light; //_Color;
			}


			ENDCG
		}

















	}
}
