﻿Shader "ComputeShaderSprites/CSParticleSpriteShader_Unlit_AudioColTex_Append_Quad_Lit"
{
	// based off of GPU_GEMS_NBodySim Unity example 
	// http://scrawkblog.com/category/particles/
	Properties
	{
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_Sprite("Sprite", 2D) = "white" {}
		_Color("Color", Color) = (0.8,0.6,0.8,1.0)
		_SpawnColAudioColMix("Spawn Col vs Audio Col Mix", Range(0,1)) = 1.0
		_Size("Size", float) = 0.1
		_SizeScaler("SizeScaler", float) = 1.0
		_TextureScaleX("TextureScale X", Range(0,1)) = 1.0
		_TextureScaleY("TextureScale Y", Range(0,1)) = 1.0
		_MaxLife("Max Life", Range(0,120)) = 60.0
		_SimSpeed("Sim Speed", float) = 1
		_SpecPow("Specular Power", float) = 10.0
		_FinalColorScaler("Final Color Scaler", float) = 1.0

		_LightDirection("Light Direction", Vector) = (1,0,0,0)

	}
	SubShader
	{
		//Tags { "Queue" = "Transparent"}
		Tags{ "LightMode" = "ForwardBase" }

		Pass
		{
			Cull Off
			//Blend one one

			CGPROGRAM
			#pragma target 5.0

			#pragma vertex vert
			#pragma geometry geom
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			StructuredBuffer<float4> _Positions;
			StructuredBuffer<float4> _Colors;
			StructuredBuffer<float4> _Velocities;
			StructuredBuffer<float4> _ForwardVectors;
			StructuredBuffer<float4> _UpVectors;
			StructuredBuffer<uint> _ParticlesToSpawnIDs; // should be AppendStructuredBuffer but that seems to cause compile error


			fixed4 _Color;
			half _SpawnColAudioColMix;
			float _Size;
			float _SizeScaler;
			sampler2D _Sprite;
			float4 _Sprite_TexelSize;

			sampler2D _MainTex;
			half _TextureScaleX;
			half _TextureScaleY;
			half _MaxLife;
			half _SimSpeed;

			float _SpecPow;
			float _FinalColorScaler;

			float4 _LightDirection;

			
			struct v2g
			{
				float4 pos : SV_POSITION;
				uint id : uint;
				//float4 col : COLOR;
				//float4 vel : FLOAT4;				
			};

			v2g vert(uint id : SV_VertexID) //SV_InstanceID is about 5-6x slower than SV_VertexID when using DrawProceduralIndirect, shame
			{
				v2g OUT;

				UNITY_INITIALIZE_OUTPUT(v2g, OUT);
				uint spawnID = _ParticlesToSpawnIDs[id]; // get proper spawn ID once, pass it along OUT.id afetrwards
				float3 worldPos = _Positions[spawnID].xyz;
				OUT.pos = float4(worldPos, 1.0f);//mul(UNITY_MATRIX_VP, float4(worldPos, 1.0f));
				OUT.id = spawnID;
				//OUT.col = _Colors[id];
				//OUT.vel = _Velocities[id];				
				return OUT;
			}

			struct g2f 
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 col : COLOR;				
				float3 normal : FLOAT3;
				//uint id : uint;
				//float lifeRatio : FLOAT;
			};

			[maxvertexcount(4)]
			void geom(point v2g IN[1], inout TriangleStream<g2f> outStream)
			{
				uint id = IN[0].id; // should be getting spawndID aka apppend buffer data 
				float velSizeScale = 1.0;

				float lifeRemaining = max(0, _Positions[id].w);
				float maxLife = _UpVectors[id].w; //_MaxLife;// BLARGH HARD CODING
				float lifeRatio = min((lifeRemaining / maxLife), 1);
				//lifeRatio = 1.0 - lifeRatio;
				//lifeRatio = 1.0 -pow(1.0 -lifeRatio,0.50);
				//lifeRatio = max(0.1, lifeRatio);
				
				float spriteScale = lifeRatio * _Velocities[id].w; //* (max(0, _Positions[id].w) / _UpVectors[id].w);
				//spriteScale = 1.0;
				float dx = spriteScale * _Size * _SizeScaler; //* velSizeScale;
				float dy = spriteScale * _Size * _SizeScaler * _Sprite_TexelSize.w / _Sprite_TexelSize.z;//_ScreenParams.x / _ScreenParams.y * velSizeScale;
				float speed = length(_Velocities[id].xyz);
				dy += 0.01* abs(_SimSpeed) * abs(speed); //speed*abs(dy*0.01*pow(1.0 + speed, 3));
				//dx += 0.01;
				//float dx = 0.010;
				//float dy = 0.010 ;
				
				g2f OUT;
				UNITY_INITIALIZE_OUTPUT(g2f, OUT);
				//OUT.col = IN[0].col;
				//OUT.id = id;

				// color calc
				//float4 aliveCol = float4(0, 1, 0, 1); // for debugging life
				//float4 deadCol = float4(1, 0, 0, 1);
				//float4 lifeCol = lerp(aliveCol, deadCol, (1.0- lifeRatio));
				float2 audioColUV = float2(_TextureScaleX, _TextureScaleY * (1.0-lifeRatio)); //+ IN.idOffset));
				float lifeColScaler = 1.0; //max((lifeRatio + 0.17), 0.5);
				
				float4 audioCol = lifeColScaler * tex2Dlod(_MainTex, float4(audioColUV, 0, 0));
				float4 spawnedCol = _Colors[id];
				float4 mixedCol = lerp(spawnedCol, audioCol, _SpawnColAudioColMix);
				OUT.col = mixedCol;

				float3 normedVel = normalize(_Velocities[id]).xyz;
				
				float3 pDir = normedVel; //_ForwardVectors[id].xyz;//normedVel;
				float3 upDir = _UpVectors[id].xyz; //float3(0, 1, 0);				


				// tried, got this from http://stackoverflow.com/questions/18558910/direction-vector-to-rotation-matrix
				
				float3 xAxis = normalize(cross(upDir, pDir));
				float3 yAxis = normalize(cross(pDir, xAxis));

				
				matrix rotMat = matrix(xAxis.x, pDir.x, yAxis.x, 0,
								xAxis.y, pDir.y, yAxis.y, 0,
								xAxis.z, pDir.z, yAxis.z, 0,
								0, 0, 0, 1);
				

				/*
				matrix rotMat = matrix(	1, 0, 0, 0,
										0, 1, 0, 0,
										0, 0, 1, 0,
										0, 0, 0, 1);
				*/

				OUT.normal = yAxis;
				//OUT.normal = mul(rotMat,yAxis);
			
				// figured out billboarind thanks to this: http://www.java-gaming.org/topics/solved-stop-geometry-shader-from-auto-billboarding-shapes/35693/view.html
				
				// normal fron side quad
				OUT.pos = mul(UNITY_MATRIX_VP, IN[0].pos + mul(rotMat, float4(-dx, dy, 0, 0)));
				OUT.uv = float2(0, 0);
				//OUT.normal = yAxis; 
				outStream.Append(OUT);

				OUT.pos = mul(UNITY_MATRIX_VP, IN[0].pos + mul(rotMat, float4(dx, dy, 0, 0)));
				OUT.uv = float2(1, 0);
				//OUT.normal = yAxis; 
				outStream.Append(OUT);

				OUT.pos = mul(UNITY_MATRIX_VP, IN[0].pos + mul(rotMat, float4(-dx, -dy, 0, 0)));
				OUT.uv = float2(0, 1);
				//OUT.normal = yAxis; 
				outStream.Append(OUT);

				OUT.pos = mul(UNITY_MATRIX_VP, IN[0].pos + mul(rotMat, float4(dx, -dy, 0, 0)));
				OUT.uv = float2(1, 1);
				//OUT.normal = yAxis; 
				outStream.Append(OUT);


				outStream.RestartStrip();
				
			}
			
			float4 frag(g2f IN, bool isFragFrontFace : SV_IsFrontFace) : COLOR
			{
				float4 mixedCol = IN.col;;				
				float4 spriteCol = tex2D(_Sprite, IN.uv);
				
				float3 normal = IN.normal;
				if (isFragFrontFace == false)
					normal = -normal;

				/// NOTE: _WorldSpaceLightPos0 doesn't seem to work, 
				/// maybe because mat isn't attached to a renderer, etc, manually passing light data				
				float diffuse = dot(-_LightDirection, normal);

				float3 viewDir = normalize(IN.pos - _WorldSpaceCameraPos);
				float spec = pow(max(0.0 ,dot( reflect(_LightDirection, normal), viewDir)), _SpecPow) ;
				float lerpedSpec = lerp(0.0, 1.0, spec);
				float4 specCol =  (0.5 * float4(1, 1, 1, 1) + 0.5 * mixedCol);
				
				float4 finalCol = _FinalColorScaler * spriteCol * mixedCol * (max(diffuse, 0.1)) + lerpedSpec * specCol;
				return finalCol;

			}


			ENDCG
		}
	}
}
