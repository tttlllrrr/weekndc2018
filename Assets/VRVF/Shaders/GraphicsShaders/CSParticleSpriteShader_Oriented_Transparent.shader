﻿Shader "ComputeShaderSprites/CSParticleSpriteShader_Oriented_Transparent"
{
	// based off of GPU_GEMS_NBodySim Unity example 
	// http://scrawkblog.com/category/particles/
	Properties
	{
		_Sprite("Sprite", 2D) = "white" {}
		_Color("Color", Color) = (0.8,0.6,0.8,1.0)
		_Size("Size", float) = 0.1		
		_SizeScaler("SizeScaler", float) = 1.0
		_Cutoff ("Alpha cutoff", Range(0,1)) = 0.01
	}
	SubShader
	{
		Tags { "Queue" = "Transparent"}
		//Tags{ "LightMode" = "ForwardBase" }
		//Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="TransparentCutout"}

		Pass
		{
			ZWrite Off
			ZTest Always
			Cull Off
			Fog{ Mode Off }
			Blend one one
			
			CGPROGRAM
			#pragma target 5.0

			#pragma vertex vert
			#pragma geometry geom
			#pragma fragment frag			
			
			#include "UnityCG.cginc"

			StructuredBuffer<float4> _Positions;
			StructuredBuffer<float4> _Colors;
			StructuredBuffer<float4> _Velocities;
			StructuredBuffer<float4> _ForwardVectors;
			StructuredBuffer<float4> _UpVectors;

			fixed4 _Color;
			float _Size;
			float _SizeScaler;
			sampler2D _Sprite;
			float4 _Sprite_TexelSize;
			float _Cutoff;
			
			struct v2g
			{
				float4 pos : SV_POSITION;
				uint id : uint;
				//float4 col : COLOR;
				//float4 vel : FLOAT4;				
			};

			v2g vert(uint id : SV_VertexID)
			{
				v2g OUT;
				UNITY_INITIALIZE_OUTPUT(v2g, OUT);
				float3 worldPos = _Positions[id].xyz;
				OUT.pos = float4(worldPos, 1.0f);//mul(UNITY_MATRIX_VP, float4(worldPos, 1.0f));
				OUT.id = id;
				//OUT.col = _Colors[id];
				//OUT.vel = _Velocities[id];				
				return OUT;
			}

			struct g2f 
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				//float4 col : COLOR;				
				float3 normal : FLOAT3;
				uint id : uint;
			};

			[maxvertexcount(4)]
			void geom(point v2g IN[1], inout TriangleStream<g2f> outStream)
			{
				uint id = IN[0].id;
				float velSizeScale = 1.0;
				
				float spriteScale = _Velocities[id].w * (max(0, _Positions[id].w) / _UpVectors[id].w);
				float dx = 1.0 * _Size * _SizeScaler; //* velSizeScale;
				float dy = 1.0 * _Size * _SizeScaler * _Sprite_TexelSize.w / _Sprite_TexelSize.z;//_ScreenParams.x / _ScreenParams.y * velSizeScale;
				g2f OUT;
				UNITY_INITIALIZE_OUTPUT(g2f, OUT);
				//OUT.col = IN[0].col;
				OUT.id = id;

				//float3 normedVel = normalize(-_Velocities[id]).xyz;
				
				float3 pDir = -_ForwardVectors[id].xyz;//normedVel;
				float3 upDir = _UpVectors[id].xyz; //float3(0, 1, 0);				
				// tried, got this from http://stackoverflow.com/questions/18558910/direction-vector-to-rotation-matrix
				
				float3 xAxis = normalize(cross(upDir, pDir));
				float3 yAxis = normalize(cross(pDir, xAxis));

				matrix rotMat = matrix(xAxis.x, pDir.x, yAxis.x, 0,
								xAxis.y, pDir.y, yAxis.y, 0,
								xAxis.z, pDir.z, yAxis.z, 0,
								0, 0, 0, 1);
				
				OUT.normal = yAxis;
			
				// figured out billboarind thanks to this: http://www.java-gaming.org/topics/solved-stop-geometry-shader-from-auto-billboarding-shapes/35693/view.html
				
				// normal fron side quad
				OUT.pos = mul(UNITY_MATRIX_VP, IN[0].pos + mul(rotMat, float4(-dx, dy, 0, 0)));
				OUT.uv = float2(0, 0); 
				//OUT.normal = yAxis; 
				outStream.Append(OUT);

				OUT.pos = mul(UNITY_MATRIX_VP, IN[0].pos + mul(rotMat, float4(dx, dy, 0, 0)));
				OUT.uv = float2(1, 0); 
				//OUT.normal = yAxis; 
				outStream.Append(OUT);

				OUT.pos = mul(UNITY_MATRIX_VP, IN[0].pos + mul(rotMat, float4(-dx, -dy, 0, 0)));
				OUT.uv = float2(0, 1);
				//OUT.normal = yAxis; 
				outStream.Append(OUT);

				OUT.pos = mul(UNITY_MATRIX_VP, IN[0].pos + mul(rotMat, float4(dx, -dy, 0, 0)));
				OUT.uv = float2(1, 1);
				//OUT.normal = yAxis; 
				outStream.Append(OUT);
				
				outStream.RestartStrip();

				/*
				// giving up on double sided quad generation, perf cost is too high (1.5 - 2x more than single side)				
				// backside quad
				OUT.pos = mul(UNITY_MATRIX_VP, IN[0].pos + mul(rotMat, float4(-dx, dy, 0, 0)));
				OUT.uv = float2(0, 0);
				OUT.normal = -yAxis;
				outStream.Append(OUT);				

				OUT.pos = mul(UNITY_MATRIX_VP, IN[0].pos + mul(rotMat, float4(-dx, -dy, 0, 0)));
				OUT.uv = float2(0, 1);
				OUT.normal = -yAxis;
				outStream.Append(OUT);

				OUT.pos = mul(UNITY_MATRIX_VP, IN[0].pos + mul(rotMat, float4(dx, dy, 0, 0)));
				OUT.uv = float2(1, 0);
				OUT.normal = -yAxis;
				outStream.Append(OUT);

				OUT.pos = mul(UNITY_MATRIX_VP, IN[0].pos + mul(rotMat, float4(dx, -dy, 0, 0)));
				OUT.uv = float2(1, 1);
				OUT.normal = -yAxis;
				outStream.Append(OUT);
				outStream.RestartStrip();
				*/
			}
			
			float4 frag(g2f IN, bool isFragFrontFace : SV_IsFrontFace) : COLOR
			{
				/*
				float3 normal = IN.normal;
				if (isFragFrontFace == false)
				{
					//return float4(1, 0, 1, 1);
					normal = -normal;
				}
				float lambertian = abs(dot(normal, normalize(_WorldSpaceLightPos0.xyz))); // max(0, dot(normal, normalize(_WorldSpaceLightPos0.xyz)));

				// blinn phong specular
				float viewDir = normalize(-IN.pos);			
				float3 halfDir = normalize(normalize(_WorldSpaceLightPos0.xyz) + viewDir);
				float specBase = max(0, dot(halfDir, normal));
				float specular = pow(specBase, 32);
				float4 specCol = float4(1, 1, 1, 1);
				float4 color = IN.col * lambertian + specCol * specular;
				float4 minimalBoost = 0.1 * normalize(IN.col) + 0.05 *float4(1,1,1,1);
				*/
				//float3 normal = IN.normal;
				//float lambertian = abs(dot(normal, normalize(_WorldSpaceLightPos0.xyz)));

				float4 texCol = tex2D(_Sprite, IN.uv);
				if(texCol.a < _Cutoff)
					discard;

				float4 color = _Colors[IN.id]; //* lambertian; //IN.col;
				float4 minimalBoost = 0.1f *_Colors[IN.id];  //float4(0.1, 0.1, 0.1, 1.0);

				return texCol * color + minimalBoost; //* IN.col *IN.light; //_Color;
			}


			ENDCG
		}
	}
}
