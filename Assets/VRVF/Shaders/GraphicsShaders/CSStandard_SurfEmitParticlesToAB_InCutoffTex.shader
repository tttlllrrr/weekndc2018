﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/CSStandard_SurfEmitParticlesToAB_InCutoffTex" 
{
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_SpawnedParticle_Life("Spawned Particle Life", Range(0,100)) = 4.0
		_SpawnedParticle_Scale("Spawned Particle Size", Range(0,10)) = 1.0
		_SpawnedParticle_VelScale("Spawned Particle Vel Scale", Range(-10,10)) = 1.0
		_SpawnedParticle_OffsetSpawnVel("Spawned Particle Offset Spawn Vel", vector) = (0,0,0,0)
		_SpawnedParticle_PosOffsetNormal("Spawned Particle Pos Offset wrt Normal", float) = 0.0

		_CutoffTex("Cutoff Tex", 2D) = "white" {}
		_CutoffThreashold("Cutoff Threashold", Range(0,1)) = 0.0

		_EmitRangeFromCutoff_Left("Emit Range From Cutoff Left", Range(0,1)) = 0.0
		_EmitRangeFromCutoff_Right("Emit Range From Cutoff Right", Range(0,1)) = 0.0

		_PixelDiscardToggle("Pixel Discard Toggle", int) = 0
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		Cull Off

		CGPROGRAM
		#pragma target 5.0

		#include "UnityCG.cginc"
		
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard finalcolor:FinalColor //vertex:vert //fullforwardshadows
		
		
		struct Input 
		{
			float2 uv_MainTex;
			float3 worldPos;
			float3 worldNormal;
			float3 worldRefl;
			float3 viewDir;
		};

		sampler2D _MainTex;
		sampler2D _CutoffTex;


		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		half _SpawnedParticle_Life;
		half _SpawnedParticle_Scale;
		half _SpawnedParticle_VelScale;
		half4 _SpawnedParticle_OffsetSpawnVel;
		half _SpawnedParticle_PosOffsetNormal;
		
		half _CutoffThreashold;
		half _EmitRangeFromCutoff_Left;
		half _EmitRangeFromCutoff_Right;

		int _PixelDiscardToggle;
		
		struct ParticleSpwanData
		{
			float4 spawnPos;
			float4 spawnVel;
			float4 spawnCol;
			float4 spawnUpVec;
			float4 spawnForwardVec;
		};

		#ifdef SHADER_API_D3D11
		AppendStructuredBuffer<uint> _particlesAliveListBuffer_Append : register(u5);
		ConsumeStructuredBuffer<uint> _particlesDeadListBuffer_Consume :  register(u6);					
		AppendStructuredBuffer<ParticleSpwanData> _particleSpawnData : register(u7); // encode remaining life in w 
		#endif
		
		

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			float2 uv = IN.uv_MainTex;
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, uv) * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;

		}

		void FinalColor(Input IN, SurfaceOutputStandard o, inout fixed4 color)
		{
			
			float2 uv = IN.uv_MainTex;
			
			float cutoffTex = tex2D(_CutoffTex, uv).r;

			///// first check if we'll be spawning particles, needs to be range fron cutoff value
			float spawnRange_Left = _EmitRangeFromCutoff_Left;
			float spawnRange_Right = _EmitRangeFromCutoff_Right;

			bool isValid_Left = (cutoffTex > (_CutoffThreashold - spawnRange_Left) );
			bool isValid_Right = (cutoffTex < (_CutoffThreashold + spawnRange_Right) );


			// for some reason the IN. stuff doesn't seem to work well inside of #ifdef SHADER_API_D3D11, so doing all data lookup before that

			//float3 worldPos = IN.worldNormal;
			float3 worldPos = IN.worldPos;
			float3 worldNormal = IN.worldNormal;
			float3 worldReflection = IN.worldRefl;
			float3 up = float3(0, 1, 0);
			float3 viewDir = IN.viewDir;

			// TOD: _CutoffThreashold here is blargh ugly hack, should be changing val of Right range
			if(  isValid_Left && isValid_Right && (_CutoffThreashold != 0) )
			{		
				

				float spawnLife = _SpawnedParticle_Life; //4.0;
				float spawnScale = _SpawnedParticle_Scale; //1.0f;


				float4 spawnPos = float4(worldPos.xyz, spawnLife) + float4(_SpawnedParticle_PosOffsetNormal * worldNormal,0);
				float4 spawnCol = float4(color.xyz, 1); //float4(1, 1, 1, 1);
				//float4 spawnUpVec = float4(worldReflection, spawnLife);
				//float4 spawnForwardVec = float4(worldNormal, 0);
				float4 spawnUpVec = float4(worldNormal, spawnLife);
				float3 t1 = cross(worldNormal, float3(0, 0, 1));
				float3 t2 = cross(worldNormal, float3(0, 1, 0));
				float3 tangent;
				if (length(t1) > length(t2))
					tangent = t1;
				else
					tangent = t2;
				float4 spawnForwardVec = float4(tangent, 0);
				float4 spawnVel = float4(_SpawnedParticle_VelScale * spawnForwardVec.xyz, spawnScale) + _SpawnedParticle_OffsetSpawnVel;

				ParticleSpwanData particleSpawnData;
				particleSpawnData.spawnPos = spawnPos;
				particleSpawnData.spawnVel = spawnVel;
				particleSpawnData.spawnCol = spawnCol;
				particleSpawnData.spawnUpVec = spawnUpVec;
				particleSpawnData.spawnForwardVec = spawnForwardVec;


				#ifdef SHADER_API_D3D11						
				// add spawn data for this pixel to the append buffer, will actually get generated in a Compute Shader next frame
				_particleSpawnData.Append(particleSpawnData);
				#endif
				
				
				//color.xyz = 1.0 * float3(1, 1, 1);
				//color.xyz *= 2.0;
			}

			if( (cutoffTex < _CutoffThreashold) )
				clip( (_PixelDiscardToggle * -1)  );
			
			// NOTE: really weird hack to make sure worldPos etc doesn't get compiled into oblivion: need to reference the IN. variable near the end as a way to "anchor" them
			// if not, all those (thus also spawn data) seems to get defaulted to 0, maybe bug caused bythe #ifdef SHADER_API_D3D11	that cuts through the function
			color.w = worldPos.x + worldNormal.x + worldReflection.x + viewDir.x; //worldNormal.x; //c.a;

			//color = fixed4(1, 1, 1, 1);
		}

		ENDCG
	}
	FallBack "Diffuse"
}
