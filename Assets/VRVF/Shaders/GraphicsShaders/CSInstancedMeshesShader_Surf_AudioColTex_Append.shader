﻿Shader "ComputeShaderInstancedMeshes/CSInstancedMeshesShader_Surf_AudioColTex_Append"
{
	/// Reference --> https://docs.unity3d.com/ScriptReference/Graphics.DrawMeshInstancedIndirect.html

	Properties
	{
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0

		_Sprite("Sprite", 2D) = "white" {}
		_Color("Color", Color) = (0.8,0.6,0.8,1.0)
		_Size("Size", float) = 0.1		
		_SizeScaler("SizeScaler", float) = 1.0
		_TextureScaleX("TextureScale X", Range(0,1)) = 1.0
		_TextureScaleY("TextureScale Y", Range(0,1)) = 1.0
		_MaxLife("Max Life", Range(0,120)) = 60.0
		_SimSpeed("Sim Speed", float) = 1
	}
	SubShader
	{
		//Tags { "Queue" = "Transparent"}
		//Tags{ "LightMode" = "ForwardBase" }
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		//ZWrite On
		//Cull Off
		//Blend one one

		CGPROGRAM
		#pragma target 5.0
		
		// Physically based Standard lighting model
		#pragma surface surf Standard vertex:vert //addshadow fullforwardshadows
		#pragma multi_compile_instancing
		//#pragma instancing_options procedural:setup
	
		#ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED
		StructuredBuffer<float4> _Positions;
		StructuredBuffer<float4> _Colors;
		StructuredBuffer<float4> _Velocities;
		StructuredBuffer<float4> _ForwardVectors;
		StructuredBuffer<float4> _UpVectors;
		StructuredBuffer<uint> _ParticlesToSpawnIDs; // should be AppendStructuredBuffer but that seems to cause compile error
		#endif

		fixed4 _Color;
		float _Size;
		float _SizeScaler;
		sampler2D _Sprite;
		float4 _Sprite_TexelSize;

		sampler2D _MainTex;
		half _TextureScaleX;
		half _TextureScaleY;
		half _MaxLife;
		half _SimSpeed;

		half _Glossiness;
		half _Metallic;

		struct Input 
		{
			float2 uv_MainTex;
		};

		void vert(inout appdata_full v, out Input o) 
		{
			UNITY_INITIALIZE_OUTPUT(Input, o);

			float4 data = float4(0, 0, 0, 0);
			uint spawnID = 0;
			float4 spawnPosData = float4(0, 0, 0, 0);
			float4 spawnVelData = float4(0, 0, 0, 0);
			#ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED			
			spawnID = _ParticlesToSpawnIDs[unity_InstanceID]; // get proper spawn ID once, pass it along OUT.id afetrwards								
			spawnPosData = _Positions[spawnID];
			spawnVelData = _Velocities[spawnID];
			data = 0.01 * spawnPosData; //positionBuffer[unity_InstanceID];
			#endif

			float lifeRemaining = max(0, spawnPosData.w);
			float maxLife = _MaxLife;// BLARGH HARD CODING
			float lifeRatio = min((lifeRemaining / maxLife), 1);
			lifeRatio = 1.0 - pow(1.0 - lifeRatio, 0.50);

			// POSITION
			// do vertex/mesh scaling here			
			float spriteScale = lifeRatio * spawnVelData.w;
			float dy = 1.0 * abs(_SimSpeed) * abs(length(spawnVelData.xyz));
			//v.vertex.xyz *= 1.0 * _Size * _SizeScaler;
			//v.vertex.y *= (1.0 + dy);
			//float3 worldPos = spawnPosData.xyz + v.vertex.xyz;

			// COLOR
			float2 audioColUV = float2(_TextureScaleX, _TextureScaleY * (1.0 - lifeRatio)); //+ IN.idOffset));
			float lifeColScaler = 1.0; //max((lifeRatio + 0.17), 0.5);
			float4 audioCol = lifeColScaler * tex2Dlod(_MainTex, float4(audioColUV, 0, 0));

			v.vertex.xyz = mul(UNITY_MATRIX_VP, float4(v.vertex.xyz +100.0* spawnPosData, 1.0f));
			//o.uv_MainTex = v.texcoord;
			//o.id = spawnID;
			//o.color = audioCol;

			//o.customColor = abs(v.normal);
		}

		/*
		void setup()
		{
			float4 data = float4(0,0,0,0);
			uint spawnID = 0;
			float4 spawnPosData = float4(0,0,0,0);
			float4 spawnVelData = float4(0,0,0,0);
			#ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED			
			spawnID = _ParticlesToSpawnIDs[unity_InstanceID]; // get proper spawn ID once, pass it along OUT.id afetrwards								
			spawnPosData = _Positions[spawnID];
			spawnVelData = _Velocities[spawnID];
			data = 0.01 * spawnPosData; //positionBuffer[unity_InstanceID];
			#endif

			float lifeRemaining = max(0, spawnPosData.w);
			float maxLife = _MaxLife;// BLARGH HARD CODING
			float lifeRatio = min((lifeRemaining / maxLife), 1);
			lifeRatio = 1.0 - pow(1.0 - lifeRatio, 0.50);

			// POSITION
			// do vertex/mesh scaling here			
			float spriteScale = lifeRatio * spawnVelData.w;
			float dy = 1.0 * _SimSpeed * abs(length(spawnVelData.xyz));
			//v.vertex.xyz *= 1.0 * _Size * _SizeScaler;
			//v.vertex.y *= (1.0 + dy);
			//float3 worldPos = spawnPosData.xyz + v.vertex.xyz;

			// COLOR
			float2 audioColUV = float2(_TextureScaleX, _TextureScaleY * (1.0 - lifeRatio)); //+ IN.idOffset));
			float lifeColScaler = 1.0; //max((lifeRatio + 0.17), 0.5);
			float4 audioCol = lifeColScaler * tex2Dlod(_MainTex, float4(audioColUV, 0, 0));


			unity_ObjectToWorld._11_21_31_41 = float4(data.w, 0, 0, 0);
			unity_ObjectToWorld._12_22_32_42 = float4(0, data.w, 0, 0);
			unity_ObjectToWorld._13_23_33_43 = float4(0, 0, data.w, 0);
			unity_ObjectToWorld._14_24_34_44 = float4(data.xyz, 1);
			unity_WorldToObject = unity_ObjectToWorld;
			unity_WorldToObject._14_24_34 *= -1;
			unity_WorldToObject._11_22_33 = 1.0f / unity_WorldToObject._11_22_33;
		}
		*/

		void surf(Input IN, inout SurfaceOutputStandard o) 
		{
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG		
	}
	FallBack "Diffuse"
}
