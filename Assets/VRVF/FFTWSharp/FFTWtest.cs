﻿using System;
using System.Runtime.InteropServices;
using FFTWSharp;

namespace FFTWSharp_test
{
    public class FFTWtest
    {
        const int sampleSize = 16384;
        const int repeatPlan = 10000;
        const bool forgetWisdom = false;
        
        //managed arrays
        float[] fin, fout;
        
        
        // and an example of the managed interface
        fftwf_plan mplan1, mplan2, mplan3, mplan4;
        fftwf_complexarray mfin, mfout;
        
        // length of arrays
        int fftLength = 0;

        // Initializes FFTW and all arrays
        // n: Logical size of the transform
        public FFTWtest(int n)
        {
            Console.WriteLine($"Start testing with n = {n.ToString("#,0")} complex numbers. All plans will be executed {repeatPlan.ToString("#,0")} times on a single thread.");
            Console.WriteLine("Please wait, creating plans...");
            fftLength = n;
            
            // create two managed arrays, possibly misalinged
            // n*2 because we are dealing with complex numbers
            fin = new float[n * 2];
            fout = new float[n * 2];
         
            // create a managed plan as well
            mfin = new fftwf_complexarray(fin);
            mfout = new fftwf_complexarray(fout);
         
            mplan1 = fftwf_plan.dft_1d(n, mfin, mfout, fftw_direction.Forward, fftw_flags.Estimate);
            mplan2 = fftwf_plan.dft_1d(n, mfin, mfout, fftw_direction.Forward, fftw_flags.Measure);
            mplan3 = fftwf_plan.dft_1d(n, mfin, mfout, fftw_direction.Forward, fftw_flags.Patient);

            mplan4 = fftwf_plan.dft_1d(n, mfout, mfin, fftw_direction.Backward, fftw_flags.Measure);
         

            // fill our arrays with an arbitrary complex sawtooth-like signal
            for (int i = 0; i < n * 2; i++) fin[i] = i % 50;
            for (int i = 0; i < n * 2; i++) fout[i] = i % 50;
         
            
            Console.WriteLine();
        }

        public void TestAll()
        {
            
            // check and see how we did, don't say anyt
            for (int i = 0; i < fftLength * 2; i++)
            {
                // check against original values
                // note that we need to scale down by length, due to FFTW scaling by N
                if (System.Math.Abs(fin[i]/fftLength - (i % 50)) > 1e-3)
                {
                    System.Console.WriteLine("FFTW consistency error!");
                    return;
                }
            }

            Console.WriteLine();
            TestPlan(mplan1, "#1 single (managed) | mfin, mfout, Forward,  Estimate");
            TestPlan(mplan2, "#2 single (managed) | mfin, mfout, Forward,  Measure ");
            TestPlan(mplan3, "#3 single (managed) | mfin, mfout, Forward,  Patient ");
            Console.WriteLine();

            // fill our input array with an arbitrary complex sawtooth-like signal
            for (int i = 0; i < fftLength * 2; i++) fin[i] = i % 50;
            for (int i = 0; i < fftLength * 2; i++) fout[i] = 0;

            mfin.SetData(fin);
            mfout.SetData(fout);
            TestPlan(mplan2, "#2 single (managed) | mfin, mfout, Forward,  Measure ");

            fout = mfout.GetData_Float();  // let's see what's in mfout
                                           // at this point mfout contains the FFT'd mfin

            TestPlan(mplan4, "#4 single (managed) | mfout, mfin, Backward, Measure ");
            // at this point we have transfarred backwards the mfout into mfin, so mfin should be very close to the original complex sawtooth-like signal

            fin = mfin.GetData_Float();
            for (int i = 0; i < fftLength * 2; i++) fin[i] /= fftLength;
            // at this point fin should be very close to the original (sawtooth-like) signal

            // check and see how we did, don't say anyt
            for (int i = 0; i < fftLength * 2; i++)
            {
                // check against original values
                if (System.Math.Abs(fin[i] - (i % 50)) > 1e-3)
                {
                    System.Console.WriteLine("FFTW consistency error!");
                    return;
                }
            }

            Console.WriteLine();

            TestPlan(mplan2, "#2 single (managed) | mfin, mfout, Forward,  Measure ");
           
            Console.WriteLine();
        }

        // Tests a single plan, displaying results
        // plan: Pointer to plan to test
        public void TestPlan(object plan, string planName)
        {
            // a: adds, b: muls, c: fmas
            double a = 0, b = 0, c = 0;

            int start = System.Environment.TickCount;
            
            
            if (plan is fftwf_plan)
            {
                fftwf_plan mplan = (fftwf_plan)plan;

                for (int i = 0; i < repeatPlan; i++)
                {
                    mplan.Execute();
                }

                fftwf.flops(mplan.Handle, ref a, ref b, ref c);
            }

            double mflops = (((a + b + 2 * c)) * repeatPlan) / (1024 * 1024);
            long ticks = (System.Environment.TickCount - start);

            //Console.WriteLine($"Plan '{planName}': {ticks.ToString("#,0")} us | mflops: {FormatNumber(mflops)} | mflops/s: {(1000*mflops/ticks).ToString("#,0.0")}");
            Console.WriteLine("Plan '{0}': {1,8:N0} us | mflops: {2,8:N0} | mflops/s: {3,8:N0}", planName, ticks, mflops, (1000 * mflops / ticks));
        }

        // Releases all memory used by FFTW/C#
        ~FFTWtest()
        {
            fftwf.export_wisdom_to_filename("wisdom.wsd");

            // it is essential that you call these after finishing
            // that's why they're in the destructor. See Also: RAII
         
        }

        static void Main(string[] args)
        {
            if (forgetWisdom)
            {
                fftwf.fftwf_forget_wisdom();
            }
            else
            {
                Console.WriteLine("Importing wisdom (wisdom speeds up the plan creation process, if that plan was previously created at least once)");
                fftwf.import_wisdom_from_filename("wisdom.wsd");
            }

            // initialize our FFTW test class
            FFTWtest test = new FFTWtest(sampleSize); 

            // run the tests, print debug output
            test.TestAll();

            // pause for user input, then quit
            System.Console.WriteLine("\nDone. Press any key to exit.");
            String str = System.Console.ReadLine();
        }
    }
}